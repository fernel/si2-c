
@php  $creat=array(); @endphp
<div class="row">
<div class="col-12">
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">
        <a href="{{route('admin.crm.index')}}">
          <i class="pe-7s-back fs-25 text-red"></i>
        </a>
        Orden # {{$finded->id}}
      </h4>
    </div>
    <div class="card-body">
      <div class="row">
				<div class="col-md-12">
          <!--INVOICE------->
          @php $cltInfo = $finded->getBuyer()->first();
            $moves = $finded->getMoves()->get();
            $tlyesp = (!empty($cltInfo)?$cltInfo->get_typeSus()->first():(object)["colorsus"=>"#e3e1ca","typesus"=>"Minorista"]);
          @endphp
          <div class="card-body">
            <div class="row ">
              <div class="col-lg-6 ">
                <p class="h3 clientname">
                  <b> Nombre:</b> {{$cltInfo->nameF." ".$cltInfo->nameS." ".$cltInfo->lastnameF." ".$cltInfo->lastnameS}}
                  <br>
                  <b>Correo:</b> {{$cltInfo->mail}} <br>
                  <b>Tipo de suscriptor:</b> {!!'<span class="badge text-white"
                    style="background-color:'.$tlyesp->colorsus.';border:0;"
                    >'.$tlyesp->typesus.'</span>'!!}<br>
                    <b>Descuento:</b> {{empty($tlyesp->discount)?$tlyesp->discount:0}} <br>
                </p>
                <address class="fs-15">
                  <b>Dirección 1: </b>{{$cltInfo->addressF}}<br>
                  <b for="">Dirección 2: </b>{{$cltInfo->addressS}}<br>
                  <b for="">Teléfono 1: </b>{{$cltInfo->phone}}<br>
                  <b for="">Teléfono 2: </b>{{$cltInfo->phone2}}<br>
                </address>
              </div>
              <div class="col-lg-6 text-right">
                <b>Despachar a</b>
                <address class="fs-15">
                  {{$cltInfo->addressS}}
                </address>
              </div>
            </div>

            <div class="">
              <p class="mb-1 mt-5"><b class="font-weight-semibold">Fecha de orden : </b>
                <span>{{date("d M Y H:m",strtotime($finded->created_at))}} hrs</span></p>
              <div class="mb-1">
                  <b>Estado de orden : </b>
                  <div style="display:inline-block">
                  @foreach($moves AS $move)
                    @php $statusl = $move->getEstatus()->first(); @endphp
                  <span class="badge text-white" style="background-color:black;color:white;padding:5px;border:0;"
                    >{{$statusl->nametype}}</span><br>
                  @endforeach
                </div>
              </div>
              {{-- <p><span class="font-weight-semibold">Order ID  : </span>#{{$finded->id}}</p> --}}
              <br><br>
            </div>
            <div class="table-responsive push">
              <table style="border-spacing:0px;">
                <tr>
                  <th class="text-center "></th>
                  <th>Producto</th>
                  <th class="text-center" >Cantidad</th>
                  <th class="text-right" >Precio</th>
                  <th class="text-right">Subtotal</th>
                </tr>
                @php $totl=0;@endphp
                  @foreach($finded->details()->get() AS $vals)
                    <tr >
                      <td style="border:1px solid black;padding:5 8px;"class="text-center">{{ $loop->index + 1 }}</td>
                      <td style="border:1px solid black;padding:5 15px;">
                        <p class="font-w600 mb-1">{{$vals->productIs()->first()->title}}</p>
                      </td>
                      <td style="border:1px solid black;text-align:center;padding:5 8px;" class="text-center">{{$vals->quantity}}</td>
                      <td style="border:1px solid black;text-align:center;padding:5 8px;" class="text-right">{{$vals->productIs()->first()->price}}</td>
                      <td style="border:1px solid black;text-align:center;padding:5 8px;" class="text-right">{{$vals->quantity*$vals->productIs()->first()->price}}</td>
                      @php $totl+=$vals->quantity*$vals->productIs()->first()->price; @endphp
                    </tr>
                  @endforeach

                <tr>
                  <td colspan="4" class="font-w600 text-right" style="border:1px solid black;text-align:right;">Total</td>
                  <td style="border:1px solid black;text-align:center;" >{{$totl}}</td>
                </tr>

              </table>
            </div>
          </div>
          <!--END INVOICE------->
				</div>
			</div>

     </div>
   </div>
</div>
</div>
</div>
