@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
    var kad={
                0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1:{type:"slct",tl:"Proveedor",nm:"proveedor_id",vl:@json($providers_arr),elv:"0"},
                2:{type:"txt",tl:"Número",nm:"numero",elv:"1", nxt:6},
                3:{type:"txt",tl:"Serie",nm:"serie",elv:"2", nxt:6},
                4:{type:"nbr",tl:"Total",nm:"total",elv:"3", nxt:6},
                5:{type:"dtmpckr",tl:"Fecha",nm:"fecha",elv:"4", nxt:6},
                6:{type:"txtarea",tl:"Información",nm:"info",elv:"5"},
                7:{type:"hddn",nm:"tipo",vl:'equipo'},
            },
        valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Bodegas de equipo</h1>
    <ul>
        <li><a href="{{url('/bodegasEquipo')}}">Inicio</a></li>
        <li><a href="{{url('/ingresosEquipo', ['bodega_id' => $bodega->id])}}">Ingresos</a></li>
        <li>Facturas</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    {{$bodega->nombre;}}
                </h3>

                <h3 class="card-title">
                    <a href="javascript:"class="" onclick="newfloatv2(kad);">
                        <i class="ion-ios7-plus-outline "></i>
                        &nbsp;&nbsp;Ingresar factura
                    </a>
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="facturas_equipos_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Proveedor</th>
                                <th>No. Factura</th>
                                <th>Total</th>
                                <th>Fecha de factura</th>
                                <th>Fecha de ingreso al sistema</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $criti = array(); @endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $providers_obj[$value->proveedor_id]->nombre }}</td>
                                    <td>{{ $value->numero }}</td>
                                    <td>{{ $value->total }}</td>
                                    <td>{{ $value->fecha }}</td>
                                    <td>{{ $value->created_at }}</td>
                                    <td><span class="badge w-badge badge-{{ $value->estado_badge }}">{{ $value->estado_texto }}</span></td>
                                    <td>
                                        <a href="{{url('/equiposFactura', ['bodega_id'=>$bodega->id,'factura_id' => $value->id])}}" title="Administrar productos de la factura">
                                            <i class="text-20"data-feather="layers"></i>
                                        </a>
                                    </td>
                                </tr>
                                @php $criti[$value->id] = [$value->nombre, $value->razon]; @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  var criteria = @json($criti);
</script>

@endsection

@section('bottom-js')
    <script>
        $('#facturas_equipos_table').DataTable();
    </script>
@endsection
