@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
    var kad={
                0 :{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1 :{type:"txt",tl:"Código",id:"codigo",nm:"codigo",elv:"0", nxt:6},
                2 :{type:"txt",tl:"Nombre",id:"nombre",nm:"nombre",elv:"1", nxt:6},
                3 :{type:"txtarea",tl:"Descripción",id:"descripcion",nm:"descripcion",elv:"2"},
                4 :{type:"slct",tl:"Categoria",id:"categoria_id",nm:"categoria_id",vl:@json($categorias_arr),elv:"3", nxt:6, add:'onchange="cambiarSubcategoria(this)"'},
                5 :{type:"slct",tl:"Subcategoria",id:"sub_categoria_id",nm:"sub_categoria_id",vl:@json($subcategorias_arr),elv:"4", nxt:6},
                6 :{type:"nbr",tl:"Depreciación (porcentaje)",id:"porcentaje_depreciacion",nm:"porcentaje_depreciacion",elv:"5"},
                7 :{type:"slct",tl:"Medida de venta",id:"medida_id",nm:"medida_id",vl:@json($unidades_arr),elv:"6", nxt:6},
                8 :{type:"nbr",tl:"Cantidad  de las que dispone",id:"cantidad",nm:"cantidad",elv:"7", nxt:6, add:``},
                9 :{type:"nbr",tl:"Costo unitario Q.",id:"costo_unidad",nm:"costo_unidad",elv:"8", nxt:6, add:``},
                10:{type:"nbr",tl:"Precio unitario al cliente Q.",id:"precio_cliente_unidad",nm:"precio_cliente_unidad",elv:"9", nxt:6},
                11:{type:"nbr",tl:"Costo del equipo Q.",id:"costo_total",nm:"costo_total",elv:"10", add:``},
            },
        valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Bodegas de equipo</h1>
    <ul>
        <li><a href="{{url('/bodegasEquipo')}}">Inicio</a></li>
        <li><a href="{{url('/ingresosEquipo', ['bodega_id' => $bodega->id])}}">Ingresos</a></li>
        <li><a href="{{url('/facturasEquipo', ['bodega_id' => $bodega->id])}}">Facturas</a></li>
        <li>Equipos de esta factura</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    Bodega: {{$bodega->nombre;}}
                </h3>
                <h3 class="card-title">
                    <a href="{{url('/facturasEquipo', ['bodega_id' => $bodega->id])}}"><i class="text-20" data-feather="skip-back"></i>Regresar</a>
                </h3>
                <div style="width:100%; height: 35px;">
                    <h4 class="float-right">Factura No.{{$factura->numero;}} Serie: {{$factura->serie;}}</h4>
                </div>
                <div style="width:100%; height: 35px;">
                    <h4 class="float-right">Proveedor: {{$factura->proveedor;}}</h4>
                </div>
                <div style="width:100%; height: 35px;">
                    <h4 class="float-right">Fecha: {{$factura->fecha;}}</h4>
                </div>
                <div style="width:100%; height: 35px;">
                    <h3 class="float-right">Total: Q.{{$factura->total;}}</h3>
                </div>

                <h3 class="card-title">
                    <a href="javascript:"class="" onclick="newfloatv2(kad,undefined,undefined,undefined,'/productosFactura/{{$bodega->id}}/{{$factura->id}}');">
                        <i class="ion-ios7-plus-outline "></i>
                        &nbsp;&nbsp; Agregar equipo a esta factura
                    </a>
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="equipos_factura_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Codigo</th>
                                <th>Equipo</th>
                                <th>Costo del equipo</th>
                                <th>Medida</th>
                                <th>Cantidad</th>
                                <th>Costo por unidad</th>
                                <th>Precio al cliente por unidad</th>
                                <th>Categoria</th>
                                <th>Subcategoria</th>
                                <th>Porcentaje de depreciacion</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $criti = array(); @endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $value->equipo->codigo }}</td>
                                    <td>{{ $value->equipo->nombre }}</td>
                                    <td>Q. {{ number_format($value->costo_total, 2) }}</td>
                                    <td>{{ $value->medida->nombre }}</td>
                                    <td>{{ $value->cantidad }}</td>
                                    <td>Q. {{ number_format($value->costo_unidad, 2) }}</td>
                                    <td>Q. {{ number_format($value->equipo->precio_cliente_unidad, 2) }}</td>
                                    <td>{{ $value->categoria->category }}</td>
                                    <td>{{ $value->sub_categoria->subCategory }}</td>
                                    <td>{{ $value->equipo->porcentaje_depreciacion }} %</td>
                                    <td>
                                        <a href="javascript:" onclick="modifyfloat('{{$value->id}}',kad,criteria,undefined,undefined);$('.card-title')[1].innerHTML = 'Editar producto';" title="Editar">
                                            <i class="text-20"data-feather="edit-3"></i>
                                        </a>
                                        <a href="jsavascript:" onclick="deleteD('{{$value->id}}','{{ csrf_token() }}', 'destroy');" title="Eliminar">
                                            <i class="text-20"data-feather="trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                @php
                                $criti[$value->id] = [
                                        $value->equipo->codigo,
                                        $value->equipo->nombre,
                                        $value->equipo->descripcion,
                                        $value->equipo->categoria_id,
                                        $value->equipo->sub_categoria_id,
                                        $value->equipo->porcentaje_depreciacion,
                                        $value->equipo->medida_id,
                                        $value->cantidad,
                                        $value->costo_unidad,
                                        $value->equipo->precio_cliente_unidad,
                                        $value->costo_total
                                    ];
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right">
                            <h4 class="float-right" style="width:300px;"></h4>
                            <h3 class="float-right"> {{number_format($suma, 2)}} </h3>
                            <h4 class="float-right">Total Q. </h4>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right" style="margin-top:10px;">
                            @if ($suma == $factura->total)
                                <h5 class="w-badge badge-success">Valor de la factura cuadrado correctamente</h5>
                            @elseif ($suma > $factura->total)
                                <h5 class="w-badge badge-danger">La suma de los productos supera el valor declarado en la factura</h5>
                            @else
                                <h5 class="w-badge badge-warning">Faltan valores para cuadrar el valor de la factura</h5>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var criteria = @json($criti),
        equipos = @json($equipos_obj),
        unidades = @json($unidades_obj),
        conversiones = @json($conversiones),
        categorias = @json($categorias);

    function calcularCantidad(costo){
        let unidad_inicial = $('#medida_id').val(),
            cantidad = $('#cantidad_medida').val(),
            producto_id = $('#producto_id').val(),
            unidad_final = productos[producto_id].medida_final;
        $('#medida').val(unidades[unidad_final].nombre)

        let cantidad_final = 1,
            operacion = null,
            valor = 1;

        for(let i = 0; i < conversiones.length; i++){
            let temp = conversiones[i];

            if (unidad_inicial == temp.unidad_inicial_id && unidad_final == temp.unidad_final_id){
                operacion = temp.operacion
                valor = temp.valor
                break
            }
        }

        if (operacion == 'dividir'){
            // el calculo se hace aplicando la inversa de la operacion indicada
            cantidad_final = cantidad * valor
        }else{
            cantidad_final = cantidad / valor
        }

        $('#cantidad').val(cantidad_final)

        let tot = $('#valor_total').val(),
            uni = $('#costo_unitario').val();

        if (costo == 'unitario'){
            let res = uni * cantidad_final
            $('#valor_total').val(res)
        }else{
            let res = Math.round(((tot / cantidad_final) + Number.EPSILON) * 100) / 100
            $('#costo_unitario').val(res)
        }
    }

    function calcularCostos(opt){
        let costo = $('#costo').val(),
            cantidad = $('#cantidad').val(),
            costo_u = $('#costo_unidad').val();

        if (opt == 'cantidad' && cantidad > 0){
            costo_u = Math.round((costo / cantidad) * 100) / 100;
            $('#costo_unidad').val(costo_u.toFixed(2))
        }

        if (opt == 'costo_u'){
            cantidad = Math.round((costo / costo_u) * 100) / 100;
            $('#cantidad').val(cantida)
        }

        $('#costo').val(costo.toFixed(2))
    }

    function cambiarSubcategoria(elm){
        let val = elm.value,
            categoria = categorias[val]
            cad = '',
            subcategoria = categoria.subcategoria;

        for(let s in subcategoria){
            cad += `<option value=${s}>${subcategoria[s]}</option>`
        }

        $('#sub_categoria_id').html(cad)
    }

</script>

@endsection

@section('bottom-js')
    <script>
        $('#equipos_factura_table').DataTable();
    </script>
@endsection
