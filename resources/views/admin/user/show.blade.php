@inject('helper', 'App\Http\Helpers\helpers')
@extends('admin.layouts.master')
@section('main-content')
@php $criti=[];@endphp
<script type="text/javascript">

var kad={ 0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
          1:{type:"txt",tl:"Nombre y apellido",id:"nomb",nm:"name",
              add:'onkeyup="username(this);"',elv:"0"},
           2:{type:"txt",tl:"Usuario",id:"userex",nm:"usersys",elv:"1"},
           3:{type:"txt",tl:"Password",nm:"password",add:'maxlength="15"',add:"placeholder=\"escribir password\""},
           4:{type:"slct",tl:"Tipo de usuario",nm:"roleUS",vl:@json($roleUsers),elv:"3"},
           5:{type:"chkbxstyl",tl:"Esta actualmente",id:"activ",nm:"statusUs",
              vl:[["1",'<span id="stat"></span>']],elv:"4",
              add:'onclick="checar(this,0);"'},

       },
       ked={ 0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
             1:{type:"txt",tl:"Nombre de país",nm:"name",elv:"0"},
             2:{type:"txt",tl:"Código de país",nm:"code",elv:"1"},
             3:{type:"slct",tl:"Plataforma",nm:"platform",vl:[["","Elegir Plataforma"],["kindi","Kindi"],["horizum","Horizum"]],elv:"2"}
            };
// 6:{type:"slct",tl:"Tipo de usuario",nm:"country",vl:[["kindi","Kindi"],["horizum","Horizum"]],elv:"5"},
    valdis={clase:"red",text:1,checkbox:1};
</script>
   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
			     <h3 class="card-title">
              <a href="javascript:"class="" onclick="newfloatv2(kad);">
                <i class="ion-ios7-plus-outline "></i>
                &nbsp;&nbsp;
                Nuevo Usuario
              </a>
              @if(Helper::superAdmin())
                &nbsp;&nbsp;&nbsp;&nbsp;
                {{-- <a href="{{route('admin.newcountry.index')}}"class="btn btn-icon btn-vimeo" >
                  &nbsp;Ver países
                </a> --}}
              @endif
           </h3>
           <div class="card-options">
             <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
             <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
             <a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
           </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table i class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Rol</th>
                  <th>Estado</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>

                @foreach ($users as $user)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td >{{$user->name}}</td>
                    <td >{{$user->role->nameRole}}</td>
                    <td class="visible-lg">{{$user->statusUs}}</td>
                    <td class="td-actions  text-left">
                      <a href="javascript:">
{{-- {!!(Helper::superAdmin()?'<img src="'.asset('img_admin/flags/'.Helper::get_country($user->country)).'.svg" width="20">':'')!!} --}}
</a>
        {{-- @php var_dump($user->country,$user->get_platform()->first()->platform);@endphp --}}
                    <a href="javascript:">
                    {{-- {!!(isset($user->get_platform()->first()->platform)?'<img
                    src="'.asset('images/'.$user->get_platform()->first()->platform).'.jpg" width="20">':'')!!} --}}
                    </a>
                      <a  href="javascript:"
                          onclick="modifyfloat('{{$user->id}}',kad,criteria);">
                          <i class="text-20"data-feather="edit-3"></i>
                      </a>
                      <a  href="javascript:"
                          onclick="deleteD('{{$user->id}}','{{ csrf_token() }}');">
                          <i class="text-12" data-feather="trash"></i>
                      </a>
                    </td>
                  </tr>
                  <?php
                  $criti[$user->id]=array($user->name,
                                                 $user->usersys,
                                                 $user->password,
                                                 $user->roleUS,
                                                 array($user->statusUs));
                  ?>
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = @json($criti);
    </script>
    @component('components.messagesForm')
    @endcomponent
@endsection
