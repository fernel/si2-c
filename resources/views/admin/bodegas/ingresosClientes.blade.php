@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
    var kad={
                0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1:{type:"txt",tl:"Nombre del banco",nm:"nombre",elv:"0"},
                2:{type:"txt",tl:"Razón social del banco",nm:"razon",elv:"1"},
            },
        valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Bodegas</h1>
    <ul>
        <li><a href="{{url('/bodegas')}}">Inicio</a></li>
        <li><a href="{{url('/bodegasCliente', ['company_id' => $company_id])}}">Bodegas clientes</a></li>
        <li>Ingresos</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    Empresa: {{$company->company}}
                </h3>
                <h3 class="card-title">
                    Cliente: {{$cliente->clientName}}
                </h3>
                <h3 class="card-title">
                    Sede / bodega: {{$bodega->nombre}}
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="ingresos_clientes_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Codigo de producto</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Medida</th>
                                <th>Cantidad convertida</th>
                                <th>Medida final</th>
                                <th>Costo unitario</th>
                                <th>Valor total</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $criti = array(); @endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $value->producto->codigo }}</td>
                                    <td>{{ $value->producto->nombre }}</td>
                                    <td>{{ $value->cantidad_medida }}</td>
                                    <td>{{ $value->medida_id }}</td>
                                    <td>{{ $value->cantidad }}</td>
                                    <td>{{ $value->medida }}</td>
                                    <td>Q. {{ number_format($value->costo_unitario, 2) }}</td>
                                    <td>Q. {{ number_format($value->valor_total, 2) }}</td>
                                    <td>{{ $value->created_at->format('d/m/Y H:m') }}</td>
                                </tr>
                                @php $criti[$value->id] = []; @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  var criteria = @json($criti);
</script>

@endsection

@section('bottom-js')
    <script>
        $('#ingresos_clientes_table').DataTable();
    </script>
@endsection
