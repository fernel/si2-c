@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
    var kad={
                0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1:{type:"txt",tl:"Nombre del banco",nm:"nombre",elv:"0"},
                2:{type:"txt",tl:"Razón social del banco",nm:"razon",elv:"1"},
            },
        valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Bodegas de equipo</h1>
    <ul>
        <li><a href="{{url('/bodegasEquipo')}}">Inicio</a></li>
        <li>Ingresos</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    Bodega: {{$bodega->nombre}}
                </h3>
                <h3>
                    <a href="{{url('/facturasEquipo', ['bodega_id' => $bodega->id])}}" title="Facturas">
                        Ver facturas<i class="text-20"data-feather="trending-up"></i>
                    </a>
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="ingresos_equipos_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Codigo</th>
                                <th>Equipo</th>
                                <th>Costo del equipo</th>
                                <th>Medida</th>
                                <th>Cantidad</th>
                                <th>Costo por unidad</th>
                                <th>Precio al cliente por unidad</th>
                                <th>Categoria</th>
                                <th>Subcategoria</th>
                                <th>Porcentaje de depreciacion</th>
                                <th>Fecha de ingreso</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $criti = array(); @endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $value->equipo->codigo }}</td>
                                    <td>{{ $value->equipo->nombre }}</td>
                                    <td>Q. {{ number_format($value->costo_total, 2) }}</td>
                                    <td>{{ $value->medida->nombre }}</td>
                                    <td>{{ $value->cantidad }}</td>
                                    <td>Q. {{ number_format($value->costo_unidad, 2) }}</td>
                                    <td>Q. {{ number_format($value->equipo->precio_cliente_unidad, 2) }}</td>
                                    <td>{{ $value->categoria->category }}</td>
                                    <td>{{ $value->sub_categoria->subCategory }}</td>
                                    <td>{{ $value->equipo->porcentaje_depreciacion }} %</td>
                                    <td>{{ $value->created_at }}</td>
                                </tr>
                                @php $criti[$value->id] = []; @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  var criteria = @json($criti);
</script>

@endsection

@section('bottom-js')
    <script>
        $('#ingresos_equipos_table').DataTable();
    </script>
@endsection
