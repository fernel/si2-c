@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Bodegas</h1>
    <ul>
        <li><a href="{{url('/bodegas')}}">Inicio</a></li>
        <li>Ingresos</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    Bodega: {{$bodega->nombre}}
                </h3>
                <h3>
                    <a href="{{url('/facturas', ['bodega_id' => $bodega->id])}}" title="Facturas">
                        Ver facturas<i class="text-20"data-feather="trending-up"></i>
                    </a>
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="tabla_ingreso_productos" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Codigo de producto</th>
                                <th>Producto</th>
                                <th>Cantidad inicial</th>
                                <th>Medida inicial</th>
                                <th>Cantidad convertida</th>
                                <th>Medida final</th>
                                <th>Costo unitario en medida final</th>
                                <th>Valor total</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $value->producto->codigo }}</td>
                                    <td>{{ $value->producto->nombre }}</td>
                                    <td>{{ $value->cantidad_medida }}</td>
                                    <td>{{ isset($value->medida_element->nombre) ? $value->medida_element->nombre : '' }}</td>
                                    <td>{{ $value->cantidad }}</td>
                                    <td>{{ isset($value->medida_final_element->nombre) ? $value->medida_final_element->nombre : '' }}</td>
                                    <td>Q. {{ number_format($value->costo_unitario, 2) }}</td>
                                    <td>Q. {{ number_format($value->valor_total, 2) }}</td>
                                    <td>{{ $value->created_at->format('d/m/Y') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection


@section('bottom-js')
    <script>
        $('#tabla_ingreso_productos').DataTable();
    </script>
@endsection
