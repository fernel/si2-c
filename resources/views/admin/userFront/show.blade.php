@inject('helper', 'App\Http\Helpers\helpers')
@extends('admin.layouts.master')
@section('main-content')
@php $criti=[];@endphp
<script type="text/javascript">

var kad={
        0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
        1:{type:"txt",tl:"Nombre y apellido",id:"nomb",nm:"name", add:'onkeyup="username(this);"',elv:"0"},
        2:{type:"txt",tl:"Usuario",id:"userex",nm:"usersys",elv:"1"},
        3:{type:"txt",tl:"Password",nm:"password",add:'maxlength="15"',add:"placeholder=\"escribir password\""},
        4:{type:"slct",tl:"Departamento",id:"departamento_id",nm:"departamento_id",vl:@json($departamentos),elv:"3", add:`onchange="selectMe()"`},
        5:{type:"slct",tl:"Sección",nm:"seccion_id",id:"seccion_id",vl:@json($secciones),elv:"4", add:`onchange="selectMe()"`},
        6:{type:"slct",tl:"Oficina",nm:"oficina_id",id:"oficina_id",vl:@json($oficinas),elv:"5", add:`onchange="selectMe()"`},
        7:{type:"slct",tl:"Delegación",nm:"delegacion_id",id:"delegacion_id",vl:@json($delegaciones),elv:"6", add:`onchange="selectMe()"`},
        8:{type:"chkbxstyl",tl:"Estado (activo o inactivo)",id:"activ",nm:"status",vl:[["1",'<span id="stat"></span>']],elv:"7",add:'onclick="checar(this,0);"'},
        9:{type:"label",tl:"",inner:"Permisos", add:"text-align:center;"},
        @php
            $i = 10;
            foreach($access as $a){
                echo $i.':{type:"checkbox_custom",tl:"'.$a["name"].'",id:"json_permissions",nm:"json-'.$a["route"].'",vl:[["Leer","Leer"],["Editar","Editar"],["Eliminar","Eliminar"]],elv:"'.($i - 2).'"},';
                $i++;
            }
        @endphp


       },
       ked={ 0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
             1:{type:"txt",tl:"Nombre de país",nm:"name",elv:"0"},
             2:{type:"txt",tl:"Código de país",nm:"code",elv:"1"},
             3:{type:"slct",tl:"Plataforma",nm:"platform",vl:[["","Elegir Plataforma"],["kindi","Kindi"],["horizum","Horizum"]],elv:"2"}
            };
// 6:{type:"slct",tl:"Tipo de usuario",nm:"country",vl:[["kindi","Kindi"],["horizum","Horizum"]],elv:"5"},
    valdis={clase:"red",text:1,checkbox:1};
</script>
   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
			     <h3 class="card-title">
              <a href="javascript:"class="" onclick="newfloatv2(kad);">
                <i class="ion-ios7-plus-outline "></i>
                &nbsp;&nbsp;
                Nuevo Usuario
              </a>
              @if(Helper::superAdmin())
                &nbsp;&nbsp;&nbsp;&nbsp;
                {{-- <a href="{{route('admin.newcountry.index')}}"class="btn btn-icon btn-vimeo" >
                  &nbsp;Ver países
                </a> --}}
              @endif
           </h3>
           <div class="card-options">
             <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
             <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
             <a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
           </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table i class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Departamento</th>
                  <th>Sección</th>
                  <th>Oficina</th>
                  <th>Delegación</th>
                  <th>Estado</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>

                @foreach ($users as $user)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ isset($user->departamento->nombre) ? $user->departamento->nombre : '' }}</td>
                    <td>{{ isset($user->seccion->nombre) ? $user->seccion->nombre : '' }}</td>
                    <td>{{ isset($user->oficina->nombre) ? $user->oficina->nombre : '' }}</td>
                    <td>{{ isset($user->delegacion->nombre) ? $user->delegacion->nombre : '' }}</td>
                    <td class="visible-lg">{{ $user->status == 0 ? "Inactivo" : "Activo" }}</td>
                    <td class="td-actions  text-left">
                      <a href="javascript:">
{{-- {!!(Helper::superAdmin()?'<img src="'.asset('img_admin/flags/'.Helper::get_country($user->country)).'.svg" width="20">':'')!!} --}}
</a>
        {{-- @php var_dump($user->country,$user->get_platform()->first()->platform);@endphp --}}
                    <a href="javascript:">
                    {{-- {!!(isset($user->get_platform()->first()->platform)?'<img
                    src="'.asset('images/'.$user->get_platform()->first()->platform).'.jpg" width="20">':'')!!} --}}
                    </a>
                      <a  href="javascript:"
                          onclick="modifyfloat('{{$user->id}}',kad,criteria);cargar_valores('{{$user->id}}');">
                          <i class="text-20"data-feather="edit-3"></i>
                      </a>
                      <a  href="javascript:"
                          onclick="deleteD('{{$user->id}}','{{ csrf_token() }}');">
                          <i class="text-12" data-feather="trash"></i>
                      </a>
                    </td>
                  </tr>
                  <?php
                      $criti[$user->id]=array(  $user->name,
                                                $user->usersys,
                                                $user->password,
                                                $user->departamento_id,
                                                $user->seccion_id,
                                                $user->oficina_id,
                                                $user->delegacion_id,
                                                [$user->status],
                                            );
                      foreach($user->permissions_array as $v){
                          $criti[$user->id][] = $v;
                      }

                  ?>
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    <script type="text/javascript">
        criteria = @json($criti);
        departamentos = @json($departamentos_obj);

        function selectMe(){
            let dep = $('#departamento_id').val(),
            sec = $('#seccion_id').val(),
            ofi = $('#oficina_id').val(),
            del = $('#delegacion_id').val(),
            cad1 = '<option value="0">---</option>',
            cad2 = '<option value="0">---</option>',
            cad3 = '<option value="0">---</option>';
            let secciones = departamentos[dep] != undefined ? (departamentos[dep]['secciones'] != undefined ? departamentos[dep]['secciones'] : []) : [];
            for(i in secciones){
                cad1 += `<option value="${i}" ${sec == i ? 'selected' : ''}>${secciones[i].nombre}</option>`

                if (sec == i){
                    let oficinas = secciones[sec]['oficinas'],
                    delegaciones = secciones[sec]['delegaciones'];

                    for(j in oficinas){
                        cad2 += `<option value="${j}" ${ofi == j ? 'selected' : ''}>${oficinas[j].nombre}</option>`
                    }

                    for(j in delegaciones){
                        cad3 += `<option value="${j}" ${del == j ? 'selected' : ''}>${delegaciones[j].nombre}</option>`
                    }
                }
            }

            $('#seccion_id').html(cad1)
            $('#oficina_id').html(cad2)
            $('#delegacion_id').html(cad3)
        }

        function cargar_valores(id){
            let obj = criteria[id]
            $('#departamento_id').val(obj[3])
            selectMe()
            $('#seccion_id').val(obj[4])
            selectMe()
            $('#oficina_id').val(obj[5])
            selectMe()
            $('#delegacion_id').val(obj[6])
            selectMe()
        }

    </script>

    @component('components.messagesForm')
    @endcomponent

@endsection
