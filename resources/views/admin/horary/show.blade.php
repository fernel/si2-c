@extends('admin.layouts.master')
@section('main-content')
<script type="text/javascript">
  var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
          1:{type:"slct",tl:"Día de la semana",nm:"day",elv:"0",vl:[['Lunes', 'Lunes'],['Martes','Martes'],['Miércoles','Miércoles'],['Jueves','Jueves'],['Viernes','Viernes'],['Sábado','Sábado'],['Domingo','Domingo']],nxt:12},
          2:{type:"time",tl:"Hora de entrada",nm:"entry",elv:"1",nxt:6},
          3:{type:"time",tl:"Hora de salida",nm:"exit",elv:"2",nxt:6},
          };
  var conform={action:"{{route('admin.permissions.store')}}"}
    valdis={clase:"red",text:1,checkbox:1,radio:1};
</script>
@php
    $criti = [];
@endphp

<div class="breadcrumb">
    <h1 class="mr-2">{{$breadcrumb['base']}}</h1>
    <ul>
        <li> <a href="{{url($breadcrumb['inicio'])}}"> Inicio</a> </li>
        <li>Horarios</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
		  <h3 class="card-title">
            Horarios de: <b>{{$name}}</b>
          </h3>
          <h5><a href="javascript:"class="" onclick="newfloatv2(kad);"> <i class="ion-ios7-plus-outline "></i>&nbsp;&nbsp;Agregar horario </a></h5>
				</div>

        <div class="card-body">
          <div class="table-responsive">
            <table id="horarios_table" class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Día</th>
                      <th>Hora de entrada</th>
                      <th>Hora de salida</th>
                      <th>Opciones</th>
                    </tr>
                  </thead>

                  <tbody>
                    @foreach ($files as $f)
                      <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td class="hidden-xs">{{$f->day}}</td>
                        <td class="hidden-xs">{{$f->entry}}</td>
                        <td class="hidden-xs">{{$f->exit}}</td>

                        <td>

                          <a href="javascript:" class="" onclick="modifyfloat('{{$f->id}}',kad,criteria);" title="Editar archivo">
                              <i class="text-20"data-feather="edit-3"></i>
                          </a>

                          <a href="javascript:"onclick="deleteD('{{$f->id}}','{{ csrf_token() }}');" title="Eliminar archivo">
                              <i class="text-12" data-feather="trash"></i>
                          </a>
                        </td>
                      </tr>
                      @php
                        $criti[$f->id] = [$f->day, $f->entry, $f->exit];
                      @endphp

                    @endforeach
    		       </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = <?=json_encode($criti)?>;
      function fillimg(sto)
      {
        $("#imag").val(sto);
        flotantecloseblank();
      }
    </script>
    @component('components.messagesForm')
    @endcomponent
@endsection

@section('bottom-js')
    <script>
        $('#horarios_table').DataTable();
    </script>
@endsection
