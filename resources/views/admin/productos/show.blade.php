@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
        1:{type:"txt",tl:"Código",nm:"codigo",elv:"0"},
        2:{type:"txt",tl:"Nombre",nm:"nombre",elv:"1"},
        3:{type:"txtarea",tl:"Descripción",nm:"descripcion",elv:"2"},
        4:{type:"slct",tl:"Categoría",nm:"categoria_id",elv:"3",nxt:6,vl:@json($categorias), add:'onclick="selectCategoria(this)"'},
        5:{type:"slct",tl:"Subcategoría",id:"sub_categoria_id",nm:"sub_categoria_id",elv:"4",nxt:6,vl:@json($sub_categorias_arr)},
        6:{type:"slct",tl:"Medida final",nm:"medida_final_id",elv:"5",vl:@json($unidades_arr)},
        7:{type:"nbr",tl:"Cantidad mínima aceptable",nm:"cantidad_minima_aceptable",elv:"6",nxt:6,vl:'0'},
        8:{type:"nbr",tl:"Porcentaje de ganancia (%)",nm:"porcentaje_ganancia",elv:"7",nxt:6,vl:'0'},
        9:{type:"formato_moneda",tl:"Precio a cliente",nm:"precio_cliente",elv:"8",nxt:6,vl:'0',add:'onchange="formato_moneda(this)"'},
    },
    valdis={clase:"red",text:1,select:1,number:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Productos</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    <a href="javascript:" onclick="newfloatv2(kad)">
                        <i class="ion-ios7-plus-outline "></i>
                        &nbsp;&nbsp;Nuevo producto
                    </a>
                </h3>

			</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="productos_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Categoría</th>
                                <th>Subcategoría</th>
                                <th>Precio cliente</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $criti=array(); @endphp
                            @foreach ($data as $dataRes)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td > {{$dataRes->codigo}}</td>
                                    <td > {{$dataRes->nombre}}</td>
                                    <td > {{isset($dataRes->categoria_element->category) ? $dataRes->categoria_element->category : ''}}</td>
                                    <td > {{isset($dataRes->sub_categoria_element->subCategory) ? $dataRes->sub_categoria_element->subCategory : ''}}</td>
                                    <td > Q {{number_format($dataRes->precio_cliente, 2)}}</td>

                                    <td class="td-actions  text-left">
                                      <a href="javascript:" onclick="modifyfloat('{{$dataRes->id}}',kad,criteria)">
                                        <i class="text-20"data-feather="edit-3"></i>
                                      </a>
                                      <a href="javascript:"
                                      onclick="deleteD('{{$dataRes->id}}','{{ csrf_token() }}');">
                                      <i class="text-12" data-feather="trash"></i></a>
                                    </td>
                                    {{-- <td > <a href="{{route("admin.products.edit",$dataRes->id)}}"class="btn btn-sm btn-primary">
                                      <i class="fa fa-eye"></i></a> </td> --}}
                                </tr>
                                @php $criti[$dataRes->id] = [
                                        $dataRes->codigo,
                                        $dataRes->nombre,
                                        $dataRes->descripcion,
                                        $dataRes->categoria_id,
                                        $dataRes->sub_categoria_id,
                                        $dataRes->medida_final_id,
                                        $dataRes->cantidad_minima_aceptable,
                                        $dataRes->porcentaje_ganancia,
                                        $dataRes->precio_cliente,
                                    ];
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var criteria = @json($criti),
        subcategorias = @json($subcategorias);

    function selectCategoria(elm){
        let cad = '',
            val = elm.value
        for(let n in subcategorias[val]){
            let obj = subcategorias[val][n]
            cad += `<option value="${obj.id}">${obj.subCategory}</option>`
        }
        $('#sub_categoria_id').html(cad)
    }

</script>

@endsection

@section('bottom-js')
    <script>
        $('#productos_table').DataTable();
    </script>
@endsection
