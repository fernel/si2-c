@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
    var kad={
                0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1:{type:"txt",tl:"Equipo",id:"equipo_title",nm:"equipo_title",elv:"0",add:'readonly'},
                2:{type:"hddn",id:"equipo_id",nm:"equipo_id",elv:"1"},
                3:{type:"slct",tl:"Cargo a cliente",nm:"asignado_a_cliente_id",vl:@json($clientes),elv:"2", nxt:6},
                4:{type:"slct",tl:"Asignado a colaborador",nm:"asignado_a_colaborador_id",vl:@json($colaboradores),elv:"3", nxt:6},
                5:{type:"txtarea",tl:"Comentario (opcional)",nm:"comentario", add:'omit="T"'},
                6:{type:"hddn",nm:"tipo_movimiento",vl:"egreso"}
            },
        kad2 = {
                0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1:{type:"txt",tl:"Equipo",id:"equipo_title",nm:"equipo_title",elv:"0",add:'readonly'},
                2:{type:"hddn",id:"equipo_id",nm:"equipo_id",elv:"1"},
                3:{type:"dtmpckr",tl:"Fecha de ingreso",nm:"fecha_movimiento"},
                4:{type:"label",tl:"Uso"},
                5:{type:"txt",tl:"Medida de uso",nm:"medida",elv:"2", nxt:6, add:`readonly`},
                6:{type:"nbr",tl:"Cantidad",id:"cantidad",nm:"cantidad", nxt:6, add:`min=0 oninput="calcularTotal(this)"`},
                7:{type:"txt",tl:"Precio por unidad Q.",id:"precio_unidad",nm:"precio_unidad",elv:"3", nxt:6, add:`readonly`},
                8:{type:"hddn",id:"costo_unidad",nm:"costo_unidad",elv:"4"},
                9:{type:"txt",tl:"Total Q.",id:"precio_total",nm:"precio_total", nxt:6, add:`readonly`},
               10:{type:"txtarea",tl:"Comentario (opcional)",nm:"comentario"},
               11:{type:"hddn",nm:"tipo_movimiento",vl:"ingreso"}
            },
        kad3={
                    0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                    1:{type:"txt",tl:"Equipo",id:"equipo_title",nm:"equipo_title",elv:"0",add:'readonly'},
                    2:{type:"hddn",id:"equipo_id",nm:"equipo_id",elv:"1"},
                    3:{type:"txtarea",tl:"Motivo (opcional)",nm:"comentario", add:'omit="T"'},
                    4:{type:"hddn",nm:"tipo_movimiento",vl:"baja"}
                },
        valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Bodegas</h1>
    <ul>
        <li><a href="{{url('/bodegasEquipo')}}">Inicio</a></li>
        <li>Stock</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    Bodega: {{$bodega->nombre;}}
                </h3>
                <h3 class="card-title">
                    <a href="{{url('/bodegasEquipo')}}"><i class="text-20" data-feather="skip-back"></i>Regresar</a>
                </h3>

                <h3 class="card-title">
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="stock_productos_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Código</th>
                                <th>Equipo</th>
                                <th>Estatus</th>
                                <th>Medida</th>
                                <th>Disponibles</th>
                                <th>Costo</th>
                                <th>Precio cliente</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $criti = array(); $criti2 = array(); $criti3 = array();@endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $value->equipo->codigo }}</td>
                                    <td>{{ $value->equipo->nombre }}</td>
                                    <td><span class="w-badge badge-{{$value->estado == 'disponible' ? 'success' : ($value->estado == 'asignado' ? 'warning' : 'danger') }}">{{ $value->estado }}</span></td>
                                    <td>{{ $value->medida }}</td>
                                    <td>{{ $value->cantidad }}</td>
                                    <td>Q. {{ number_format($value->costo_unitario, 2) }}</td>
                                    <td>Q. {{ number_format($value->equipo->precio_cliente_unidad, 2) }}</td>
                                    <td>
                                    @if($value->estado == 'disponible')
                                        <a href="javascript:" onclick="modifyfloat('{{$value->equipo->id}}',kad,criteria);$('#verifyModalContent_title').html('Egreso de equipo');" title="Egreso de equipo">
                                            <i class="text-20"data-feather="trending-up"></i>
                                        </a>

                                        <a href="javascript:" onclick="modifyfloat('{{$value->equipo->id}}',kad3,criteria3,'literalUrl','/stockEmpresasEquipoBaja/{{$bodega_id}}/{{$value->equipo->id}}');$('#verifyModalContent_title').html('Baja de equipo');" title="Dar de baja equipo">
                                            <i class="text-20"data-feather="download"></i>
                                        </a>
                                    @else
                                        <a href="javascript:" onclick="modifyfloat('{{$value->equipo->id}}',kad2,criteria2,'literalUrl','/stockEmpresasEquipoIngreso/{{$bodega_id}}/{{$value->equipo->id}}');$('#verifyModalContent_title').html('Ingreso de equipo');" title="Ingreso de equipo">
                                            <i class="text-20"data-feather="trending-down"></i>
                                        </a>
                                    @endif

                                    </td>
                                </tr>
                                @php
                                $criti[$value->equipo->id] = [
                                        $value->equipo->nombre,
                                        $value->equipo->id,
                                    ];

                                $criti2[$value->equipo->id] = [
                                        $value->equipo->nombre,
                                        $value->equipo->id,
                                        $value->medida,
                                        number_format($value->equipo->precio_cliente_unidad, 2),
                                        $value->costo_unitario
                                    ];

                                $criti3[$value->equipo->id] = [
                                        $value->equipo->nombre,
                                        $value->equipo->id,
                                    ];
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- -------------------------- Equipos de baja --------------------------------------- --}}
<div class="row mt-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Equipos de baja
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="stock_productos_baja_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Código</th>
                                <th>Equipo</th>
                                <th>Estatus</th>
                                <th>Fecha de baja</th>
                                <th>Comentario</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($baja as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $value->equipo->codigo }}</td>
                                    <td>{{ $value->equipo->nombre }}</td>
                                    <td><span class="w-badge badge-warning">{{ $value->estado }}</span></td>
                                    <td>{{ $value->fecha->format('d/m/Y H:m') }}</td>
                                    <td>{{ $value->comentario }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var criteria = @json($criti),
        criteria2 = @json($criti2),
        criteria3 = @json($criti3),
        productos = @json($data);

    function calcularTotal(elm){
        let cantidad = elm.value,
            costo = $('#precio_unidad').val(),
            total = 0;
        console.log(cantidad, costo, total)
        total = Math.round((cantidad * costo) * 100) / 100
        $('#precio_total').val(total.toFixed(2))
    }
</script>

@endsection

@section('bottom-js')
    <script>
        $('#stock_productos_table').DataTable();
        $('#stock_productos_baja_table').DataTable();
    </script>
@endsection
