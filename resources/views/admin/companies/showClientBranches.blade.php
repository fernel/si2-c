@extends('admin.layouts.master')
@section('main-content')
<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
         1:{type:"txt",tl:"Nombre de sede",nm:"namebranch",elv:"0"},
         2:{type:"txt",tl:"Direccion",nm:"address",elv:"1"},
         3:{type:"txt",tl:"Latitud",nm:"latitude",elv:"2"},
         4:{type:"txt",tl:"Longitud",nm:"longitude",elv:"3"},
         5:{type:"txt",tl:"Telefono",nm:"phone1",elv:"4"},
         6:{type:"hddn",vl:"{{$idm}}",nm:"idClient"},
       };
    valdis={clase:"red",text:1};

</script>

<div class="breadcrumb">
    <h1 class="mr-2">Clientes</h1>
    <ul>
        <li><a href="{{url("clients")}}">Inicio</a></li>
        <li>Sedes</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
		  <h3>
              Cliente: <b>{{$clientName}}</b>
          </h3>
          <h3 class="card-title">
              <a href="javascript:"class="" onclick="newfloatv2(kad,null,'{{route('admin.clientsbranch.store')}}');">
                  <i class="ion-ios7-plus-outline "></i>
                  &nbsp;&nbsp;Nueva sede
              </a>
          </h3>
		</div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="sedes_table" class="table" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Sede</th>
                  <th>Direccion</th>
                  <th>Latitud</th>
                  <th>Longitud</th>
                  <th>Telefono</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                @php $criti = $crite = []; @endphp
                @foreach ($data as $dataRes)
                  <tr>
                      <td>{{ $loop->index + 1 }}</td>

                  <td > {{$dataRes->namebranch}}</td>
                  <td > {{$dataRes->address}}</td>
                  <td > {{$dataRes->latitude}}</td>
                  <td > {{$dataRes->longitude}}</td>
                  <td > {{$dataRes->phone1}}</td>

                  <td class="td-actions text-left table-subtitle">

                    <a href="{{route('admin.clientsbrancharea.show', $dataRes->id)}}"class="" title="Administrar áreas">
                        <i class="text-20" data-feather="layers"></i>
                    </a>

                    <a  href="javascript:"
                        onclick="modifyfloat('{{$dataRes->id}}',kad,criteria,'literalUrl','{{route('admin.clientsbranch.update',$dataRes->id)}}');">
                        <i class="text-20"data-feather="edit-3"></i>
                    </a>

                    <a  href="javascript:"
                        onclick="deleteD('{{$dataRes->id}}','{{ csrf_token() }}');">
                        <i class="text-12" data-feather="trash"></i>
                    </a>

                  </td>
                  </tr>
                  @php $criti[$dataRes->id] = [$dataRes->namebranch,
                                               $dataRes->address,
                                               $dataRes->latitude,
                                               $dataRes->longitude,
                                               $dataRes->phone1,]; @endphp
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = @json($criti);
          criterie = @json($crite);

    </script>

@endsection

@section('bottom-js')
    <script>
        $('#sedes_table').DataTable();
    </script>
@endsection
