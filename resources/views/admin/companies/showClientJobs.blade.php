@extends('admin.layouts.master')
@section('main-content')
<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
         1:{type:"slct",tl:"Puesto",nm:"idJobCatalog",vl:@json($jobsCatalog),elv:"0"},

         10:{type:"hddn",vl:"{{$idm}}",nm:"idClient"},
       },
       ked={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1:{type:"txt",tl:"Nombre de tarea",nm:"title",elv:"0"},
                2:{type:"hddn",vl:"",nm:"idJob",id:"idJob"},
                3:{type:"hddn",vl:"{{$idm}}",nm:"idClients"},
               };
    valdis={clase:"red",text:1};

</script>
   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
					<h3 class="card-title">
            <a href="javascript:"class="" onclick="newfloatv2(kad,null,'{{route('admin.clientsjobs.store')}}');">
            <i class="ion-ios7-plus-outline "></i>
            &nbsp;&nbsp;Nuevo puesto
          </a>
          </h3>
					<div class="card-options">
						<a href="#" class="card-options-collapse" data-toggle="card-collapse">
              <i class="fe fe-chevron-up"></i>
            </a>
						<a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen">
              <i class="fe fe-maximize"></i>
            </a>
						<a href="#" class="card-options-remove" data-toggle="card-remove">
              <i class="fe fe-x"></i>
            </a>
					</div>
				</div>
        <div class="card-body">
          <div class="table-responsive">
            <table i class="table" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Puesto</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                @php $criti = $crite = []; @endphp
                @foreach ($data as $dataRes)
                  <tr>
                      <td>{{ $loop->index + 1 }}</td>

                  <td > {{$dataRes->getJobName()->first()->nombre}}</td>

                  <td class="td-actions text-left table-subtitle">
                    <a  href="javascript:"
                      onclick="newfloatv2(ked,null,'{{route('admin.clientsjobstask.store')}}');$('#idJob').val('{{$dataRes->id}}');">
                        <i class="fa fa-plus "></i>
                    </a>
                    <a  href="javascript:"
                        onclick="modifyfloat('{{$dataRes->id}}',kad,criteria,'literalUrl','{{route('admin.clientsjobs.update',$dataRes->id)}}');">
                        <i class="pe-7s-note "></i>
                    </a>
                    <a  href="javascript:"
                        onclick="deleteD('{{$dataRes->id}}','{{ csrf_token() }}');">
                        <i class="text-12" data-feather="trash-2"></i>
                    </a>

                  </td>
                  </tr>
                  @php $tareas = $dataRes->getTasks()->get();@endphp
                  @if(!empty( $tareas))
                    @foreach( $tareas AS $valus)
                      <tr class="subparone l{{$dataRes->id}}k table-subtitle" style="background-color:#eff7ff;">
                        <td>{{ $loop->index + 1 }}</td>
                        <td >&#8627;{{$valus->title}}</td>
                        <td class="td-actions text-left table-subtitle">
                          <a  href="javascript:"
                              onclick="modifyfloat('{{$valus->id}}',ked,criterie,'literalUrl','{{route('admin.clientsjobstask.update',$valus->id)}}');">
                              <i class="pe-7s-note "></i>
                          </a>
                          <a  href="javascript:"
                              onclick="deleteD('{{$valus->id}}','{{ csrf_token() }}');">
                              <i class="pe-7s-trash "></i>
                          </a>
                        </td>
                      </tr>
                      @php $crite[$valus->id]=[$valus->title,$valus->id]; @endphp
                    @endforeach
                  @endif
                  @php $criti[$dataRes->id] = [$dataRes->idJobCatalog]; @endphp
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = @json($criti);
          criterie = @json($crite);

    </script>

@endsection
