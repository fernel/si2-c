@extends('admin.layouts.master')

@section('main-content')

@php  $creat=array();
  $colors = ["ausencia"=>'#f14848',"sin horario"=>'#a99944',"asistencia"=>'#38b6f5',
            "Permiso"=>'#ecffef',"Vacaciones"=>'#f3fbff',"Iggs"=>'#f3fbff'];
@endphp
<div class="breadcrumb">
    <h1 class="mr-2"> <a href="{{route('admin.company.index')}}">Empresas</a> </h1>
    <ul>
        <li><a href="{{route('admin.colaboradoresCompany',$idCompany)}}">Colaboradores</a></li>
        <li>Reporte colaborador</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
  <div class="col-12">
    <div class="card o-hidden mb-4">
      <div class="card-header bg-transparent">
          <div class="card-title mb-0">Datos del empleado</div>
      </div>
      <div class="card-body">
          <div class="row">
            <div class="col-4">
                <label  style="font-weight:bold;">Código:</label>
                {{$colabInfo->id }}<br>
                <label  style="font-weight:bold;">Nombre: </label>
                {{$colabInfo->nombres.' '.$colabInfo->apellidos }}<br>
                <label  style="font-weight:bold;">Estado: </label>
                {{$colabInfo->estatus}}
            </div>
            <div class="col-4">
              <label  style="font-weight:bold;">Puesto: </label>
              {{$colabInfo->puesto }}<br>
            </div>
          </div>
      </div>
  </div>
    <div class="card o-hidden mb-4">
      <div class="card-header bg-transparent">
          <div class="card-title mb-0">Reporte</div>
          <div class="col-md-1 form-group mb-3" style="float:left;">
            <select class="form-control" onchange="filter();" id="year" >
              {{-- {{dd($years)}} --}}
              @if(count($years)>0)
                @foreach($years AS $vls)
                  <option value="{{$vls->year}}" {{($yr==$vls?'selected':'')}}>{{$vls->year}}</option>
                @endforeach
              @endif
            </select>
          </div>
          <div class="col-md-1 form-group mb-3" style="float:left;">

            @php $yearTop = (date("Y")==$yr?date("W"):52); @endphp
            <select class="form-control" onchange="filter();" id="week">
              @if(count($years)>0)
                @foreach(range($yearTop,1) AS $vls)
                  <option value="{{$vls}}" {{($wk==$vls?'selected':'')}}>{{$vls}}</option>
                @endforeach
              @endif
            </select>
          </div>
        {{-- <div class="col-md-3 form-group mb-3" style="float:left;">
          <span class="badge badge-pill badge-outline-primary p-2 m-1" style="border-color:#7dd4ff;background-color:#f3fbff;color:gray;">Asistencia</span>
          <span class="badge badge-pill badge-outline-primary p-2 m-1" style="border-color:#ff7a7a;background-color:#fff0f0;color:gray;">Ausencia</span>
          <span class="badge badge-pill badge-outline-primary p-2 m-1" style="border-color:#ffe452;background-color:#fffdf2;color:gray;">Día no laboral</span>

          <span class="badge badge-pill badge-outline-primary p-2 m-1" style="border-color: #6eda66;background-color: #ecffef;color:gray;">Permiso</span>
          <span class="badge badge-pill badge-outline-primary p-2 m-1" style="border-color:#ffe452;background-color:#fffdf2;color:gray;">Vacaciones</span>
          <span class="badge badge-pill badge-outline-primary p-2 m-1" style="border-color:#ffe452;background-color:#fffdf2;color:gray;">Iggs</span>
        </div> --}}
      </div>

    <div class="card-body">
      <div class="row">
        <div class="col-12" style="overflow-x:scroll">
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th>Fecha</th>
                <th>Dia laborado</th>
                <th>Intermedio</th>
                <th>Extras</th>
                <th>Viáticos</th>
                <th>Metas</th>
                <th>Opciones</th>

              </tr>
            </thead>
            <tbody>

          @foreach($data AS $valuesAssocs)
            @foreach($valuesAssocs AS $valuesDays)
                @foreach($valuesDays AS $elems)

                <form class="" action="{{route('admin.colabCompanyWorkSave',[$idColab,$idCompany])}}" method="post">
                <tr >
                  <td>{{$elems["dayWeek"]}}<br>
                    @if(!empty($elems["dateDay"]))
                    @php
                    $mes =  ["1"=>"Enero","2"=>"Febrero",
                            "3"=>"Marzo","4"=>"Abril",
                            "5"=>"Mayo","6"=>"Junio","7"=>"Julio",
                            "8"=>"Agosto","9"=>"Septiembre","10"=>"Octubre",
                            "11"=>"Noviembre","12"=>"Diciembre"
                          ];
                        $fech = date("d/m/Y",strtotime($elems["dateDay"]));
                          $fech = explode("/",$fech);
                    @endphp
                             {{$fech[0]." ".$mes[$fech[1]]." ".$fech[2]}} <br>
                               {{$elems["clientName"]}}<br>
                               <span class="badge badge-pill badge-info"
                               style="background-color:{{isset($colors[strtolower($elems['estado'])])?$colors[strtolower($elems['estado'])]:'#47404f!important'}}">
                               {{$elems['estado']}}
                               @if(!empty($elems['descripDay']))
                                 <a href="javascript:" onclick="showDesc('{{$elems['descripDay']}}');"
                                 class="text-white"
                                 >(ver)</a>
                               @endif
                             </span>
                    @else
                      <input type="date" name="dateDay" value="">
                      <input type="hidden" name="userId" value="{{$idUser}}">
                      <input type="hidden" name="dayWeek" value="{{$elems["dayWeek"]}}">
                      @if($elems["estado"]=='Ausensia'||$elems["estado"]=='Permiso')
                        <input type="hidden" name="clientName" value="{{$elems["clientName"]}}">
                        <input type="hidden" name="idBranch" value="{{$elems["idBranch"]}}">
                        <input type="hidden" name="idClient" value="{{$elems["idClient"]}}">
                        <input type="hidden" name="idLink" value="{{$elems["idLink"]}}">
                        <input type="hidden" name="idDay" value="{{$elems["idDay"]}}">
                        <input type="hidden" name="idArea" value="{{$elems["idArea"]}}">
                        <input type="hidden" name="idCompany" value="{{$elems["idCompany"]}}">
                        <input type="hidden" name="entry" value="{{$elems["entry"]}}">
                        <input type="hidden" name="exit" value="{{$elems["exit"]}}">
                        <input type="hidden" name="dateDay" value="{{$elems["dateDay"]}}">
                      @elseif($elems["estado"]=='Sin horario')
                        <input type="hidden" name="clientName" value="{{$elems["clientName"]}}">
                        <input type="hidden" name="idBranch" value="{{$elems["idBranch"]}}">
                        <input type="hidden" name="idClient" value="{{$elems["idClient"]}}">
                        <input type="hidden" name="idLink" value="{{$elems["idLink"]}}">
                        <input type="hidden" name="idArea" value="{{$elems["idArea"]}}">
                        <input type="hidden" name="idCompany" value="{{$elems["idCompany"]}}">
                        <input type="hidden" name="dateDay" value="{{$elems["dateDay"]}}">

                      @endif
                     @endif
                  </td>
                  <td>
                    @csrf
                      <input type="hidden" name="idPer" value="{{$elems["id"]}}">
                      <span style="font-size:10px;">Entrada</span> <br>
                      <input type="time" name="hourEnter" onchange="changColor(this)"
                      {{(empty($elems["hourEnter"])?'style=color:#c5c5c5':'')}}
                        value="{{!empty($elems["hourEnter"])?date("H:i",strtotime($elems["hourEnter"])):'00:00'}}">
                      <br>
                      <span style="font-size:10px;">Atraso:
                      {{!empty($elems["outTimeEnter"])?date("H:i",strtotime($elems["outTimeEnter"])):'00:00'}}</span>
                      <br>
                      <span style="font-size:10px;">Salida</span> <br>
                      <input type="time" name="hourExit"  onchange="changColor(this)"
                        {{(empty($elems["hourExit"])?'style=color:#c5c5c5':'')}}
                        value="{{!empty($elems["hourExit"])?date("H:i",strtotime($elems["hourExit"])):'00:00'}}">
                      <br>
                      <span style="font-size:10px;">Horas laboradas:
                      {{!empty($elems["hourDay"])?date("H:i",strtotime($elems["hourDay"])):'00:00'}}</span>
                  </td>

                  <td><span style="font-size:10px;">Salida</span> <br>
                    <input type="time" name="hourEnterBetw"  onchange="changColor(this)"
                    {{(empty($elems["hourEnterBetw"])?'style=color:#c5c5c5':'')}}
                    value="{{!empty($elems["hourEnterBetw"])?date("H:i",strtotime($elems["hourEnterBetw"])):'00:00'}}">
                   <br><span style="font-size:10px;">Entrada</span> <br>
                    <input type="time" name="hourExitBetw" onchange="changColor(this)"
                    {{(empty($elems["hourExitBetw"])?'style=color:#c5c5c5':'')}}
                    value="{{!empty($elems["hourExitBetw"])?date("H:i",strtotime($elems["hourExitBetw"])):'00:00'}}">
                  </td>
                  <td>
                    <span style="color:black;font-size:10px;">Hora extra</span> <br>
                    <input type="time" name="extraHourDay" {{(empty($elems["extraHourDay"])?'style=color:#c5c5c5':'')}}
                    value="{{!empty($elems["extraHourDay"])?date("H:i",strtotime($elems["extraHourDay"])):'00:00'}}">
                    <br>
                    <span style="color:black;font-size:10px;">Extra nocturna</span><br>
                    <input type="time" name="extraHourNight"  {{(empty($elems["extraHourDay"])?'style=color:#c5c5c5':'')}}
                    value="{{(!empty($elems["extraHourNight"])?date("H:i",strtotime($elems["extraHourDay"])):'00:00')}}">
                    <br>
                    <span style="color:black;font-size:10px;">Extra especial</span><br>
                    <input type="time" name="extraHourEspecial"  {{(empty($elems["extraHourEspecial"])?'style=color:#c5c5c5':'')}}
                    value="{{(!empty($elems["extraHourEspecial"])?date("H:i",strtotime($elems["extraHourEspecial"])):'00:00')}}">
                  </td>

                  <td> <input type="number" name="viatics" value="{{!empty($elems["viatics"])?$elems["viatics"]:0}}"
                    id="" style="width:80%" onchange="calcViatics(this);">
                  <br>
                  <span style="color:black;font-size:10px;">Adicionales</span>
                  <br>
                    Q.<input type="number" name="viaticsAdded" value="{{!empty($elems["viaticsAdded"])?$elems["viaticsAdded"]:0}}" style="width:80%">
                  </td>
                  <td>
                    @if(isset($elems["baseTasks"]))
                      <span style="color:black;font-size:10px;">Completado: {{$elems["quantityTask"]}} / {{$elems["baseTasks"]}}</span> <br>
                      <span style="color:black;font-size:10px;">Adicional:{{$elems["aditTask"]}}</span> <br>
                      <a href="javascript:" title="Productividad"
                      onclick = "showProductiv({{$elems["tasks"]}},{{$elems["id"]}},'{{$elems["clientName"]}}','{{$elems["dayWeek"]}}');"
                      > <i data-feather="eye"></i></a>
                    @endif
                  </td>
                  <td>
                    {{-- <a href="javascript:"title="Referencia"> <i data-feather="book-open"></i></a> --}}
                    @if($elems["estado"]!='no trabaja')
                      <button type="submit" class="btn btn-icon button"> <i data-feather="save"></i> </button>
                    @endif
                  </td>
                </tr>
                </form>
              @endforeach
            @endforeach
          @endforeach
            </tbody>

            <tfoot>
              <tr>
                <th>Total</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
		</div>
	</div>
 </div>
</div>



<script type="text/javascript">
  function calcViatics(des){
    let viatics = {{!empty($colabInfo->viaticos)?$colabInfo->viaticos:1}};
    $(des).parent().find("span").html('Q.'+viatics*des.value);
    // console.log(viatics*des.value);
  }
  function filter(){
    if($('#week').val()!=''){
      window.location= '{{route('admin.colabCompanyWork',[$idColab,$idCompany])}}'+'/'+$('#week').val()+'/'+$('#year').val();
    }else{
      alert('Por favor eliga una semana');
    }
  }
  function showProductiv(produ,idTas,nmCl,day){
    console.log(produ);
    let listof ='';
    for(let ll in produ){
      listof+='<tr><td>'+produ[ll].name+'</td><td> <input type="number" value="'+produ[ll].descript+'" name="altu['+produ[ll].ids+'_'+produ[ll].origin+']">\
      </td></tr>';
    }
    swal.fire({title:'<small>Cliente:</small> '+nmCl+ '<small> Día: </small>'+day,
               html:'<div class="row"><div class="col-12"><form action="{{route('admin.workModify',[$idColab,$idCompany,$yr,$wk])}}" method="post">\
               <table class="table">\
                <tbody>'+listof+'<tr><td colspan="2">\
                <input type="submit" value="Guardar" class="btn btn-raised btn-raised-primary m-1"></td></tr>\
                </tbody>\
               </table>\
               <input type="hidden" value="{{ csrf_token() }}" name="_token"/>\
               <input type="hidden" value="'+idTas+'" name="idFollower"/>\
               </form></div></div>',
              showCloseButton: false,
              showCancelButton: false,
              showConfirmButton: false
          });
  }
  function showDesc(messa){
    swal.fire({title:'Descripción de permiso',
               html:'<div class="row"><div class="col-12">'+messa+'</div></div>',
              showCloseButton: true,
              showCancelButton: false,
              showConfirmButton: false
          });
  }
  function changColor(estos){
    $(estos).css("color","black");
  }
</script>
<style media="screen">
input[type=text],input[type=time],input[type=number]{
  width: 100%;
}
.button{
  display: inline;
  margin: 0;
  padding: 0;
  height: 26px;
  margin-top: -14px;
  border: 0;
  background: none;
  color: #093982;
}
.escrit .form-group{
  float: left;
  width: auto;
}
.escrit .form-group.numbr{
  float: left;
  width: auto;
}
.rubrica td{
    vertical-align: top;
}
.rubrica tr{
    border-bottom: 3px solid #d2d2d2;
}
.numbr{
      width: 72px;
}
.red{
  border:1px solid red;
}
td{
  white-space:nowrap;
}
</style>
@endsection

@section('page-js')



@endsection

@section('bottom-js')

{{-- <script src="{{asset('assets/js/quill.script.js')}}"></script> --}}
<script>
$(document).ready(function () {
    var quill = new Quill('#full-editor', {
        modules: {
            syntax: !0,
            toolbar: [
                [{
                    font: []
                }, {
                    size: []
                }],
                ["bold", "italic", "underline", "strike"],
                [{
                    color: []
                }, {
                    background: []
                }],
                [{
                    script: "super"
                }, {
                    script: "sub"
                }],
                [{
                    header: "1"
                }, {
                    header: "2"
                }, "blockquote", "code-block"],
                [{
                    list: "ordered"
                }, {
                    list: "bullet"
                }, {
                    indent: "-1"
                }, {
                    indent: "+1"
                }],
                ["direction", {
                    align: []
                }],
                ["link", "image", "video", "formula"],
                ["clean"]
            ]
        },
        theme: 'snow'
    });

    quill.on('text-change', function(delta, oldDelta, source) {
      if (source == 'api') {
        console.log("An API call triggered this change.");
      } else if (source == 'user') {
        let val = $('#full-editor').children()[0].innerHTML
        $('#full-editor-data').val(val)
      }
    });

});



</script>


@endsection
