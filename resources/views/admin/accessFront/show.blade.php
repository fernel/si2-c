@extends('admin.layouts.master')
@section('main-content')
<script type="text/javascript">
  var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
          1:{type:"txt",tl:"Nombre",nm:"name",elv:"0",nxt:6},
          2:{type:"txt",tl:"Url",nm:"route",elv:"1",nxt:6},
          // 3:{type:"chkbxstyl",tl:"Publico",nm:"publc",vl:[["1",'<span id="stat"></span>']],
          //    elv:"2",add:'onclick="checar(this,0);" omit="T"',nxt:6},
          3:{type:"txt",tl:"Icono",id:"imag",nm:"icon",elv:"2", add:'onclick="flotingblank(this);" readonly',nxt:6},
          4:{type:"txt",tl:"Grupo",nm:"group",elv:"3"}};

  var conform={action:"{{route('admin.accesosFront.store')}}"}
    valdis={clase:"red",text:1,checkbox:1,radio:1};

</script>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="javascript:"class="" onclick="newfloatv2(kad);">
                        <i class="ion-ios7-plus-outline"></i>&nbsp;&nbsp;Nuevo acceso
                    </a>
                </h3>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table i class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Grupo</th>
                                <th>Título</th>
                                <th>Ícono</th>
                                <th>Archivo</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php
                                $criti = [];
                            @endphp
                            @foreach ($accesos as $a)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td class="hidden-xs">{{ $a->group }}</td>
                                    <td>{{ $a->name }}</td>
                                    <td><i class="text-20 {{ $a->icon }}"></i></td>
                                    <td class="hidden-xs">{{ $a->route }}</td>

                                    <td class="td-actions  text-left">
                                        <a href="javascript:" class=""
                                            onclick="modifyfloat('{{$a->id}}',kad,criteria);">
                                            <i class="text-20"data-feather="edit-3"></i>
                                        </a>
                                        <a href="javascript:"
                                            onclick="deleteD('{{$a->id}}','{{ csrf_token() }}');">
                                            <i class="text-12" data-feather="trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @php
                                $criti[$a->id] = [$a->name,
                                    $a->route,
                                    $a->icon,
                                    $a->group,
                                ];
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var criteria = <?=json_encode($criti)?>;

    function fillimg(sto)
    {
        $("#imag").val(sto);
        flotantecloseblank();
    }
</script>

@component('components.messagesForm')
@endcomponent

@endsection
