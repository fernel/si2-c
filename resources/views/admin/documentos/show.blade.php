@extends('admin.layouts.master')

@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Documentos</h1>
    <ul>
        <li><a href="{{url('/documentos')}}">Inicio</a></li>
        <li>Crear documento</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<form method="POST" action="/documentos{{isset($data['id']) ? '/'.$data['id'] : ''}}" enctype="multipart/form-data">

    @csrf
    @if($edit)
        @method('PUT')
    @endif

    <div class="row">
        <div class="col-md-12">

            <h4></h4>
            <div class="card mb-1 col-lg-12">
                <div class="card-body">
                    <div class="row">

                        <div class="col-lg-6 col-md-12 form-group mb-4">
                            <label for="name">Nombre</label>
                            <input class="form-control" id="name" name="name" type="text" placeholder="Nombre del documento" value="{{isset($data['name']) ? $data['name'] : ''}}"/>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-lg-12 col-md-12 form-group mb-4">
                            <label for="description">Descripción</label>
                            <input class="form-control" id="description" name="description" type="text" placeholder="Descripción del documento" value="{{isset($data['description']) ? $data['description'] : ''}}"/>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-12 col-md-12 form-group mb-2">
                            <label for="full-editor">Cuerpo del documento</label>
                            <p>Indicaciones: la plataforma puede sustituír los datos del colaborador en las partes del documento en donde se encuentren las siguientes claves.</p>
                            <ul>
                                <li><b>$nombres$</b> = Ambos nombres</li>
                                <li><b>$apellidos$</b> = Ambos apellidos</li>
                                <li><b>$dpi$</b> = DPI completo</li>
                            </ul>
                            <div class="mx-auto col-md-12 mb-5" style="height: 900px;">
                                <div id="full-editor" onchange="changeMe(this);">
                                    {!! isset($data['body']) ? $data['body'] : '' !!}
                                </div>
                                <input id="full-editor-data" name="body" type="hidden">
                            </div>
                        </div>

                    </div>
                </div>
            </div>




            <div class="col-md-12">
                <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                    {{ $edit ? 'Guardar cambios' : 'Guardar'}}
                </button>
            </div>

        </div>

    </div>

</form>

@endsection

@section('bottom-js')
<script>
$(document).ready(function () {
    $('#full-editor-data').val(`{!! isset($data['body']) ? $data['body'] : '' !!}`)
    var quill = new Quill('#full-editor', {
        modules: {
            syntax: !0,
            toolbar: [
                [{
                    font: []
                }, {
                    size: []
                }],
                ["bold", "italic", "underline", "strike"],
                [{
                    color: []
                }, {
                    background: []
                }],
                [{
                    script: "super"
                }, {
                    script: "sub"
                }],
                [{
                    header: "1"
                }, {
                    header: "2"
                }, "blockquote", "code-block"],
                [{
                    list: "ordered"
                }, {
                    list: "bullet"
                }, {
                    indent: "-1"
                }, {
                    indent: "+1"
                }],
                ["direction", {
                    align: []
                }],
                ["link", "image", "video", "formula"],
                ["clean"]
            ]
        },
        theme: 'snow'
    });

    quill.on('text-change', function(delta, oldDelta, source) {
      if (source == 'api') {
        console.log("An API call triggered this change.");
      } else if (source == 'user') {
        let val = $('#full-editor').children()[0].innerHTML
        $('#full-editor-data').val(val)
      }
    });
});

</script>
@endsection
