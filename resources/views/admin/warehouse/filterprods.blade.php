@extends('admin.layouts.master')
@section('main-content')
   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
					<h3 class="card-title">
            {{-- <a href="{{route("admin.products.create")}}">
              <i class="ion-ios7-plus-outline "></i>
              &nbsp;&nbsp;Nuevo producto
            </a> --}}
          </h3>
					<div class="card-options">
						<a href="#" class="card-options-collapse" data-toggle="card-collapse">
              <i class="fe fe-chevron-up"></i>
            </a>
						<a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen">
              <i class="fe fe-maximize"></i>
            </a>
						<a href="#" class="card-options-remove" data-toggle="card-remove">
              <i class="fe fe-x"></i>
            </a>
					</div>
				</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover" cellspacing="0"
            width="100%" id="bodeg">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Bodega</th>
                  <th>Producto</th>
                  <th>Fecha de ingreso</th>
                  <th>Ingresado</th>
                  <th>Egresado</th>
                  <th>Disponible</th>
                  <th>Ingresado por</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
    <script type="text/javascript">
      let tableCl;
        function setTable(){
          tableCl= $('#bodeg').DataTable( {
            columns: [
              { data: 'num' },
              { data: 'warehouse' },
              { data: 'title' },
              { data: 'date' },
              { data: 'amount' },
              { data: 'egress' },
              { data: 'total' },
              { data: 'whom' },
              {
                data: null,
                className: "td-actions center",
                defaultContent:
                '<a href="javascript:" onclick="addBook(this)"> <i class="text-20"data-feather="edit-3"></i></a>\
                    <a href="javascript:" onclick="deleteM(this)"><i class="text-12" data-feather="trash"></i></a>'
              }
          ],
            data: @json($data),
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Buscar",
            }
          } );
        }
    </script>

@endsection
