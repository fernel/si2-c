@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
        1:{type:"txt",tl:"Código",nm:"codigo",elv:"0"},
        2:{type:"txt",tl:"Nombre",nm:"nombre",elv:"1"},
        3:{type:"txtarea",tl:"Descripción",nm:"descripcion",elv:"2"},
        4:{type:"slct",tl:"Categoría",nm:"categoria_id",elv:"3",nxt:6,vl:@json($categorias), add:'onclick="selectCategoria(this)"'},
        5:{type:"slct",tl:"Subcategoría",id:"sub_categoria_id",nm:"sub_categoria_id",elv:"4",nxt:6,vl:[[null, '- - -']]},
        6:{type:"slct",tl:"Medida final",nm:"medida_id",elv:"5",vl:@json($unidades_arr)},
        7:{type:"nbr",tl:"Precio a cliente por unidad (Quetzales)",nm:"precio_cliente_unidad",elv:"6",nxt:6,vl:'0'},
        8:{type:"nbr",tl:"Porcentaje de depreciación (%)",nm:"porcentaje_depreciacion",elv:"7",nxt:6,vl:'0'},
        9:{type:"txt",tl:"Estado",nm:"estado",elv:"8"},
    },
    valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Equipos</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    <a href="javascript:" onclick="newfloatv2(kad)">
                        <i class="ion-ios7-plus-outline "></i>
                        &nbsp;&nbsp;Nuevo equipo
                    </a>
                </h3>

			</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table i class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Categoría</th>
                                <th>Subcategoría</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $criti=array(); @endphp
                            @foreach ($data as $dataRes)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td > {{$dataRes->codigo}}</td>
                                    <td > {{$dataRes->nombre}}</td>
                                    <td > {{$dataRes->categoria_name }}</td>
                                    <td > {{isset($dataRes->sub_categoria_element->subCategory) ? $dataRes->sub_categoria_element->subCategory : ''}}</td>
                                    <td > {{$dataRes->estado}}</td>

                                    <td class="td-actions  text-left">
                                      <a href="javascript:" onclick="modifyfloat('{{$dataRes->id}}',kad,criteria)">
                                        <i class="text-20"data-feather="edit-3"></i>
                                      </a>
                                      <a href="javascript:"
                                      onclick="deleteD('{{$dataRes->id}}','{{ csrf_token() }}');">
                                      <i class="text-12" data-feather="trash"></i></a>
                                    </td>
                                    {{-- <td > <a href="{{route("admin.products.edit",$dataRes->id)}}"class="btn btn-sm btn-primary">
                                      <i class="fa fa-eye"></i></a> </td> --}}
                                </tr>
                                @php $criti[$dataRes->id] = [
                                        $dataRes->codigo,
                                        $dataRes->nombre,
                                        $dataRes->descripcion,
                                        $dataRes->categoria,
                                        $dataRes->subcategoria,
                                        $dataRes->medida_id,
                                        $dataRes->precio_cliente_unidad,
                                        $dataRes->porcentaje_depreciacion,
                                        $dataRes->estado,
                                    ];
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var criteria = @json($criti),
        subcategorias = @json($subcategorias);

    function selectCategoria(elm){
        console.log(subcategorias)
        let cad = '',
            val = elm.value
        console.log(subcategorias[val])
        for(let n in subcategorias[val]){
            let obj = subcategorias[val][n]
            cad += `<option value="${obj.id}">${obj.subCategory}</option>`
        }
        $('#sub_categoria').html(cad)
    }
</script>

@endsection
