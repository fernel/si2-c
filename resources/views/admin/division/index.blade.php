@extends('admin.layouts.master')
@section('main-content')

<style>

    .row1{
		display:none;
		background-color: #9BA4B4!important;
	}
	.row2{
		display:none;
		background-color: #495877!important;
	}
	.row3{
		display:none;
		background-color: #405065!important;
	}

	.table-subtitle-custom th{
        padding: 0.25rem;
        background-color: #999;
		font-weight: 700;
		color: white;
	}

    .subtitle1 th{
        background-color: #8B94A4!important;
	}

    .subtitle2 th{
        background-color: #394867!important;
	}

    .subtitle3 th{
        background-color: #14274E!important;
	}



</style>

<script type="text/javascript">
    var kad={
                0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1:{type:"txt",tl:"Nombre ",nm:"nombre",elv:"0"},
                2:{type:"txt",tl:"Descripción",nm:"descripcion",elv:"1"},
            },
        valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Departamentos</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    <a href="javascript:"class="" onclick="newfloatv2(kad);">
                    <i class="ion-ios7-plus-outline "></i>
                    &nbsp;&nbsp;Nueva división
                    </a>
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="areas_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>División</th>
                                <th colspan=2>Descripción</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $datos_departamento = array(); @endphp
                            @php $datos_seccion = array(); @endphp
                            @php $datos_oficina = array(); @endphp
                            @php $datos_division = array(); @endphp

                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td onclick="mostrar_columna('l1<?=$value->id?>');">{{ $value->nombre }}</td>
                                    <td onclick="mostrar_columna('l1<?=$value->id?>');" colspan=2>{{ $value->descripcion }}</td>
                                    <td>
                                        <a href="javascript:" onclick="newfloatv2(kad, null, 'departamento/{{$value->id}}');" title="Agregar departamento / delegación">
                                            <i class="text-20"data-feather="plus"></i>
                                        </a>

                                        <a href="javascript:" onclick="modifyfloat('{{$value->id}}', kad, datos_division);" title="Editar división">
                                            <i class="text-20"data-feather="edit-3"></i>
                                        </a>

                                        <a href="javascript:" onclick="deleteD('{{$value->id}}', '{{ csrf_token() }}');" title="Eliminar división">
                                            <i class="text-12" data-feather="trash"></i>
                                        </a>
                                    </td>
                                </tr>

                                <tr class="row1 l1<?=$value->id?> table-subtitle-custom subtitle1">
                                    <th>#</th>
                                    <th>Departamento</th>
                                    <th colspan=2>Descripción</th>
                                    <th>Opciones</th>
                                </tr>
                                @foreach ($value["departamento"] as $value2)
                                    <tr class="row1 l1<?=$value->id?>">
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td onclick="mostrar_columna('l2<?=$value2->id?>');">{{ $value2->nombre }}</td>
                                        <td onclick="mostrar_columna('l2<?=$value2->id?>');" colspan=2>{{ $value2->descripcion }}</td>
                                        <td>
                                            <a href="javascript:" onclick="newfloatv2(kad, null, 'seccion/{{$value2->id}}');" title="Agregar sección">
                                                <i class="text-20"data-feather="plus"></i>
                                            </a>

                                            <a href="javascript:" onclick="modifyfloat('{{$value2->id}}', kad, datos_departamento, 'literalUrl', 'departamento/{{$value2->id}}');" title="Editar departamento">
                                                <i class="text-20"data-feather="edit-3"></i>
                                            </a>

                                            <a href="javascript:" onclick="deleteD('{{$value2->id}}', '{{ csrf_token() }}', 'departamento');" title="Eliminar departamento">
                                                <i class="text-12" data-feather="trash"></i>
                                            </a>
                                        </td>
                                    </tr>

                                    <tr class="row2 l2<?=$value2->id?> table-subtitle-custom subtitle2">
                                        <th>#</th>
                                        <th>Sección</th>
                                        <th colspan=2>Descripción</th>
                                        <th>Opciones</th>
                                    </tr>

                                    {{-- ////////////////////////////////oficina///////////////////////// --}}
                                    @foreach ($value2["seccion"] as $value3)
                                        <tr class="row2 l2<?=$value2->id?>">
                                            <td>{{ $loop->index + 1 }}</td>
                                            <td onclick="mostrar_columna('l3<?=$value3->id?>');">{{ $value3->nombre }}</td>
                                            <td onclick="mostrar_columna('l3<?=$value3->id?>');" colspan=2>{{ $value3->descripcion }}</td>
                                            <td>
                                                <a href="javascript:" onclick="newfloatv2(kad, null, 'oficina/{{$value2->id}}');" title="Agregar oficina">
                                                    <i class="text-20"data-feather="plus"></i>
                                                </a>

                                                <a href="javascript:" onclick="modifyfloat('{{$value3->id}}', kad, datos_seccion, 'literalUrl', 'seccion/{{$value3->id}}');" title="Editar oficina">
                                                    <i class="text-20"data-feather="edit-3"></i>
                                                </a>

                                                <a href="javascript:" onclick="deleteD('{{$value3->id}}', '{{ csrf_token() }}', 'seccion');" title="Eliminar seccion">
                                                    <i class="text-12" data-feather="trash"></i>
                                                </a>
                                            </td>
                                        </tr>

                                        <tr class="row3 l3<?=$value3->id?> table-subtitle-custom subtitle3">
                                            <th>#</th>
                                            <th>Oficina</th>
                                            <th colspan=2>Descripción</th>
                                            <th>Opciones</th>
                                        </tr>

                                        @foreach ($value3["oficina"] as $value4)
                                            <tr class="row3 l3<?=$value3->id?>">
                                                <td>{{ $loop->index + 1 }}</td>
                                                <td onclick="">{{ $value4->nombre }}</td>
                                                <td onclick="" colspan=2>{{ $value4->descripcion }}</td>
                                                <td>
                                                    <a href="javascript:" onclick="modifyfloat('{{$value4->id}}', kad, datos_oficina, 'literalUrl', 'oficina/{{$value4->id}}');" title="Editar oficina">
                                                        <i class="text-20"data-feather="edit-3"></i>
                                                    </a>

                                                    <a href="javascript:" onclick="deleteD('{{$value4->id}}', '{{ csrf_token() }}', 'oficina');" title="Eliminar oficina">
                                                        <i class="text-12" data-feather="trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @php $datos_oficina[$value4->id] = [$value4->nombre, $value4->descripcion]; @endphp
                                        @endforeach

                                        @php $datos_seccion[$value3->id] = [$value3->nombre, $value3->descripcion]; @endphp
                                    @endforeach
                                    {{-- ////////////////////////////////delegacion///////////////////////// --}}
                                    {{-- @foreach ($value3["oficina"] as $value4)
                                        <tr class="row2 l2<?=$value2->id?>">
                                            <td>{{ $loop->index + 1 }}</td>
                                            <td onclick="mostrar_columna('l2<?=$value3->id?>');">{{ $value3->nombre }}</td>
                                            <td>Delegación</td>
                                            <td onclick="mostrar_columna('l2<?=$value3->id?>');">{{ $value3->descripcion }}</td>
                                            <td>
                                                <a href="javascript:" onclick="modifyfloat('{{$value3->id}}', kad, datos_delegacion, 'literalUrl', 'delegacion/{{$value3->id}}');" title="Editar delegacion">
                                                    <i class="text-20"data-feather="edit-3"></i>
                                                </a>

                                                <a href="javascript:" onclick="deleteD('{{$value3->id}}', '{{ csrf_token() }}', 'delegacion');" title="Eliminar delegacion">
                                                    <i class="text-12" data-feather="trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @php $datos_delegacion[$value3->id] = [$value3->nombre, $value3->descripcion]; @endphp
                                    @endforeach --}}

                                    @php $datos_departamento[$value2->id] = [$value2->nombre, $value2->descripcion]; @endphp
                                @endforeach

                                @php $datos_division[$value->id] = [$value->nombre, $value->descripcion]; @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  let datos_departamento = @json($datos_departamento),
      datos_seccion = @json($datos_seccion),
      datos_oficina = @json($datos_oficina),
      datos_division = @json($datos_division);

  function mostrar_columna(elm){
      let stat = $('.' + elm).css('display'),
          cls = $('.' + elm).attr('class');
      if (stat == 'none'){
          $('.' + elm).show()
      }else{

          $('.' + elm).hide()

          if(cls.match('row1')){
              $('.row2').hide()
              $('.row3').hide()
          }else if(cls.match('row2')){
              $('.row3').hide()
          }
      }
  }



</script>

@endsection

@section('bottom-js')
    {{-- <script>
        $('#areas_table').DataTable();
    </script> --}}
@endsection
