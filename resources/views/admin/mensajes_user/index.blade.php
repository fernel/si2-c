@inject('helper', 'App\Http\Helpers\helpers')
@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Mensajes User</h1>
    <!--
                        <ul>
                            <li>Buzón de Entrada</li>
                            <li><a href="{{url('/mensajes_enviados')}}">Enviados</a></li>
                        </ul>
    -->
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
			     <h3 class="card-title">
              <a href="{{ url('/mensajes_user/create') }}" class="" >
                <i class="ion-ios7-plus-outline "></i>
                &nbsp;&nbsp;
                Nuevo Mensaje
              </a>
           </h3>
           <div class="card-options">
             <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
             <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
             <a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
           </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table i class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  
                  <th>De</th>
                  <th>Para</th>
                  <th>Asunto</th>
                  <th>Fecha</th>
                </tr>
              </thead>
              <tbody>

                @forelse($mensajes as $mensaje)
                  <tr>
                    

                    @if (($mensaje['tipo']==0 AND $mensaje['status']==0) OR ($mensaje['tipo']==1 AND $mensaje['status']==2))
                        <td ><strong><a href="{{ url('mensajes_user') }}/{{ $mensaje['id'] }}">{{ $mensaje['de'] }}</a></strong></td>
                        <td ><strong><a href="{{ url('mensajes_user') }}/{{ $mensaje['id'] }}">{{ $mensaje['para'] }}</a></strong></td>
                        <td ><strong><a href="{{ url('mensajes_user') }}/{{ $mensaje['id'] }}">{{ $mensaje['titulo'] }}</a></strong></td>
                        <td><strong><a href="{{ url('mensajes_user') }}/{{ $mensaje['id'] }}">{{ $mensaje['fecha'] }}</a></strong></td>
                    @else
                        <td ><a href="{{ url('mensajes_user') }}/{{ $mensaje['id'] }}">{{ $mensaje['de'] }}</a></td>
                        <td ><a href="{{ url('mensajes_user') }}/{{ $mensaje['id'] }}">{{ $mensaje['para'] }}</a></td>
                        <td ><a href="{{ url('mensajes_user') }}/{{ $mensaje['id'] }}">{{ $mensaje['titulo'] }}</a></td>
                        <td><a href="{{ url('mensajes_user') }}/{{ $mensaje['id'] }}">{{ $mensaje['fecha'] }}</a></td>
                    @endif


                  </tr>
                @empty
                  <div class="alert alert-success">
                    No hay mensajes en el buzón.
                  </div>

                @endforelse
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    @component('components.messagesForm')
    @endcomponent
@endsection
