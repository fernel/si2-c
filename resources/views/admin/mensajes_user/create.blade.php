@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Mensajes User</h1>

                        <ul>
                            <li><a href="{{url('/mensajes_user')}}">Buzón de Mensajes</a></li>
                        </ul>    
    <!--
                        <ul>
                            <li><a href="{{url('/mensajes')}}">Buzón de Entrada</a></li>
                            <li><a href="{{url('/mensajes_enviados')}}">Enviados</a></li>
                        </ul>
    -->
</div>
<div class="separator-breadcrumb border-top"></div>

<form method="POST" action="{{ url('mensajes_user') }}" >

    @csrf

    <div class="row">
        <div class="col-md-12">


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Nuevo Mensaje</h4>
                </div>
                <div class="card-body">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                             <label for="genero">Para:</label>
                             <select class="form-control" id="para_admin" name="para_admin">
                                 <option selected disabled value=""> ------ </option>
                                 @foreach($admins as $admin)
                                     <option value="{{ $admin->id }}">{{ $admin->name }}</option>
                                 @endforeach
                             </select>
                        </div>
                    </div>         

                             <input type="hidden" id="de_user" name="de_user" value="{{ $user->id }}">
                             <input type="hidden" id="tipo" name="tipo" value="1">

                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="asunto">Asunto</label>
                            <input class="form-control" id="titulo" name="titulo" type="text" placeholder="Asunto" />
                        </div>

                    </div>



                    <div class="row">
                        <div class="col-md-12 form-group mb-3">
                             <label for="observaciones">Mensaje:</label>
                             <textarea class="form-control" style="height: 150px;" id="mensaje" name="mensaje" placeholder="Mensaje..." >{{ isset($data['mensaje']) ? $data['mensaje'] : '' }}</textarea>
                        </div>
                    </div>     
                    
                </div>
            </div>

            <div class="col-md-12">
                <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                    Enviar Mensaje
                </button>
            </div>

        </div>

    </div>

</form>


@endsection
