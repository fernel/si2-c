<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  @include('front.layouts.common.scripts')
</head>
<body class="text-left">

<div class="app-front-wrap layout-sidebar-large">
    @include('front.layouts.common.header')
    @include('front.layouts.common.sidebar')
    <div class="main-content-wrap sidenav-open d-flex flex-column">
      <div class="main-content">
         @section('main-content')
         @show
       </div>
       <div class="flex-grow-1"></div>
       @include('front.layouts.common.footer')
    </div>
</div>
@include('front.layouts.common.scriptsfooter')

<!-- <div class='loadscreen' id="preloader">
<div class="loader spinner-bubble spinner-bubble-primary"></div>
</div>
<div id="loading">
<img src="{{asset('assets/images/other/loader.svg')}}" class="loader-img" alt="Loader">
</div> -->

@section('bottom-js')
@show



</body>

</html>
