@extends('front.layouts.master')
@section('main-content')

<script type="text/javascript">
    var kad={
                0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1:{type:"txt",tl:"Número de expediente/requerimiento MP",nm:"numero",elv:"0"},
                2:{type:"datetimepicker",tl:"Fecha del requerimiento MP",nm:"fecha_requerimiento",elv:"1"},
                3:{type:"txt",tl:"Fiscalia que emite",nm:"fiscalia_emite",elv:"2"},
                4:{type:"txt",tl:"Procedencia",nm:"procedencia",elv:"3"},
                5:{type:"txt",tl:"Destinatario",nm:"destinatario",elv:"4"},
                6:{type:"datetimepicker",tl:"Fecha de recepción",nm:"fecha_recepcion",elv:"5"},
                7:{type:"time",tl:"Hora de recepción",nm:"hora_recepcion",elv:"6"},
                8:{type:"txt",tl:"Plazo",nm:"plazo",elv:"7"},

            },
        valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Requerimientos MP</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    <?php if (Helper::has_permission($menubar["permissions"])) { ?>
                        <a href="javascript:"class="" onclick="newfloatv2(kad);">
                        <i class="ion-ios7-plus-outline "></i>
                            &nbsp;&nbsp;Nuevo requerimiento
                        </a>
                    <?php } ?>
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="bancos_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Numero</th>
                                <th>Fecha</th>
                                <th>Fiscalía</th>
                                <th>Procedencia</th>
                                <th>Destinatario</th>
                                <th>Fecha de recepción</th>
                                <th>Hora de recepcón</th>
                                <th>Plazo</th>
                                <th>Creada</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $criti = array(); @endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $value->numero }}</td>
                                    <td>{{ $value->fecha_requerimiento }}</td>
                                    <td>{{ $value->fiscalia_emite }}</td>
                                    <td>{{ $value->procedencia }}</td>
                                    <td>{{ $value->destinatario }}</td>
                                    <td>{{ $value->fecha_recepcion }}</td>
                                    <td>{{ $value->hora_recepcion }}</td>
                                    <td>{{ $value->plazo }}</td>
                                    <td>{{ $value->created_at }}</td>
                                    <td>
                                        <?php if (Helper::has_permission($menubar["permissions"])) { ?>
                                            <a href="javascript:" onclick="modifyfloat('{{$value->id}}', kad, criteria);">
                                                <i class="text-20"data-feather="edit-3"></i>
                                            </a>
                                        <?php } ?>

                                        <?php if (Helper::has_permission($menubar["permissions"], "Eliminar")) { ?>
                                            <a href="javascript:" onclick="deleteD('{{$value->id}}', '{{ csrf_token() }}');">
                                                <i class="text-12" data-feather="trash"></i>
                                            </a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                @php $criti[$value->id] = [
                                                            $value->numero,
                                                            $value->fecha_requerimiento,
                                                            $value->fiscalia_emite,
                                                            $value->procedencia,
                                                            $value->destinatario,
                                                            $value->fecha_recepcion,
                                                            $value->hora_recepcion,
                                                            $value->plazo,
                                                        ];
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  var criteria = @json($criti);
</script>

@endsection

@section('bottom-js')
    <script>
        $('#bancos_table').DataTable();
    </script>
@endsection
