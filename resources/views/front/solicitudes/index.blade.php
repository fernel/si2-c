@extends('front.layouts.master')
@section('main-content')

    <script type="text/javascript">
        var kad={
                    0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                    1:{type:"slct",tl:"Tipo de asignación",nm:"tipo_asignacion",id:"tipo_asignacion",vl:[[null, '- - -'],[0,'Persona'],[1,'Division'],[2,'Departamento'],[3,'Seccion'],[4,'Oficina']],elv:"0",add:'onchange="selectTipoAssign(this)"'},
                    2:{type:"slct",tl:"Asignar a",nm:"asignado_id",id:"asignado_id",vl:@json($usuarios),elv:"1"},
                },
            valdis={clase:"red",text:1};
    </script>

    <div class="breadcrumb">
        <h1 class="mr-2">Solicitudes</h1>
        <ul>
            <li>Inicio</li>
        </ul>
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">
                    <h3 class="card-title">
                        {{-- <a href="javascript:"class="" onclick="newfloatv2(kad);">
                        <i class="ion-ios7-plus-outline "></i>
                            &nbsp;&nbsp;Nueva solicitud
                        </a> --}}
                    </h3>
        		</div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="asignados_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Código</th>
                                    <th>Usuario que la creó</th>
                                    <th>Tipo de formulario</th>
                                    <th>Información en el formulario</th>
                                    <th>Estado</th>
                                    <th>Tipo de asignación</th>
                                    <th>Asignado a</th>
                                    <th>Fecha de creación</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $criti = array(); @endphp
                                @foreach ($data as $value)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $value->codigo }}</td>
                                        <td>{{ $value->userFront->name }}</td>
                                        <td>{{ $value->tipo_formulario }}</td>
                                        <td>{{ $value->info_formulario }}</td>
                                        <td><span class="badge w-badge badge-{{$value->estado == 'pendiente' ? 'warning' : 'success'}}">{{ $value->estado}}</span></td>
                                        <td>{{ $value->asig }}</td>
                                        <td>{{ $value->gente }}</td>
                                        <td>{{ $value->created_at }}</td>
                                        <td>
                                            <?php if (Helper::has_permission($menubar["permissions"], "Editar")) { ?>
                                                <a href="javascript:" onclick="modifyfloat('{{$value->id}}', kad, criteria);loadSelected('{{$value->id}}');">
                                                    <i class="text-20"data-feather="edit-3"></i>
                                                </a>
                                            <?php } ?>
                                            {{-- <a href="javascript:" onclick="deleteD('{{$value->id}}', '{{ csrf_token() }}');">
                                                <i class="text-12" data-feather="trash"></i>
                                            </a> --}}
                                        </td>
                                    </tr>
                                @php
                                    $criti[$value->id]=[
                                        $value->tipo_asignacion,
                                        $value->asignado_id
                                    ];
                                @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        criteria = @json($criti);
        usuarios = @json($usuarios);
        division = @json($division);
        departamentos = @json($departamentos);
        seccion = @json($seccion);
        oficina = @json($oficina);

        function selectTipoAssign(elm){
            let cad = '';
            let val = $(elm).val()

            if (val == 0){
                for(let i = 0; i < usuarios.length; i++){
                    cad += `<option value="${usuarios[i][0]}">${usuarios[i][1]}</option>`;
                }

            }else if (val == 1){
                for(let i = 0; i < division.length; i++){
                    cad += `<option value="${division[i][0]}">${division[i][1]}</option>`;
                }
            }else if (val == 2){
                for(let i = 0; i < departamentos.length; i++){
                    cad += `<option value="${departamentos[i][0]}">${departamentos[i][1]}</option>`;
                }
            }else if (val == 3){
                for(let i = 0; i < seccion.length; i++){
                    cad += `<option value="${seccion[i][0]}">${seccion[i][1]}</option>`;
                }
            }else if (val == 4){
                for(let i = 0; i < oficina.length; i++){
                    cad += `<option value="${oficina[i][0]}">${oficina[i][1]}</option>`;
                }
            }

            $('#asignado_id').html(cad)
        }

        function loadSelected(id){
            let elem = criteria[id];
            $('#tipo_asignacion').trigger('change')
            $('#asignado_id').val(elem[1])
        }

    </script>

    @endsection

    @section('bottom-js')
        <script>
            $('#asignados_table').DataTable();
        </script>
    @endsection
