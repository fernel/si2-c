<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\AuthenticationController;
use App\Http\Controllers\api\loggsController;
use App\Http\Controllers\api\permissionsController;
use App\Http\Controllers\api\listPetitionsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/create-account', [AuthenticationController::class, 'createAccount']);

Route::post('/signin', [AuthenticationController::class, 'signin']);
//using middleware
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/profile', function(Request $request) {
        return auth()->user();
    });
    Route::post('/sign-out', [AuthenticationController::class, 'logout']);
    Route::post('/loggs',[loggsController::class, 'store']);
    Route::get('/shedules',[loggsController::class, 'shedules']);
    Route::post('/storeWork',[loggsController::class, 'storeWorkday']);
    Route::get('/shedules',[loggsController::class, 'shedules']);
    Route::get('/getPetitions',[loggsController::class, 'getPetitions']);
    Route::post('/timepermitions',[permissionsController::class, 'setPetitionTime']);
    Route::post('/defaulttimeperm',[permissionsController::class, 'setPetitionTimeDefault']);
    Route::post('/economicpetition',[permissionsController::class, 'economicpetition']);
    Route::post('/simplepetitions',[permissionsController::class, 'simplepetitions']);
    Route::post('/complaintments',[permissionsController::class, 'complaintments']);
    Route::get('/getListPetitions',[listPetitionsController::class, 'listPetitions']);


});

// Route::any('/birthday',[cumplesapiController::class, 'birthday']);

Route::any('/birthday', 'api\cumplesapiController@birthday');