<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();
Route::prefix('/admin')->name('admin.')->namespace('Admin')->group(function(){
    Route::namespace('Auth')->group(function(){
        Route::get('/login', 'LoginController@showLoginView')->name('login');
        Route::post('/login', 'LoginController@Login')->name('login.submit');
        Route::get('/logout', 'LoginController@logout')->name('logout');
    });
    Route::get('/home', 'adminController@index')->name('adminhome');
    Route::get('/', 'adminController@index')->name('home');
    Route::resource('/permissions', 'permissionsController');
    Route::resource('/users', 'usersController');
    Route::resource('/roles', 'rolesController');

    Route::resource('/division', 'divisionController');

    Route::post('/departamento/{idparent}', 'divisionController@storeDepartamento');
    Route::put('/departamento/{idparent}', 'divisionController@updateDepartamento');
    Route::delete('/division/departamento/{idparent}', 'divisionController@destroyDepartamento');

    Route::post('/seccion/{idparent}', 'divisionController@storeSeccion');
    Route::put('/seccion/{idparent}', 'divisionController@updateSeccion');
    Route::delete('/division/seccion/{idparent}', 'divisionController@destroySeccion');

    Route::post('/oficina/{idparent}', 'divisionController@storeOficina');
    Route::put('/oficina/{idparent}', 'divisionController@updateOficina');
    Route::delete('/division/oficina/{idparent}', 'divisionController@destroyOficina');

    Route::post('/delegacion/{idparent}', 'divisionController@storeDelegacion');
    Route::put('/delegacion/{idparent}', 'divisionController@updateDelegacion');
    Route::delete('/division/delegacion/{idparent}', 'divisionController@destroyDelegacion');


    Route::resource('/usuariosFront', 'usersFrontController');

    Route::resource('/accesosFront', 'accessFrontController');
});


Route::namespace('AuthFront')->group(function(){
    Route::get('/login', 'LoginController@showLoginView')->name('login');
    Route::post('/login', 'LoginController@Login')->name('login.submit');
    Route::get('/logout', 'LoginController@logout')->name('logout');
});

Route::get('/', 'HomeController@index')->name('home')->middleware('authtimeout');
Route::get('/home', 'HomeController@index')->middleware('authtimeout');

Route::resource('/solicitudes', 'SolicitudesController')->middleware('authtimeout');

Route::resource('/requerimiento_MP', 'RequerimientoMPController')->middleware('authtimeout');

Route::resource('/requerimiento_', 'RequerimientosController')->middleware('authtimeout');




// View::composer('*', function($view){
//     $view->alertas = Controller::list_alertas();
// });
