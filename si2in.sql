-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: si2in
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_access`
--

DROP TABLE IF EXISTS `admin_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_access` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `naccess` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archaccess` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iconaccess` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publc` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `groupacc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_access`
--

LOCK TABLES `admin_access` WRITE;
/*!40000 ALTER TABLE `admin_access` DISABLE KEYS */;
INSERT INTO `admin_access` VALUES (1,'Roles de usuario','roles','i-Gears','1','Configuración','2020-07-30 09:31:00','2021-10-05 09:53:34'),(2,'Personal','users','i-Gears','1','Configuración','2020-07-30 09:31:28','2021-10-05 09:53:46'),(3,'Accesos','permissions','cogs','1','Configuración','2019-11-07 11:05:02','2021-10-02 08:04:01');
/*!40000 ALTER TABLE `admin_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_access_roles`
--

DROP TABLE IF EXISTS `admin_access_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_access_roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_role` bigint unsigned NOT NULL,
  `id_access` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_access_roles_id_role_foreign` (`id_role`),
  KEY `admin_access_roles_id_access_foreign` (`id_access`),
  CONSTRAINT `admin_access_roles_id_access_foreign` FOREIGN KEY (`id_access`) REFERENCES `admin_access` (`id`) ON DELETE CASCADE,
  CONSTRAINT `admin_access_roles_id_role_foreign` FOREIGN KEY (`id_role`) REFERENCES `admin_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_access_roles`
--

LOCK TABLES `admin_access_roles` WRITE;
/*!40000 ALTER TABLE `admin_access_roles` DISABLE KEYS */;
INSERT INTO `admin_access_roles` VALUES (5,1,2,NULL,NULL);
/*!40000 ALTER TABLE `admin_access_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_alertas`
--

DROP TABLE IF EXISTS `admin_alertas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_alertas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `elemento_id` bigint unsigned DEFAULT NULL,
  `referencia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mensaje` text COLLATE utf8mb4_unicode_ci,
  `nombre_modulo` text COLLATE utf8mb4_unicode_ci,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_alertas_referencia_unique` (`referencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_alertas`
--

LOCK TABLES `admin_alertas` WRITE;
/*!40000 ALTER TABLE `admin_alertas` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_alertas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_archivos`
--

DROP TABLE IF EXISTS `admin_archivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_archivos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_file_manager` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `url` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_archivos`
--

LOCK TABLES `admin_archivos` WRITE;
/*!40000 ALTER TABLE `admin_archivos` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_archivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_bancos`
--

DROP TABLE IF EXISTS `admin_bancos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_bancos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `razon` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numero` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_bancos`
--

LOCK TABLES `admin_bancos` WRITE;
/*!40000 ALTER TABLE `admin_bancos` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_bancos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_bodegas`
--

DROP TABLE IF EXISTS `admin_bodegas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_bodegas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_bodegas`
--

LOCK TABLES `admin_bodegas` WRITE;
/*!40000 ALTER TABLE `admin_bodegas` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_bodegas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_categories`
--

DROP TABLE IF EXISTS `admin_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_categories`
--

LOCK TABLES `admin_categories` WRITE;
/*!40000 ALTER TABLE `admin_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_changeLog`
--

DROP TABLE IF EXISTS `admin_changeLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_changeLog` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `element_id` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_changeLog`
--

LOCK TABLES `admin_changeLog` WRITE;
/*!40000 ALTER TABLE `admin_changeLog` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_changeLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_client_colab_assoc`
--

DROP TABLE IF EXISTS `admin_client_colab_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_client_colab_assoc` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `idClient` bigint unsigned NOT NULL,
  `idColab` bigint unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idPuesto` bigint unsigned DEFAULT NULL,
  `idBranch` bigint unsigned DEFAULT NULL,
  `idArea` bigint unsigned DEFAULT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inicio` date DEFAULT NULL,
  `fin` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_client_colab_assoc`
--

LOCK TABLES `admin_client_colab_assoc` WRITE;
/*!40000 ALTER TABLE `admin_client_colab_assoc` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_client_colab_assoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_colaboradores`
--

DROP TABLE IF EXISTS `admin_colaboradores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_colaboradores` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellidos` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` text COLLATE utf8mb4_unicode_ci,
  `fecha` date DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dpi` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nit` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iggs` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `irtra` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `intecap` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `licencia` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_salud` date DEFAULT NULL,
  `fecha_pulmones` date DEFAULT NULL,
  `genero` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nivel_academico` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nacionalidad` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado_civil` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `conyuge` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hijos` smallint unsigned NOT NULL DEFAULT '0',
  `profesion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel1` varchar(18) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel2` varchar(18) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci,
  `departamento` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `municipio` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacto1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel_con1` varchar(18) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentesco_con1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacto2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel_con2` varchar(18) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentesco_con2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_sangre` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contrato` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `puesto` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sueldo` double(6,2) DEFAULT NULL,
  `bono_extra` double(6,2) DEFAULT NULL,
  `antiguedad` double(6,2) DEFAULT NULL,
  `bono_ley` double(6,2) DEFAULT NULL,
  `hora_extra_diurna` double(6,2) DEFAULT NULL,
  `hora_extra_nocturna` double(6,2) DEFAULT NULL,
  `hora_extra_especial` double(6,2) DEFAULT NULL,
  `viaticos` double(6,2) DEFAULT NULL,
  `movil` double(6,2) DEFAULT NULL,
  `hora_normal` double(6,2) DEFAULT NULL,
  `hora_normal_nocturna` double(6,2) DEFAULT NULL,
  `tipo_pago` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banco` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pago_sueldo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otros_pagos` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `puntos1` double(6,2) DEFAULT NULL,
  `puntos2` double(6,2) DEFAULT NULL,
  `puntos3` double(6,2) DEFAULT NULL,
  `puntos4` double(6,2) DEFAULT NULL,
  `estatus` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `observaciones` text COLLATE utf8mb4_unicode_ci,
  `idUser` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `vence_licencia` date DEFAULT NULL,
  `cuenta1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cuenta2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cuenta3` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firma` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_file_manager` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idWRegister` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_colaboradores`
--

LOCK TABLES `admin_colaboradores` WRITE;
/*!40000 ALTER TABLE `admin_colaboradores` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_colaboradores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_colors`
--

DROP TABLE IF EXISTS `admin_colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_colors` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `namecolor` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_colors`
--

LOCK TABLES `admin_colors` WRITE;
/*!40000 ALTER TABLE `admin_colors` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_company`
--

DROP TABLE IF EXISTS `admin_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_company` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nit` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `legalrepresent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` text COLLATE utf8mb4_unicode_ci,
  `phone1` int NOT NULL,
  `phone2` int NOT NULL,
  `depto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `municip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addess` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idUser` bigint unsigned DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `igss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dpi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado_civil` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genero` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calculo_sobre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `nacionalidad` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vecino` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iggs_patronal` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `irtra` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iggs_laboral` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `intecap` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bono_extra` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `horas_extra` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productividad` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_file_manager` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idWRegister` int DEFAULT NULL,
  `bodega_id` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_company_iduser_foreign` (`idUser`),
  KEY `admin_company_bodega_id_foreign` (`bodega_id`),
  CONSTRAINT `admin_company_bodega_id_foreign` FOREIGN KEY (`bodega_id`) REFERENCES `admin_bodegas` (`id`),
  CONSTRAINT `admin_company_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `admin_usersadmin` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_company`
--

LOCK TABLES `admin_company` WRITE;
/*!40000 ALTER TABLE `admin_company` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_company_client_assoc`
--

DROP TABLE IF EXISTS `admin_company_client_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_company_client_assoc` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `idClient` bigint unsigned NOT NULL,
  `idCompany` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_company_client_assoc_idclient_foreign` (`idClient`),
  KEY `admin_company_client_assoc_idcompany_foreign` (`idCompany`),
  CONSTRAINT `admin_company_client_assoc_idclient_foreign` FOREIGN KEY (`idClient`) REFERENCES `admin_company_clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `admin_company_client_assoc_idcompany_foreign` FOREIGN KEY (`idCompany`) REFERENCES `admin_company` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_company_client_assoc`
--

LOCK TABLES `admin_company_client_assoc` WRITE;
/*!40000 ALTER TABLE `admin_company_client_assoc` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_company_client_assoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_company_clients`
--

DROP TABLE IF EXISTS `admin_company_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_company_clients` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `clientName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nit` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone1` int NOT NULL,
  `phone2` int NOT NULL,
  `depto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `municip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addess` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `credit` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nombre_factura` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observaciones` text COLLATE utf8mb4_unicode_ci,
  `vma` int DEFAULT NULL,
  `venta_sobregiro` tinyint(1) NOT NULL DEFAULT '0',
  `id_file_manager` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_area` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_company_clients`
--

LOCK TABLES `admin_company_clients` WRITE;
/*!40000 ALTER TABLE `admin_company_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_company_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_company_clients_branch_area`
--

DROP TABLE IF EXISTS `admin_company_clients_branch_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_company_clients_branch_area` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `idBranch` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_company_clients_branch_area`
--

LOCK TABLES `admin_company_clients_branch_area` WRITE;
/*!40000 ALTER TABLE `admin_company_clients_branch_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_company_clients_branch_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_company_clients_branchof`
--

DROP TABLE IF EXISTS `admin_company_clients_branchof`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_company_clients_branchof` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `namebranch` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idClient` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `bodega_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_company_clients_branchof_idclient_foreign` (`idClient`),
  KEY `admin_company_clients_branchof_bodega_id_foreign` (`bodega_id`),
  CONSTRAINT `admin_company_clients_branchof_bodega_id_foreign` FOREIGN KEY (`bodega_id`) REFERENCES `admin_bodegas` (`id`),
  CONSTRAINT `admin_company_clients_branchof_idclient_foreign` FOREIGN KEY (`idClient`) REFERENCES `admin_company_clients` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_company_clients_branchof`
--

LOCK TABLES `admin_company_clients_branchof` WRITE;
/*!40000 ALTER TABLE `admin_company_clients_branchof` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_company_clients_branchof` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_company_clients_jobs`
--

DROP TABLE IF EXISTS `admin_company_clients_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_company_clients_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `idJobCatalog` bigint unsigned NOT NULL,
  `idClientComp` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_company_clients_jobs_idjobcatalog_foreign` (`idJobCatalog`),
  KEY `admin_company_clients_jobs_idclientcomp_foreign` (`idClientComp`),
  CONSTRAINT `admin_company_clients_jobs_idclientcomp_foreign` FOREIGN KEY (`idClientComp`) REFERENCES `admin_company_client_assoc` (`id`) ON DELETE CASCADE,
  CONSTRAINT `admin_company_clients_jobs_idjobcatalog_foreign` FOREIGN KEY (`idJobCatalog`) REFERENCES `admin_puestos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_company_clients_jobs`
--

LOCK TABLES `admin_company_clients_jobs` WRITE;
/*!40000 ALTER TABLE `admin_company_clients_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_company_clients_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_company_clients_jobs_todo`
--

DROP TABLE IF EXISTS `admin_company_clients_jobs_todo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_company_clients_jobs_todo` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idJob` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_company_clients_jobs_todo_idjob_foreign` (`idJob`),
  CONSTRAINT `admin_company_clients_jobs_todo_idjob_foreign` FOREIGN KEY (`idJob`) REFERENCES `admin_company_clients_jobs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_company_clients_jobs_todo`
--

LOCK TABLES `admin_company_clients_jobs_todo` WRITE;
/*!40000 ALTER TABLE `admin_company_clients_jobs_todo` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_company_clients_jobs_todo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_company_colab_assoc`
--

DROP TABLE IF EXISTS `admin_company_colab_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_company_colab_assoc` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `idCompany` bigint unsigned NOT NULL,
  `idColab` bigint unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_company_colab_assoc`
--

LOCK TABLES `admin_company_colab_assoc` WRITE;
/*!40000 ALTER TABLE `admin_company_colab_assoc` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_company_colab_assoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_contratos`
--

DROP TABLE IF EXISTS `admin_contratos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_contratos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_contratos`
--

LOCK TABLES `admin_contratos` WRITE;
/*!40000 ALTER TABLE `admin_contratos` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_contratos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_conversiones`
--

DROP TABLE IF EXISTS `admin_conversiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_conversiones` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `unidad_inicial_id` bigint unsigned DEFAULT NULL,
  `unidad_final_id` bigint unsigned DEFAULT NULL,
  `operacion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` decimal(14,2) DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_conversiones_unidad_inicial_id_foreign` (`unidad_inicial_id`),
  KEY `admin_conversiones_unidad_final_id_foreign` (`unidad_final_id`),
  CONSTRAINT `admin_conversiones_unidad_final_id_foreign` FOREIGN KEY (`unidad_final_id`) REFERENCES `admin_unidades` (`id`),
  CONSTRAINT `admin_conversiones_unidad_inicial_id_foreign` FOREIGN KEY (`unidad_inicial_id`) REFERENCES `admin_unidades` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_conversiones`
--

LOCK TABLES `admin_conversiones` WRITE;
/*!40000 ALTER TABLE `admin_conversiones` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_conversiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_cumples`
--

DROP TABLE IF EXISTS `admin_cumples`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_cumples` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `mensaje` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_cumples`
--

LOCK TABLES `admin_cumples` WRITE;
/*!40000 ALTER TABLE `admin_cumples` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_cumples` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_documentos`
--

DROP TABLE IF EXISTS `admin_documentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_documentos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_documentos`
--

LOCK TABLES `admin_documentos` WRITE;
/*!40000 ALTER TABLE `admin_documentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_documentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_equipo_asignado`
--

DROP TABLE IF EXISTS `admin_equipo_asignado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_equipo_asignado` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `equipo_id` bigint unsigned DEFAULT NULL,
  `asignado_a_bodega_id` bigint unsigned DEFAULT NULL,
  `asignado_a_cliente_id` bigint unsigned DEFAULT NULL,
  `asignado_a_colaborador_id` bigint unsigned DEFAULT NULL,
  `origen_bodega_id` bigint unsigned DEFAULT NULL,
  `tipo_movimiento` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_movimiento` date DEFAULT NULL,
  `comentario` text COLLATE utf8mb4_unicode_ci,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_equipo_asignado_equipo_id_foreign` (`equipo_id`),
  KEY `admin_equipo_asignado_asignado_a_bodega_id_foreign` (`asignado_a_bodega_id`),
  KEY `admin_equipo_asignado_asignado_a_cliente_id_foreign` (`asignado_a_cliente_id`),
  KEY `admin_equipo_asignado_asignado_a_colaborador_id_foreign` (`asignado_a_colaborador_id`),
  KEY `admin_equipo_asignado_origen_bodega_id_foreign` (`origen_bodega_id`),
  CONSTRAINT `admin_equipo_asignado_asignado_a_bodega_id_foreign` FOREIGN KEY (`asignado_a_bodega_id`) REFERENCES `admin_bodegas` (`id`),
  CONSTRAINT `admin_equipo_asignado_asignado_a_cliente_id_foreign` FOREIGN KEY (`asignado_a_cliente_id`) REFERENCES `admin_company_clients` (`id`),
  CONSTRAINT `admin_equipo_asignado_asignado_a_colaborador_id_foreign` FOREIGN KEY (`asignado_a_colaborador_id`) REFERENCES `admin_colaboradores` (`id`),
  CONSTRAINT `admin_equipo_asignado_equipo_id_foreign` FOREIGN KEY (`equipo_id`) REFERENCES `admin_equipos` (`id`),
  CONSTRAINT `admin_equipo_asignado_origen_bodega_id_foreign` FOREIGN KEY (`origen_bodega_id`) REFERENCES `admin_bodegas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_equipo_asignado`
--

LOCK TABLES `admin_equipo_asignado` WRITE;
/*!40000 ALTER TABLE `admin_equipo_asignado` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_equipo_asignado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_equipos`
--

DROP TABLE IF EXISTS `admin_equipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_equipos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `categoria_id` bigint unsigned DEFAULT NULL,
  `sub_categoria_id` bigint unsigned DEFAULT NULL,
  `medida_id` bigint unsigned DEFAULT NULL,
  `precio_cliente_unidad` decimal(14,2) DEFAULT NULL,
  `porcentaje_depreciacion` decimal(14,2) DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_equipos_categoria_id_foreign` (`categoria_id`),
  KEY `admin_equipos_sub_categoria_id_foreign` (`sub_categoria_id`),
  KEY `admin_equipos_medida_id_foreign` (`medida_id`),
  CONSTRAINT `admin_equipos_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `admin_categories` (`id`),
  CONSTRAINT `admin_equipos_medida_id_foreign` FOREIGN KEY (`medida_id`) REFERENCES `admin_unidades` (`id`),
  CONSTRAINT `admin_equipos_sub_categoria_id_foreign` FOREIGN KEY (`sub_categoria_id`) REFERENCES `admin_subCategories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_equipos`
--

LOCK TABLES `admin_equipos` WRITE;
/*!40000 ALTER TABLE `admin_equipos` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_equipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_facturas`
--

DROP TABLE IF EXISTS `admin_facturas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_facturas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `proveedor_id` bigint unsigned DEFAULT NULL,
  `numero` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serie` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` decimal(14,2) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `info` text COLLATE utf8mb4_unicode_ci,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bodega_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'producto',
  PRIMARY KEY (`id`),
  KEY `admin_facturas_proveedor_id_foreign` (`proveedor_id`),
  KEY `admin_facturas_bodega_id_foreign` (`bodega_id`),
  CONSTRAINT `admin_facturas_bodega_id_foreign` FOREIGN KEY (`bodega_id`) REFERENCES `admin_bodegas` (`id`),
  CONSTRAINT `admin_facturas_proveedor_id_foreign` FOREIGN KEY (`proveedor_id`) REFERENCES `admin_providers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_facturas`
--

LOCK TABLES `admin_facturas` WRITE;
/*!40000 ALTER TABLE `admin_facturas` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_facturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_horary`
--

DROP TABLE IF EXISTS `admin_horary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_horary` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_horary_manager` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry` time DEFAULT NULL,
  `exit` time DEFAULT NULL,
  `type` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_horary`
--

LOCK TABLES `admin_horary` WRITE;
/*!40000 ALTER TABLE `admin_horary` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_horary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_mensajes`
--

DROP TABLE IF EXISTS `admin_mensajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_mensajes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `de_admin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `para_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `de_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `para_admin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `de` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `para` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mensaje` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `status` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `respuestas` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_mensajes`
--

LOCK TABLES `admin_mensajes` WRITE;
/*!40000 ALTER TABLE `admin_mensajes` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_mensajes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_petitions_complaintments`
--

DROP TABLE IF EXISTS `admin_petitions_complaintments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_petitions_complaintments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `reponse` text COLLATE utf8mb4_unicode_ci,
  `responseDate` date DEFAULT NULL,
  `idUser` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_petitions_complaintments_iduser_foreign` (`idUser`),
  CONSTRAINT `admin_petitions_complaintments_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `admin_colaboradores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_petitions_complaintments`
--

LOCK TABLES `admin_petitions_complaintments` WRITE;
/*!40000 ALTER TABLE `admin_petitions_complaintments` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_petitions_complaintments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_petitions_economicpetitions`
--

DROP TABLE IF EXISTS `admin_petitions_economicpetitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_petitions_economicpetitions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `typos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `applyTo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otherAplyText` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `descript` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `reponse` text COLLATE utf8mb4_unicode_ci,
  `responseDate` date DEFAULT NULL,
  `authorized` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idUser` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_petitions_economicpetitions_iduser_foreign` (`idUser`),
  CONSTRAINT `admin_petitions_economicpetitions_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `admin_colaboradores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_petitions_economicpetitions`
--

LOCK TABLES `admin_petitions_economicpetitions` WRITE;
/*!40000 ALTER TABLE `admin_petitions_economicpetitions` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_petitions_economicpetitions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_petitions_restoretimepermissions`
--

DROP TABLE IF EXISTS `admin_petitions_restoretimepermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_petitions_restoretimepermissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `dateOut` date DEFAULT NULL,
  `dateEnter` date DEFAULT NULL,
  `deteRepEnter` date DEFAULT NULL,
  `deteRepOut` date DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `typos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `daysapply` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `reponse` text COLLATE utf8mb4_unicode_ci,
  `responseDate` date DEFAULT NULL,
  `idUser` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_petitions_restoretimepermissions_iduser_foreign` (`idUser`),
  CONSTRAINT `admin_petitions_restoretimepermissions_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `admin_colaboradores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_petitions_restoretimepermissions`
--

LOCK TABLES `admin_petitions_restoretimepermissions` WRITE;
/*!40000 ALTER TABLE `admin_petitions_restoretimepermissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_petitions_restoretimepermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_petitions_simplepetitions`
--

DROP TABLE IF EXISTS `admin_petitions_simplepetitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_petitions_simplepetitions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_petitions_simplepetitions`
--

LOCK TABLES `admin_petitions_simplepetitions` WRITE;
/*!40000 ALTER TABLE `admin_petitions_simplepetitions` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_petitions_simplepetitions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_petitions_simplepetitions_save`
--

DROP TABLE IF EXISTS `admin_petitions_simplepetitions_save`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_petitions_simplepetitions_save` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `reponse` text COLLATE utf8mb4_unicode_ci,
  `responseDate` date DEFAULT NULL,
  `idUser` bigint unsigned DEFAULT NULL,
  `idType` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_petitions_simplepetitions_save_iduser_foreign` (`idUser`),
  KEY `admin_petitions_simplepetitions_save_idtype_foreign` (`idType`),
  CONSTRAINT `admin_petitions_simplepetitions_save_idtype_foreign` FOREIGN KEY (`idType`) REFERENCES `admin_petitions_simplepetitions` (`id`),
  CONSTRAINT `admin_petitions_simplepetitions_save_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `admin_colaboradores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_petitions_simplepetitions_save`
--

LOCK TABLES `admin_petitions_simplepetitions_save` WRITE;
/*!40000 ALTER TABLE `admin_petitions_simplepetitions_save` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_petitions_simplepetitions_save` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_petitions_timepermissions`
--

DROP TABLE IF EXISTS `admin_petitions_timepermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_petitions_timepermissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `datePerm` date DEFAULT NULL,
  `typos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `typeOther` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `days` int DEFAULT NULL,
  `status` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `reponse` text COLLATE utf8mb4_unicode_ci,
  `responseDate` date DEFAULT NULL,
  `idUser` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_petitions_timepermissions_iduser_foreign` (`idUser`),
  CONSTRAINT `admin_petitions_timepermissions_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `admin_colaboradores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_petitions_timepermissions`
--

LOCK TABLES `admin_petitions_timepermissions` WRITE;
/*!40000 ALTER TABLE `admin_petitions_timepermissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_petitions_timepermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_productos`
--

DROP TABLE IF EXISTS `admin_productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_productos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `categoria_id` bigint unsigned DEFAULT NULL,
  `sub_categoria_id` bigint unsigned DEFAULT NULL,
  `medida_final_id` bigint unsigned DEFAULT NULL,
  `cantidad_minima_aceptable` bigint DEFAULT NULL,
  `porcentaje_ganancia` decimal(14,2) DEFAULT NULL,
  `precio_cliente` decimal(14,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_productos_categoria_id_foreign` (`categoria_id`),
  KEY `admin_productos_sub_categoria_id_foreign` (`sub_categoria_id`),
  KEY `admin_productos_medida_final_id_foreign` (`medida_final_id`),
  CONSTRAINT `admin_productos_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `admin_categories` (`id`),
  CONSTRAINT `admin_productos_medida_final_id_foreign` FOREIGN KEY (`medida_final_id`) REFERENCES `admin_unidades` (`id`),
  CONSTRAINT `admin_productos_sub_categoria_id_foreign` FOREIGN KEY (`sub_categoria_id`) REFERENCES `admin_subCategories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_productos`
--

LOCK TABLES `admin_productos` WRITE;
/*!40000 ALTER TABLE `admin_productos` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_products`
--

DROP TABLE IF EXISTS `admin_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_products` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lot` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `min_dispatch` int NOT NULL,
  `max_dispatch` int NOT NULL,
  `min_stock` int NOT NULL,
  `provider` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateIn` date NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `sizes` text COLLATE utf8mb4_unicode_ci,
  `colors` text COLLATE utf8mb4_unicode_ci,
  `category` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subcategory` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_products`
--

LOCK TABLES `admin_products` WRITE;
/*!40000 ALTER TABLE `admin_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_providers`
--

DROP TABLE IF EXISTS `admin_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_providers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nit` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contacto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_providers`
--

LOCK TABLES `admin_providers` WRITE;
/*!40000 ALTER TABLE `admin_providers` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_puestos`
--

DROP TABLE IF EXISTS `admin_puestos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_puestos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `payDT` int NOT NULL,
  `payNT` int NOT NULL,
  `payEDT` int NOT NULL,
  `payENT` int NOT NULL,
  `extraPay` int NOT NULL,
  `perDiem` int NOT NULL,
  `movil` int NOT NULL,
  `points` int NOT NULL,
  `id_task_manager` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_horary_manager` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_puestos`
--

LOCK TABLES `admin_puestos` WRITE;
/*!40000 ALTER TABLE `admin_puestos` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_puestos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_roles`
--

DROP TABLE IF EXISTS `admin_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nameRole` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_roles`
--

LOCK TABLES `admin_roles` WRITE;
/*!40000 ALTER TABLE `admin_roles` DISABLE KEYS */;
INSERT INTO `admin_roles` VALUES (1,'Sadmin','su00','2020-09-11 13:04:39','2021-02-13 16:45:44');
/*!40000 ALTER TABLE `admin_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_sizes`
--

DROP TABLE IF EXISTS `admin_sizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_sizes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `namesize` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_sizes`
--

LOCK TABLES `admin_sizes` WRITE;
/*!40000 ALTER TABLE `admin_sizes` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_sizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_solicitudes`
--

DROP TABLE IF EXISTS `admin_solicitudes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_solicitudes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reposicion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descuento` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_solicitudes`
--

LOCK TABLES `admin_solicitudes` WRITE;
/*!40000 ALTER TABLE `admin_solicitudes` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_solicitudes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_stock`
--

DROP TABLE IF EXISTS `admin_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_stock` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `producto_id` bigint unsigned DEFAULT NULL,
  `cantidad` bigint DEFAULT NULL,
  `medida_id` bigint unsigned DEFAULT NULL,
  `costo_unitario` decimal(14,2) DEFAULT NULL,
  `valor_total` decimal(14,2) DEFAULT NULL,
  `bodega_id` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_stock_producto_id_foreign` (`producto_id`),
  KEY `admin_stock_medida_id_foreign` (`medida_id`),
  KEY `admin_stock_bodega_id_foreign` (`bodega_id`),
  CONSTRAINT `admin_stock_bodega_id_foreign` FOREIGN KEY (`bodega_id`) REFERENCES `admin_bodegas` (`id`),
  CONSTRAINT `admin_stock_medida_id_foreign` FOREIGN KEY (`medida_id`) REFERENCES `admin_unidades` (`id`),
  CONSTRAINT `admin_stock_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `admin_productos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_stock`
--

LOCK TABLES `admin_stock` WRITE;
/*!40000 ALTER TABLE `admin_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_stock_egresos`
--

DROP TABLE IF EXISTS `admin_stock_egresos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_stock_egresos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `producto_id` bigint unsigned DEFAULT NULL,
  `cantidad_medida` bigint DEFAULT NULL,
  `medida_id` bigint unsigned DEFAULT NULL,
  `cantidad` bigint DEFAULT NULL,
  `medida` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `costo_unitario` decimal(14,2) DEFAULT NULL,
  `valor_total` decimal(14,2) DEFAULT NULL,
  `precio_unitario` decimal(14,2) DEFAULT NULL,
  `precio_total` decimal(14,2) DEFAULT NULL,
  `tipo_egreso` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_cobro` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cuotas` int DEFAULT NULL,
  `bodega_id` bigint unsigned DEFAULT NULL,
  `receptor_cliente_id` bigint unsigned DEFAULT NULL,
  `receptor_colaborador_id` bigint unsigned DEFAULT NULL,
  `receptor_bodega_id` bigint unsigned DEFAULT NULL,
  `cobrar_cliente_id` bigint unsigned DEFAULT NULL,
  `cobrar_colaborador_id` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_stock_egresos_producto_id_foreign` (`producto_id`),
  KEY `admin_stock_egresos_medida_id_foreign` (`medida_id`),
  KEY `admin_stock_egresos_bodega_id_foreign` (`bodega_id`),
  KEY `admin_stock_egresos_receptor_bodega_id_foreign` (`receptor_bodega_id`),
  KEY `admin_stock_egresos_receptor_cliente_id_foreign` (`receptor_cliente_id`),
  KEY `admin_stock_egresos_receptor_colaborador_id_foreign` (`receptor_colaborador_id`),
  KEY `admin_stock_egresos_cobrar_cliente_id_foreign` (`cobrar_cliente_id`),
  KEY `admin_stock_egresos_cobrar_colaborador_id_foreign` (`cobrar_colaborador_id`),
  CONSTRAINT `admin_stock_egresos_bodega_id_foreign` FOREIGN KEY (`bodega_id`) REFERENCES `admin_bodegas` (`id`),
  CONSTRAINT `admin_stock_egresos_cobrar_cliente_id_foreign` FOREIGN KEY (`cobrar_cliente_id`) REFERENCES `admin_company_clients` (`id`),
  CONSTRAINT `admin_stock_egresos_cobrar_colaborador_id_foreign` FOREIGN KEY (`cobrar_colaborador_id`) REFERENCES `admin_colaboradores` (`id`),
  CONSTRAINT `admin_stock_egresos_medida_id_foreign` FOREIGN KEY (`medida_id`) REFERENCES `admin_unidades` (`id`),
  CONSTRAINT `admin_stock_egresos_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `admin_productos` (`id`),
  CONSTRAINT `admin_stock_egresos_receptor_bodega_id_foreign` FOREIGN KEY (`receptor_bodega_id`) REFERENCES `admin_bodegas` (`id`),
  CONSTRAINT `admin_stock_egresos_receptor_cliente_id_foreign` FOREIGN KEY (`receptor_cliente_id`) REFERENCES `admin_company_clients` (`id`),
  CONSTRAINT `admin_stock_egresos_receptor_colaborador_id_foreign` FOREIGN KEY (`receptor_colaborador_id`) REFERENCES `admin_colaboradores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_stock_egresos`
--

LOCK TABLES `admin_stock_egresos` WRITE;
/*!40000 ALTER TABLE `admin_stock_egresos` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_stock_egresos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_stock_egresos_equipo`
--

DROP TABLE IF EXISTS `admin_stock_egresos_equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_stock_egresos_equipo` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `equipo_id` bigint unsigned DEFAULT NULL,
  `cantidad` bigint DEFAULT NULL,
  `costo_unidad` decimal(14,2) DEFAULT NULL,
  `costo_total` decimal(14,2) DEFAULT NULL,
  `precio_total` decimal(14,2) DEFAULT NULL,
  `bodega_id` bigint unsigned DEFAULT NULL,
  `cargo_a_cliente_id` bigint unsigned DEFAULT NULL,
  `cargo_a_colaborador_id` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_stock_egresos_equipo_equipo_id_foreign` (`equipo_id`),
  KEY `admin_stock_egresos_equipo_bodega_id_foreign` (`bodega_id`),
  KEY `admin_stock_egresos_equipo_cargo_a_cliente_id_foreign` (`cargo_a_cliente_id`),
  KEY `admin_stock_egresos_equipo_cargo_a_colaborador_id_foreign` (`cargo_a_colaborador_id`),
  CONSTRAINT `admin_stock_egresos_equipo_bodega_id_foreign` FOREIGN KEY (`bodega_id`) REFERENCES `admin_bodegas` (`id`),
  CONSTRAINT `admin_stock_egresos_equipo_cargo_a_cliente_id_foreign` FOREIGN KEY (`cargo_a_cliente_id`) REFERENCES `admin_company_clients` (`id`),
  CONSTRAINT `admin_stock_egresos_equipo_cargo_a_colaborador_id_foreign` FOREIGN KEY (`cargo_a_colaborador_id`) REFERENCES `admin_colaboradores` (`id`),
  CONSTRAINT `admin_stock_egresos_equipo_equipo_id_foreign` FOREIGN KEY (`equipo_id`) REFERENCES `admin_equipos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_stock_egresos_equipo`
--

LOCK TABLES `admin_stock_egresos_equipo` WRITE;
/*!40000 ALTER TABLE `admin_stock_egresos_equipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_stock_egresos_equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_stock_equipos`
--

DROP TABLE IF EXISTS `admin_stock_equipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_stock_equipos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `equipo_id` bigint unsigned DEFAULT NULL,
  `cantidad` bigint DEFAULT NULL,
  `medida_id` bigint unsigned DEFAULT NULL,
  `costo_unitario` decimal(14,2) DEFAULT NULL,
  `valor_total` decimal(14,2) DEFAULT NULL,
  `bodega_id` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'activo',
  PRIMARY KEY (`id`),
  KEY `admin_stock_equipos_equipo_id_foreign` (`equipo_id`),
  KEY `admin_stock_equipos_bodega_id_foreign` (`bodega_id`),
  KEY `admin_stock_equipos_medida_id_foreign` (`medida_id`),
  CONSTRAINT `admin_stock_equipos_bodega_id_foreign` FOREIGN KEY (`bodega_id`) REFERENCES `admin_bodegas` (`id`),
  CONSTRAINT `admin_stock_equipos_equipo_id_foreign` FOREIGN KEY (`equipo_id`) REFERENCES `admin_equipos` (`id`),
  CONSTRAINT `admin_stock_equipos_medida_id_foreign` FOREIGN KEY (`medida_id`) REFERENCES `admin_unidades` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_stock_equipos`
--

LOCK TABLES `admin_stock_equipos` WRITE;
/*!40000 ALTER TABLE `admin_stock_equipos` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_stock_equipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_stock_ingresos`
--

DROP TABLE IF EXISTS `admin_stock_ingresos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_stock_ingresos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `producto_id` bigint unsigned DEFAULT NULL,
  `cantidad_medida` bigint DEFAULT NULL,
  `medida_id` bigint unsigned DEFAULT NULL,
  `cantidad` bigint DEFAULT NULL,
  `medida` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `costo_unitario` decimal(14,2) DEFAULT NULL,
  `valor_total` decimal(14,2) DEFAULT NULL,
  `bodega_id` bigint unsigned DEFAULT NULL,
  `factura_id` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_stock_ingresos_producto_id_foreign` (`producto_id`),
  KEY `admin_stock_ingresos_medida_id_foreign` (`medida_id`),
  KEY `admin_stock_ingresos_bodega_id_foreign` (`bodega_id`),
  KEY `admin_stock_ingresos_factura_id_foreign` (`factura_id`),
  CONSTRAINT `admin_stock_ingresos_bodega_id_foreign` FOREIGN KEY (`bodega_id`) REFERENCES `admin_bodegas` (`id`),
  CONSTRAINT `admin_stock_ingresos_factura_id_foreign` FOREIGN KEY (`factura_id`) REFERENCES `admin_facturas` (`id`),
  CONSTRAINT `admin_stock_ingresos_medida_id_foreign` FOREIGN KEY (`medida_id`) REFERENCES `admin_unidades` (`id`),
  CONSTRAINT `admin_stock_ingresos_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `admin_productos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_stock_ingresos`
--

LOCK TABLES `admin_stock_ingresos` WRITE;
/*!40000 ALTER TABLE `admin_stock_ingresos` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_stock_ingresos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_stock_ingresos_equipo`
--

DROP TABLE IF EXISTS `admin_stock_ingresos_equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_stock_ingresos_equipo` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `equipo_id` bigint unsigned DEFAULT NULL,
  `cantidad` bigint DEFAULT NULL,
  `costo_unidad` decimal(16,2) DEFAULT NULL,
  `costo_total` decimal(16,2) DEFAULT NULL,
  `bodega_id` bigint unsigned DEFAULT NULL,
  `factura_id` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_stock_ingresos_equipo_equipo_id_foreign` (`equipo_id`),
  KEY `admin_stock_ingresos_equipo_bodega_id_foreign` (`bodega_id`),
  KEY `admin_stock_ingresos_equipo_factura_id_foreign` (`factura_id`),
  CONSTRAINT `admin_stock_ingresos_equipo_bodega_id_foreign` FOREIGN KEY (`bodega_id`) REFERENCES `admin_bodegas` (`id`),
  CONSTRAINT `admin_stock_ingresos_equipo_equipo_id_foreign` FOREIGN KEY (`equipo_id`) REFERENCES `admin_equipos` (`id`),
  CONSTRAINT `admin_stock_ingresos_equipo_factura_id_foreign` FOREIGN KEY (`factura_id`) REFERENCES `admin_facturas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_stock_ingresos_equipo`
--

LOCK TABLES `admin_stock_ingresos_equipo` WRITE;
/*!40000 ALTER TABLE `admin_stock_ingresos_equipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_stock_ingresos_equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_subCategories`
--

DROP TABLE IF EXISTS `admin_subCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_subCategories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `subCategory` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_subcategories_category_id_foreign` (`category_id`),
  CONSTRAINT `admin_subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `admin_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_subCategories`
--

LOCK TABLES `admin_subCategories` WRITE;
/*!40000 ALTER TABLE `admin_subCategories` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_subCategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_tasks`
--

DROP TABLE IF EXISTS `admin_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_tasks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_task_manager` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `count` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tasks`
--

LOCK TABLES `admin_tasks` WRITE;
/*!40000 ALTER TABLE `admin_tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_unidades`
--

DROP TABLE IF EXISTS `admin_unidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_unidades` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_unidades`
--

LOCK TABLES `admin_unidades` WRITE;
/*!40000 ALTER TABLE `admin_unidades` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_unidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usersys` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statusUs` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roleUS` bigint unsigned NOT NULL,
  `superuser` int NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_usersys_unique` (`usersys`),
  KEY `admin_users_roleus_foreign` (`roleUS`),
  CONSTRAINT `admin_users_roleus_foreign` FOREIGN KEY (`roleUS`) REFERENCES `admin_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (1,'Sistema Admin','sadmin','$2y$10$3aCx1cm1Sriq4ImSZrQKdO6Ms4cMJxiCOA3XzrwCRj7Fu0ztas0Lu','1',NULL,'su00',1,1,'2020-09-11 13:04:39','2021-02-13 17:09:34',NULL);
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_usersadmin`
--

DROP TABLE IF EXISTS `admin_usersadmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_usersadmin` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `sysuser` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sysuserpass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `typeuser` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_usersadmin`
--

LOCK TABLES `admin_usersadmin` WRITE;
/*!40000 ALTER TABLE `admin_usersadmin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_usersadmin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_warehouse`
--

DROP TABLE IF EXISTS `admin_warehouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_warehouse` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lon` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_warehouse`
--

LOCK TABLES `admin_warehouse` WRITE;
/*!40000 ALTER TABLE `admin_warehouse` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_warehouse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_werehouse_products`
--

DROP TABLE IF EXISTS `admin_werehouse_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_werehouse_products` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_werhouse` bigint unsigned NOT NULL,
  `id_product` bigint unsigned NOT NULL,
  `amount` int NOT NULL,
  `lot` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `datein` date NOT NULL,
  `whom` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_werehouse_products_whom_foreign` (`whom`),
  KEY `admin_werehouse_products_id_werhouse_foreign` (`id_werhouse`),
  KEY `admin_werehouse_products_id_product_foreign` (`id_product`),
  CONSTRAINT `admin_werehouse_products_id_product_foreign` FOREIGN KEY (`id_product`) REFERENCES `admin_products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `admin_werehouse_products_id_werhouse_foreign` FOREIGN KEY (`id_werhouse`) REFERENCES `admin_warehouse` (`id`) ON DELETE CASCADE,
  CONSTRAINT `admin_werehouse_products_whom_foreign` FOREIGN KEY (`whom`) REFERENCES `admin_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_werehouse_products`
--

LOCK TABLES `admin_werehouse_products` WRITE;
/*!40000 ALTER TABLE `admin_werehouse_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_werehouse_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loggs_user`
--

DROP TABLE IF EXISTS `loggs_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loggs_user` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `latitude` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitud` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitudeExit` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitudExit` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitudeEnterBtw` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitudEnterBtw` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitudeExitBtw` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitudExitBtw` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ipClient` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refrence` text COLLATE utf8mb4_unicode_ci,
  `ontime` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `onRange` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statusDay` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hourEnter` time DEFAULT '00:00:00',
  `hourExit` time DEFAULT '00:00:00',
  `hourEnterBetw` time DEFAULT '00:00:00',
  `hourExitBetw` time DEFAULT '00:00:00',
  `befTimeEnter` time DEFAULT '00:00:00',
  `befTimeEnterTxt` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `outTimeEnter` time DEFAULT '00:00:00',
  `outTimeEnterTxt` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hourIdeal` time DEFAULT '00:00:00',
  `hourDay` time DEFAULT '00:00:00',
  `payDay` double(10,2) DEFAULT NULL,
  `extraHourDay` time DEFAULT '00:00:00',
  `extraHourDayMiddle` double(10,2) DEFAULT NULL,
  `extraPayDay` double(10,2) DEFAULT NULL,
  `hourNight` time DEFAULT '00:00:00',
  `extraHourNight` time DEFAULT '00:00:00',
  `extraHourNightMiddle` double(10,2) DEFAULT NULL,
  `extraPayNight` double(10,2) DEFAULT NULL,
  `extraHourEspecial` time DEFAULT '00:00:00',
  `extraPayEspec` double(10,2) DEFAULT NULL,
  `aproved` int DEFAULT NULL,
  `dayWeek` varchar(29) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateDay` date DEFAULT NULL,
  `delay` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dayLab` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idFollower` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `viatics` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viaticsAdded` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tasks` text COLLATE utf8mb4_unicode_ci,
  `quantityTask` int DEFAULT NULL,
  `baseTasks` int DEFAULT NULL,
  `priceTask` int DEFAULT NULL,
  `complTask` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aditTask` int DEFAULT NULL,
  `lessTask` int DEFAULT NULL,
  `priceAdiTask` int DEFAULT NULL,
  `userId` bigint unsigned DEFAULT NULL,
  `idArea` bigint unsigned DEFAULT NULL,
  `idBranch` bigint unsigned DEFAULT NULL,
  `idClient` bigint unsigned DEFAULT NULL,
  `idLink` bigint unsigned DEFAULT NULL,
  `idDay` bigint unsigned DEFAULT NULL,
  `idCompany` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `loggs_user_userid_foreign` (`userId`),
  KEY `loggs_user_idarea_foreign` (`idArea`),
  KEY `loggs_user_idbranch_foreign` (`idBranch`),
  KEY `loggs_user_idclient_foreign` (`idClient`),
  KEY `loggs_user_idlink_foreign` (`idLink`),
  KEY `loggs_user_idday_foreign` (`idDay`),
  KEY `loggs_user_idcompany_foreign` (`idCompany`),
  CONSTRAINT `loggs_user_idarea_foreign` FOREIGN KEY (`idArea`) REFERENCES `admin_company_clients_branch_area` (`id`) ON DELETE RESTRICT,
  CONSTRAINT `loggs_user_idbranch_foreign` FOREIGN KEY (`idBranch`) REFERENCES `admin_company_clients_branchof` (`id`) ON DELETE RESTRICT,
  CONSTRAINT `loggs_user_idclient_foreign` FOREIGN KEY (`idClient`) REFERENCES `admin_company_clients` (`id`) ON DELETE RESTRICT,
  CONSTRAINT `loggs_user_idcompany_foreign` FOREIGN KEY (`idCompany`) REFERENCES `admin_company` (`id`) ON DELETE RESTRICT,
  CONSTRAINT `loggs_user_idday_foreign` FOREIGN KEY (`idDay`) REFERENCES `admin_horary` (`id`) ON DELETE RESTRICT,
  CONSTRAINT `loggs_user_idlink_foreign` FOREIGN KEY (`idLink`) REFERENCES `admin_client_colab_assoc` (`id`) ON DELETE RESTRICT,
  CONSTRAINT `loggs_user_userid_foreign` FOREIGN KEY (`userId`) REFERENCES `admin_colaboradores` (`id`) ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loggs_user`
--

LOCK TABLES `loggs_user` WRITE;
/*!40000 ALTER TABLE `loggs_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `loggs_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2019_12_14_000001_create_personal_access_tokens_table',1),(2,'2021_02_10_004756_create_admin_roles_table',1),(3,'2021_02_10_005223_create_admin_users_table',1),(4,'2021_02_10_230758_create_admin_access_table',1),(5,'2021_02_10_233048_create_admin_access_roles_table',1),(6,'2021_02_20_010744_create_admin_products_table',1),(7,'2021_02_20_165149_create_admin_warehouse_table',1),(8,'2021_02_22_192957_create_admin_sizes_table',1),(9,'2021_02_22_193311_create_admin_colors_table',1),(10,'2021_02_22_194656_create_admin_werehouse_products_table',1),(11,'2021_05_27_120000_create_admin_categories_table',1),(12,'2021_05_27_120000_create_admin_subCategories_table',1),(13,'2021_08_10_162200_create_admin_puestos_table',1),(14,'2021_08_23_120000_create_admin_providers_table',1),(15,'2021_08_23_120000_create_admin_solicitudes_table',1),(16,'2021_09_27_120000_create_admin_colaboradores_table',1),(17,'2021_09_27_191338_create_admin_usersadmin_table',1),(18,'2021_09_28_014335_create_admin_comanies_table',1),(19,'2021_09_28_065247_create_admin_company_clients_table',1),(20,'2021_09_28_065347_create_admin_company_client_assoc_table',1),(21,'2021_09_28_164934_create_admin_company_clients_jobs_table',1),(22,'2021_09_28_170026_create_admin_company_clients_jobs_todo_table',1),(23,'2021_09_29_025533_create_admin_company_clients_branchof_table',1),(24,'2021_09_29_030059_update_admin_puestos__table',1),(25,'2021_09_29_120000_create_admin_client_colab_assoc_table',1),(26,'2021_09_29_120000_create_admin_company_colab_assoc_table',1),(27,'2021_09_29_120000_create_admin_documentos_table',1),(28,'2021_10_04_194253_update_admin_company_table',1),(29,'2021_10_07_120000_create_admin_bancos_table',1),(30,'2021_10_07_130000_create_admin_contratos_table',1),(31,'2021_10_08_150000_update_admin_company2_table',1),(32,'2021_10_26_120000_update_admin_colaboradores_table',1),(33,'2021_10_26_130000_update_admin_company3_table',1),(34,'2021_10_27_120000_create_admin_archivos_table',1),(35,'2021_10_27_120000_update_admin_company_clients_table',1),(36,'2021_10_29_120000_create_admin_horary_table',1),(37,'2021_10_29_120000_create_admin_tasks_table',1),(38,'2021_10_29_130000_update_admin_puestos_table2',1),(39,'2021_11_02_090000_create_admin_company_clients_branch_area_table',1),(40,'2021_11_02_100000_update_admin_tasks_table',1),(41,'2021_11_02_120000_create_admin_changeLog_table',1),(42,'2021_11_04_234548_update_admin_colaboradores2_table',1),(43,'2021_11_05_100000_update_admin_company4_table',1),(44,'2021_11_05_235008_create_loggs_user_table',1),(45,'2021_11_08_120000_update_admin_client_colab_assoc_table',1),(46,'2021_11_10_110000_create_admin_unidades_table',1),(47,'2021_11_10_120000_create_admin_bodegas_table',1),(48,'2021_11_10_120000_create_admin_facturas_table',1),(49,'2021_11_10_120000_create_admin_productos_table',1),(50,'2021_11_10_120000_update_admin_bodegas_table',1),(51,'2021_11_10_120000_update_admin_company5_table',1),(52,'2021_11_10_130000_create_admin_stock_table',1),(53,'2021_11_10_130000_update_admin_company_clients_branchof_table',1),(54,'2021_11_10_140000_create_admin_stock_egresos_table',1),(55,'2021_11_10_140000_create_admin_stock_ingresos_table',1),(56,'2021_11_11_120000_create_admin_conversiones_table',1),(57,'2021_11_14_032010_update_admin_colaboradores3_table',1),(58,'2021_11_19_110000_create_admin_equipos_table',1),(59,'2021_11_19_120000_create_admin_equipo_asignado_table',1),(60,'2021_11_19_120000_create_admin_stock_egresos_equipo_table',1),(61,'2021_11_19_120000_create_admin_stock_equipos_table',1),(62,'2021_11_19_120000_create_admin_stock_ingresos_equipo_table',1),(63,'2021_11_19_120000_update_admin_facturas_table',1),(64,'2021_11_26_153618_create_admin_restoretimepermissions_table',1),(65,'2021_11_26_154948_create_admin_timepermissions_table',1),(66,'2021_11_26_160332_create_admin_economicpetitions_table',1),(67,'2021_11_26_160815_create_admin_simplepetitions_table',1),(68,'2021_11_26_161046_create_admin_simplepetitions_save_table',1),(69,'2021_11_26_161500_create_admin_complaintments_table',1),(70,'2021_11_29_120000_update_admin_changeLog_table',1),(71,'2021_11_30_120000_update_admin_stock_equipos_table',1),(72,'2021_12_08_120000_create_admin_alertas_table',1),(73,'2021_12_11_122350_create_admin_cumples_table',1),(74,'2021_12_11_122438_create_admin_mensajes_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-06  0:33:47
