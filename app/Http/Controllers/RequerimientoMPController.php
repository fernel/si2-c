<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\frontModels\RequerimientoMPModel;
use App\frontModels\SolicitudesModel;
use Auth;

class RequerimientoMPController extends Controller
{
    public function __construct(){
        $this->middleware('auth:front');
        $this->baseModel = new RequerimientoMPModel;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!$this->restrict_access()){
            return redirect('/home');
        }

        $data = RequerimientoMPModel::get();

        return view('front.requerimientosMP.index',[
            'menubar' => $this->list_sidebar_front(),
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only($this->baseModel->getFillable());
        $this->baseModel->fill($data)->save();

        $from = date('Y-m-d') . " 00:00:00";
        $to = date('Y-m-d') . " 23:59:59";

        $solicitudes_data = SolicitudesModel::whereBetween('created_at', [$from, $to])->get();

        $correlativo = count($solicitudes_data) + 1;

        $codigo = date("dmY", time()) . "-" . $this->formatear_correlativo($correlativo);

        // dd($codigo);

        $data2 = [
            'codigo' => $codigo,
            'created_by_front_user_id' => Auth::guard('front')->user()->id,
            'tipo_formulario' => 'requerimiento_MP',
            'formulario_id' => $this->baseModel->id,
            'estado' => 'pendiente'
        ];
        $solicitud = new SolicitudesModel();
        $solicitud->fill($data2)->save();



        return redirect()->back()->with('success','Guardado correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = $this->baseModel::find($id);
        $data = $request->only($this->baseModel->getFillable());
        $model->fill($data)->save();
        return redirect()->back()->with('info','Actualizado correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
        $this->baseModel::destroy($id);
            return redirect()->back()->with('warning','Borrado correctamente');
        }catch (\Exception $e) {
            return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
        }
    }

    public function formatear_correlativo($num){
        if ($num < 10){
            $cad = "00" . $num;
        }elseif($num < 100){
            $cad = "0" . $num;
        }
        return $cad;
    }
}
