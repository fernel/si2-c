<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\frontModels\SolicitudesModel;

use App\adminModels\colaboradores;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

        $this->middleware('auth:front');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      //
      // dd(UserAdmin::find($request->user()->id_prson)->role->codaccus);
      // dd($this->list_sidebar());
        // session[]

        /*
        return view('admin.home')->
               with('menubar', $this->list_sidebar());
        */
        $solicitudes = solicitudesModel::get();
        return view('front.home', [
            'menubar'=> $this->list_sidebar_front(),
            'solicitudes' => $solicitudes
        ]);

    }

}
