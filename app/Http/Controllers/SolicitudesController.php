<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\frontModels\SolicitudesModel;
use App\frontModels\UserFront;
use App\frontModels\RequerimientoMPModel;
use App\adminModels\UserAdmin;
use App\adminModels\divisionModel;
use App\adminModels\departamentosModel;
use App\adminModels\seccionModel;
use App\adminModels\oficinaModel;
use Auth;

class SolicitudesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:front');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!$this->restrict_access()){
            return redirect('/home');
        }

        $query_usuarios = userFront::get();
        $usuarios = [[null, '---']];
        foreach($query_usuarios as $qu){
            if ( $qu->id != Auth::user()->id){
                $usuarios[] = [
                    $qu->id,
                    $qu->usersys,
                ];
            }
        }

        $query_division = divisionModel::get();
        $query_departamentos = departamentosModel::get();
        $query_seccion = seccionModel::get();
        $query_oficina = oficinaModel::get();
        $departamentos = [[null, '---']];
        foreach($query_division as $qu){
            $division[] = [
                $qu->id,
                $qu->nombre . " (división)",
            ];
        }
        foreach($query_departamentos as $qu){
            $departamentos[] = [
                $qu->id,
                $qu->nombre . " (departamento)",
            ];
        }
        foreach($query_seccion as $qu){
            $seccion[] = [
                $qu->id,
                $qu->nombre . " (sección)",
            ];
        }
        foreach($query_oficina as $qu){
            $oficina[] = [
                $qu->id,
                $qu->nombre . " (oficina)",
            ];
        }

        $tipo_asignacion = [
            ['Persona'],
            ['Division'],
            ['Departamento'],
            ['Seccion'],
            ['Oficina'],
        ];

        $data = SolicitudesModel::get();

        foreach($data as $temp){
            $temp['userFront'] = UserFront::find($temp['created_by_front_user_id']);
            // $temp['userAdmin'] = UserFront::find($temp['admin_user_id']);
            $temp['asig'] = isset($tipo_asignacion[$temp['tipo_asignacion']][0]) ? $tipo_asignacion[$temp['tipo_asignacion']][0] : '';

            $asig = $temp['tipo_asignacion'];
            if ($asig == 0){
                $query = userFront::find($temp['asignado_id']);
                $temp['gente'] = isset($query->usersys) ? $query->usersys : '';
            }elseif($asig == 1){
                $query = divisionModel::find($temp['asignado_id']);
                $temp['gente'] = isset($query->nombre) ? $query->nombre : '';
            }elseif($asig == 2){
                $query = departamentosModel::find($temp['asignado_id']);
                $temp['gente'] = isset($query->nombre) ? $query->nombre : '';
            }elseif($asig == 3){
                $query = seccionModel::find($temp['asignado_id']);
                $temp['gente'] = isset($query->nombre) ? $query->nombre : '';
            }elseif($asig == 4){
                $query = oficinaModel::find($temp['asignado_id']);
                $temp['gente'] = isset($query->nombre) ? $query->nombre : '';
            }

            if ($temp['tipo_formulario'] == "requerimiento_MP"){
                $req = RequerimientoMPModel::find($temp->formulario_id);
                // dd($req);
                $temp["info_formulario"] = 'Número: ' . $req->numero;
            }

        }


        return view('front.solicitudes.index',[
            'menubar' => $this->list_sidebar_front(),
            'data' => $data,
            'usuarios' => $usuarios,
            'division' => $division,
            'departamentos' => $departamentos,
            'seccion' => $seccion,
            'oficina' => $oficina,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = new solicitudesModel;
        $elem = $model->find($id);
        $data = $request->only($model->getFillable());

        $data['admin_user_id'] = Auth::guard('front')->user()->id;
        $data['estado'] = 'asignado';

        $elem->fill($data)->save();

        return redirect()->back()->with('info', 'Actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
