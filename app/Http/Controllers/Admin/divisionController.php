<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\divisionModel;
use App\adminModels\departamentosModel;
use App\adminModels\seccionModel;
use App\adminModels\oficinaModel;
use App\adminModels\delegacionModel;

class divisionController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
        $this->back = 'division';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = divisionModel::get();

        foreach($data as $temp){
            $temp['departamento'] = departamentosModel::where('division_id', $temp->id)->get();

            foreach($temp['departamento'] as $temp2){

                $temp2['seccion'] = seccionModel::where('departamento_id', $temp2->id)->get();

                foreach($temp2['seccion'] as $temp3){
                    $temp3['oficina'] = oficinaModel::where('seccion_id', $temp3->id)->get();
                }
            }
        }

        return view('admin.division.index',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $elem = new divisionModel;
        $elem->fill($request->only($elem->getFillable()));
        $elem->save();

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    public function storeDepartamento(Request $request, $idparent)
    {
        $elem = new departamentosModel;
        $data = $request->only($elem->getFillable());
        $data['division_id'] = $idparent;
        $elem->fill($data);
        $elem->save();

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    public function storeSeccion(Request $request, $idparent)
    {
        $elem = new seccionModel;
        $data = $request->only($elem->getFillable());
        $data['departamento_id'] = $idparent;
        $elem->fill($data);
        $elem->save();

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    public function storeOficina(Request $request, $idparent)
    {
        $elem = new oficinaModel;
        $data = $request->only($elem->getFillable());
        $data['seccion_id'] = $idparent;
        $elem->fill($data);
        $elem->save();

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    public function storeDelegacion(Request $request, $idparent)
    {
        $elem = new delegacionModel;
        $data = $request->only($elem->getFillable());
        $data['seccion_id'] = $idparent;
        $elem->fill($data);
        $elem->save();

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = new divisionModel;
        $elem = $model->find($id);
        $elem->fill($request->only($model->getFillable()));
        $elem->save();

        return redirect()->back()->with('info', 'Actualizado correctamente');
    }

    public function updateDepartamento(Request $request, $id)
    {
        $model = new departamentosModel;
        $elem = $model->find($id);
        $elem->fill($request->only($model->getFillable()));
        $elem->save();

        return redirect()->back()->with('info', 'Actualizado correctamente');
    }

    public function updateSeccion(Request $request, $id)
    {
        $model = new seccionModel;
        $elem = $model->find($id);
        $elem->fill($request->only($model->getFillable()));
        $elem->save();

        return redirect()->back()->with('info', 'Actualizado correctamente');
    }

    public function updateOficina(Request $request, $id)
    {
        $model = new oficinaModel;
        $elem = $model->find($id);
        $elem->fill($request->only($model->getFillable()));
        $elem->save();

        return redirect()->back()->with('info', 'Actualizado correctamente');
    }

    public function updateDelegacion(Request $request, $id)
    {
        $model = new delegacionModel;
        $elem = $model->find($id);
        $elem->fill($request->only($model->getFillable()));
        $elem->save();

        return redirect()->back()->with('info', 'Actualizado correctamente');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        divisionModel::destroy($id);
        return redirect()->back()->with('warning', 'Eliminado correctamente');
    }

    public function destroyDepartamento($id)
    {
        departamentosModel::destroy($id);
        return redirect()->back()->with('warning', 'Eliminado correctamente');
    }

    public function destroySeccion($id)
    {
        seccionModel::destroy($id);
        return redirect()->back()->with('warning', 'Eliminado correctamente');
    }

    public function destroyOficina($id)
    {
        oficinaModel::destroy($id);
        return redirect()->back()->with('warning', 'Eliminado correctamente');
    }

    public function destroyDelegacion($id)
    {
        delegacionModel::destroy($id);
        return redirect()->back()->with('warning', 'Eliminado correctamente');
    }
}
