<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\tasksModel;
use Illuminate\Support\Facades\Storage;

class tasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function show($id){

      $reference = explode('_', $id);
      $name = $reference[0];
      $breadcrumb = session('breadcrumb') != null ? session('breadcrumb') : [];

      $files = tasksModel::where('id_task_manager', $id)->get();

      foreach($files as $tl){
          $tl['file'] = Storage::url($tl->url);
      }

      return view('admin.tasks.show',
            [
                'menubar'=> $this->list_sidebar(),
                'files'=>$files,
                'id_file_manager' => $id,
                'breadcrumb' => $breadcrumb,
                'name' => $name
            ]);
    }

    public function store(Request $request, $id) {

      $archivos = new tasksModel;
      $data = $request->only($archivos->getFillable());

      $data['id_task_manager'] = $id;

      $archivos->fill($data)->save();
      return redirect()->back()->with('success','Guardado correctamente!');
    }

    public function update(Request $request, $id_file_manager, $id) {
      $archivos = new tasksModel;
      $elm = $archivos::find($id);
      $data = $request->only($archivos->getFillable());

      $elm->fill($data)->save();

      return redirect()->back()->with('info','Actualizado correctamente!');
    }

    public function destroy($id_file_manager, $id_file) {

      $elm = tasksModel::find($id_file);

      Storage::disk('public')->delete($elm->url);

      $elm->delete($id_file);

      return redirect()->back()->with('warning','Borrado correctamente!');
    }

}
