<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\frontAccessModel;

class accessFrontController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->indexUrl= 'admin.accesosFront.index';
    }

    public function index(){
      $accesos = frontAccessModel::get();

      return view('admin.accessFront.show',
            ['menubar'=> $this->list_sidebar(),
             'accesos' => $accesos
            ]);

    }

    public function store(Request $request) {
      $validator = $request->validate([
          'name' => 'required',
          'route' => 'required',
      ]);

      $access = new frontAccessModel;
      $data = $request->only($access->getFillable());
      // $data["publc"] = (isset($data["publc"])?1:0);
      $access->fill($data)->save();
      return redirect()->route($this->indexUrl)
      ->with('success','Guardado correctamente!');
    }


    public function destroy($id) {
      frontAccessModel::destroy($id);
      return redirect()->route($this->indexUrl)
      ->with('warning','Borrado correctamente!');
    }


    public function update(Request $request, $id) {
      $model = new frontAccessModel;
      $access = $model::find($id);
      $data = $request->only($model->getFillable());
      // $data["publc"] = (isset($data["publc"])?1:0);
      $access->fill($data)->save();
      // dd($access);
      // return redirect('pages/aracislemler/'.$vehicle->id);
      return redirect()->route($this->indexUrl)
      ->with('info','Actualizado correctamente!');
    }
}
