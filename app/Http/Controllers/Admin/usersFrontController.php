<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\frontModels\UserFront;
use App\adminModels\roles;
use App\adminModels\departamentosModel;
use App\adminModels\seccionModel;
use App\adminModels\oficinaModel;
use App\adminModels\delegacionModel;
use App\adminModels\frontAccessModel;

use Illuminate\Support\Facades\Hash;
class usersFrontController extends Controller
{
  protected $redirecUlr;


  public function __construct()
  {
      $this->middleware('auth:admin');
  }


  public function index(){
    $en = $this->superuser();
    $count = $this->get_country();
    // if(self::get_userType()["type"]==1){
    $roleUsers = [];
    // $listof=userFront::when(!$en,
    //   function ($query) use ($count) {
    //        return $query->where('country', $count);
    //    })->get();
       // dd($count);

    $departamentos = [[0, '---']];
    $secciones = [[0, '---']];
    $oficinas = [[0, '---']];
    $delegaciones = [[0, '---']];

    $departamentos_query = departamentosModel::get();
    $seccion_query = seccionModel::get();
    $oficina_query = oficinaModel::get();
    $delegacion_query = delegacionModel::get();
    $departamentos_obj = [];

    foreach ($departamentos_query as $temp1){
        $departamentos[] = [$temp1->id, $temp1->nombre];

        $seccion_query = seccionModel::where('departamento_id', $temp1->id)->get();

        $secciones = [];

        foreach($seccion_query as $temp2){
            $oficinas_query = oficinaModel::where('seccion_id', $temp2->id)->get();
            $delegaciones_query = delegacionModel::where('seccion_id', $temp2->id)->get();
            $oficinas = [];
            $delegaciones = [];

            foreach($oficinas_query as $temp3){
                $oficinas[$temp3->id] = [
                    'nombre' => $temp3->nombre
                ];
            }

            foreach($delegaciones_query as $temp3){
                $delegaciones[$temp3->id] = [
                    'nombre' => $temp3->nombre
                ];
            }

            $secciones[$temp2->id] = [
                'nombre' => $temp2->nombre,
                'oficinas' => $oficinas,
                'delegaciones' => $delegaciones
            ];
        }
        $departamentos_obj[$temp1->id] = [
            'nombre' => $temp1->nombre,
            'secciones' => $secciones
        ];
    }

    $access = frontAccessModel::get();


    $users = userFront::get();
    foreach($users as $temp){
        $temp['departamento'] = departamentosModel::find($temp['departamento_id']);
        $temp['seccion'] = seccionModel::find($temp['seccion_id']);
        $temp['oficina'] = oficinaModel::find($temp['oficina_id']);
        $temp['delegacion'] = delegacionModel::find($temp['delegacion_id']);

        $permissions_array = [];
        $permissions_data = json_decode($temp['permissions']);
        $permissions_data = $permissions_data != null ? $permissions_data : [];
        $i = 0;
        foreach($access as $k1 => $v1){
            $permissions_array[$i] = [];

            foreach($permissions_data as $k2 => $v2){
                if ($k2 == $v1->route){
                    $permissions_array[$i] = $v2;
                    break;
                }
            }
            $i++;
        }

        $temp['permissions_array'] = $permissions_array;
    }


    return view('admin.userFront.show',
          ['menubar'=> $this->list_sidebar(),
           'users'=>$users,
           'roleUsers'=>$roleUsers,
           'departamentos'=>$departamentos,
           'secciones'=>$secciones,
           'oficinas'=>$oficinas,
           'delegaciones'=>$delegaciones,
           'departamentos_obj' => $departamentos_obj,
           'access' => $access
          ]);
  }


  public function store(Request $request) {
    $validator = $request->validate([
        'name' => 'required',
        'password' => 'required',
        'usersys' => 'required',
    ]);

    $permissions = [];
    $values = $request->all();
    foreach($values as $k => $v){
        $ex = explode("-", $k);
        if ($ex[0] == "json"){
            if ($ex[2] == 0){
                $permissions[$ex[1]][] = 'Leer';
            }elseif($ex[2] == 1){
                $permissions[$ex[1]][] = 'Editar';
            }elseif($ex[2] == 2){
                $permissions[$ex[1]][] = 'Eliminar';
            }
        }
    }


    $model = new userFront;
    $data = $request->only($model->getFillable());
    $data['password'] =  Hash::make($request->password);
    $data['status'] =  isset($request->status) ? 1 : 0;
    $data['permissions'] = json_encode($permissions);

    $model->fill($data)->save();
    return redirect()->back()->with('success','Guardado correctamente!');
  }


  public function destroy($id) {
    userFront::destroy($id);
    return redirect()->back()->with('warning','Borrado correctamente!');
  }


  public function update(Request $request, $id) {

    $model = new userFront;
    $finded = $model::find($id);
    $dataMod = $request->only($model->getFillable());
    $dataMod['password'] =  Hash::make($request->password);
    $dataMod['status'] =  isset($request->status) ? 1 : 0;

    $permissions = [];
    $values = $request->all();
    foreach($values as $k => $v){
        $ex = explode("-", $k);
        if ($ex[0] == "json"){
            if ($ex[2] == 0){
                $permissions[$ex[1]][] = 'Leer';
            }elseif($ex[2] == 1){
                $permissions[$ex[1]][] = 'Editar';
            }elseif($ex[2] == 2){
                $permissions[$ex[1]][] = 'Eliminar';
            }
        }
    }

    $dataMod['permissions'] = json_encode($permissions);
    // dd($dataMod);
    $finded->fill($dataMod)->save();
    return redirect()->back()->with('info','Actualizado correctamente!');
  }

}
