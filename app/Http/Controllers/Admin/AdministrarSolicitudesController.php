<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\frontModels\SolicitudesModel;
use App\frontModels\UserFront;
use App\adminModels\UserAdmin;
use Auth;

class AdministrarSolicitudesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = SolicitudesModel::get();

        $q = [
            ['Persona',[
                    'Omar Velasques',
                    'Jairo Martinez'
                ]],
            ['Departamento',[
                    'Departamento de cibercrimen',
                    'Departamento de fraudes'
                ]],
        ];

        foreach($data as $temp){
            $temp['userFront'] = UserFront::find($temp['front_user_id']);
            $temp['userAdmin'] = UserAdmin::find($temp['admin_user_id']);
            $temp['asig'] = isset($q[$temp['tipo_asignacion']][0]) ? $q[$temp['tipo_asignacion']][0] : '';
            $temp['gente'] = isset($q[$temp['tipo_asignacion']][1][$temp['asignado_id']]) ? $q[$temp['tipo_asignacion']][1][$temp['asignado_id']] : '';
        }


        return view('admin.solicitudes.index',[
            'data' => $data,
            'menubar' => $this->list_sidebar()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $model = new solicitudesModel;
        $elem = $model->find($id);
        $data = $request->only($model->getFillable());

        $data['admin_user_id'] = Auth::guard('admin')->user()->id;
        $data['estado'] = 'asignado';

        $elem->fill($data)->save();

        return redirect()->back()->with('info', 'Actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
