<?php

namespace App\Http\Controllers\Admin\companies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\companyclientsModel;
use App\adminModels\companyclientsAssocModel;
use App\adminModels\companiesModel;

class companyclientsController extends Controller
{
    private $baseModel;
    public function __construct()
    {
      $this->middleware('auth:admin');
      $this->baseModel=new companyclientsAssocModel;
      $this->routeTo='admin.clientscompany.index';
    }
    public function show($id)
    {
      // Los clientes ya asignados
      $clients = [];
      $qclients = companyclientsAssocModel::get();
      $clients_ids = [];
      foreach($qclients as $t){
          $clients_ids[] = $t->idClient;
      }
      $clien = companyclientsModel::whereNotIn('id', $clients_ids)->orderBy("clientName")->get();
      foreach($clien AS $CLIN){
          $clients[] = [$CLIN->id, $CLIN->clientName];
      }

      // solo los asignados a esta
      $dataRes=$this->baseModel::where('idCompany', $id)->get();

      $nameCompany = isset(companiesModel::find($id)['company']) ? companiesModel::find($id)['company'] : '';

      return view('admin.companies.showClientCompany',
            ['menubar'=> $this->list_sidebar(),
             'data'=>$dataRes,
             'clients'=>$clients,
             'idm'=>$id,
             'nameCompany' => $nameCompany

           ]);
    }

    public function store(Request $request)
    {
      // $nuuser = new usersadminModel;
      // $nuuser->sysuser = 'admin'.$this->randem(4);
      // $nuuser->sysuserpass = $this->randem(8);
      // $nuuser->typeuser = 'clientadmin';
      // $nuuser->save();
      $data = $request->only($this->baseModel->getFillable());
      // $data["idCompany"] = 1;
      // dd($data);
      $this->baseModel->fill($data)->save();
      return redirect()->route('admin.clientscompany.show',$request->idCompany)->with('success','Guardado correctamente!');
    }
    public function update(Request $request, $id)
    {
      $finded = $this->baseModel::find($id);
      $data = $request->only($finded->getFillable());
      $finded->fill($data)->save();
      return redirect()->route($this->routeTo)->with('info','Actualizado correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
          $this->baseModel::destroy($id);
          return redirect()->back()->with('warning','Borrado correctamente');
      }catch (\Exception $e) {
         return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
      }
    }
}
