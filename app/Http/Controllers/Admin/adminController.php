<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
// use App\adminModels\UserAdmin;

use App\adminModels\colaboradores;

class adminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      // dd(Auth::user());
      // dd(UserAdmin::find($request->user()->id_prson)->role->codaccus);
      // dd($this->list_sidebar());
        // session[]

        /*
        return view('admin.home')->
               with('menubar', $this->list_sidebar());
        */

        return view('admin.home',
                ['menubar'=> $this->list_sidebar()
              ]);

    }

}
