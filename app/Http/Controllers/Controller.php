<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Helper\Helper;
use App\adminModels\roles;
use App\adminModels\roles_names;
use App\adminModels\changeLogModel;
use App\adminModels\alertasModel;
use App\frontModels\UserFront;
use App\adminModels\frontAccessModel;
use Auth;
use Illuminate\Support\Facades\Storage;
use nusoap_client;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function list_sidebar()
    {
      // dd(!Helper::superAdmin());
      if(!Helper::superAdmin()){
          $varm=roles::find(Auth::user()->roleUS)->nameroles->where('publc','1');
      }else{
        $varm=roles_names::orderBy('naccess', 'ASC')->get();
      }

      // dd($varm);
      $icns=array();
      foreach($varm AS $elemts){
        $grupi=($elemts->groupacc!=''?$elemts->groupacc:'NA');
        $icns[str_replace(" ","",$grupi)][]=array("group"=>$elemts->groupacc,
                                                  "name"=>$elemts->naccess,
                                                  "perm"=>$elemts->archaccess);
        $groups[str_replace(" ","",$grupi)]=array("icon"=>$elemts->iconaccess,
                                                  "perm"=>$elemts->archaccess,
                                                  "name"=>$elemts->groupacc);
      }
      return array("groups"=>$groups,"access"=>$icns);
    }


    public function list_sidebar_front()
    {
        $access = frontAccessModel::get();
        $query = UserFront::find(Auth::user()->id);
        $permissions_data = json_decode($query['permissions']);

        $icons = [];
        $groups = [];
        $permissions = [];
        // se revisan todos los accesos
        foreach($access AS $temp_acc){
            // permisos por default
            $perms = [
                'Leer' => false,
                'Editar' => false,
                'Eliminar' => false,
            ];
            foreach($permissions_data as $per_key => $per_val){
                // se cargan los permisos para el acceso actual
                if ($per_key == $temp_acc->route){
                    foreach($per_val as $val){
                        $perms[$val] = true;
                    }
                }
            }
            // Si no tiene el permiso de lectura directamente no despliega
            if ($perms['Leer']){
                // se asocia el grupo
                $grupo = $temp_acc->group != '' ? $temp_acc->group : 'NA';
                $grupo = str_replace(" ", "", $grupo);
                $icons[$grupo][] = [
                    "group"=>$temp_acc->group,
                    "name"=>$temp_acc->name,
                    "perm"=>$temp_acc->route
                ];

                $groups[$grupo] = [
                    "icon"=>$temp_acc->icon,
                    "perm"=>$temp_acc->route,
                    "name"=>$temp_acc->group
                ];
            }

            $permissions[$temp_acc->route] = $perms;
        }
        return array("groups"=>$groups,"access"=>$icons, "permissions" => $permissions);
    }


    public function  randem($longitud,$nmbr=0)
    {
            $this->long=$longitud;
            $this->key ='';
            $patrn=array('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ','1234567890');
            $this->pattern = $patrn[$nmbr];
            $this->max = strlen($this->pattern)-1;
            for($i=0;$i < $this->long;$i++) $this->key.= $this->pattern[mt_rand(0,$this->max)];
            return $this->key;
    }
    public function superuser(){
      return Helper::superAdmin();
    }
    public function get_country(){
      return Auth::user()->country;
    }
    public function getUserId(){
      return Auth::user()->id;
    }
    public function uploadFiles($file,$path,$owname=1)
    {
       $extension = $file->getClientOriginalExtension();
       $name= ($owname==1?$this->randem(35).'.'.$extension:$file->getClientOriginalName());
       $filePath = $path . $name;
       // dd($file);
       Storage::disk('digitalocean')->put($filePath, file_get_contents($file));
       return $filePath;
    }
    public function deleteFiles($files)
    {
      Storage::delete($files);
    }
    public static function get_depmunic($ret='all')
    {
      	$deptos["Alta Verapaz"]=array('Chahal',
      					'Lanquín',
      					'San Juan Chamelco',
      					'Santa María Cahabón',
      					'Tucurú',
      					'Chisec',
      					'Panzós',
      					'San Pedro Carchá',
      					'Senahú',
      					'Cobán',
      					'Raxruhá',
      					'Santa Catalina La Tinta',
      					'Tactic',
      					'Fray Bartolomé de las Casas',
      					'San Cristóbal Verapaz',
      					'Santa Cruz Verapaz',
      					'Tamahú');
      	$deptos['Baja Verapaz']=array('Cubulco',
      				'Salamá',
      				'Granados',
      				'San Jerónimo',
      				'Purulhá',
      				'San Miguel Chicaj',
      				'Rabinal',
      				'Santa Cruz el Chol');
      	$deptos['Chimaltenango']=array('Acatenango',
      					'Patzicía',
      					'San José Poaquil',
      					'Santa Cruz Balanyá',
      					'Chimaltenango',
      					'Patzún',
      					'San Juan Comalapa',
      					'Tecpán',
      					'El Tejar',
      					'Pochuta',
      					'San Martín Jilotepeque',
      					'Yepocapa',
      					'Parramos',
      					'San Andrés Itzapa',
      					'Santa Apolonia',
      					'Zaragoza');
      	$deptos['Chiquimula']=array('Camotán',
      					'Ipala',
      					'San Jacinto',
      					'Chiquimula',
      					'Jocotán',
      					'San José La Arada',
      					'Concepción Las Minas',
      					'Olopa',
      					'San Juan Ermita',
      					'Esquipulas',
      					'Quezaltepeque');

      	$deptos['El Progreso']=array('El Jícaro',
      					'San Antonio La Paz',
      					'Guastatoya',
      					'San Cristóbal Acasaguastlán',
      					'Morazán',
      					'Sanarate',
      					'San Agustín Acasaguastlán',
      					'Sansare');

      	$deptos['Escuintla']=array('Escuintla',
      				'La Gomera',
      				'San José',
      				'Tiquisate',
      				'Guanagazapa',
      				'Masagua',
      				'San Vicente Pacaya',
      				'Iztapa',
      				'Nueva Concepción',
      				'Santa Lucía Cotzumalguapa',
      				'La Democracia',
      				'Palín',
      				'Siquinalá');

      	$deptos['Guatemala']=array('Amatitlán',
      					'Guatemala',
      					'San José Pinula',
      					'San Pedro Sacatepéquez',
      					'Villa Nueva',
      					'Chinautla',
      					'Mixco',
      					'San Juan Sacatepéquez',
      					'San Raymundo',
      					'Chuarrancho',
      					'Palencia',
      					'San Miguel Petapa',
      					'Santa Catarina Pinula',
      					'Fraijanes',
      					'San José del Golfo',
      					'San Pedro Ayampuc',
      					'Villa Canales');

      	$deptos['Huehuetenango']=array( 'Aguacatán',
      					'Cuilco',
      					'La Libertad',
      					'San Gaspar Ixchil',
      					'San Mateo Ixtatán',
      					'San Rafael La Independencia',
      					'Santa Ana Huista',
      					'Santiago Chimaltenango',
      					'Chiantla',
      					'Huehuetenango',
      					'Malacatancito',
      					'San Ildefonso Ixtahuacán',
      					'San Miguel Acatán',
      					'San Rafael Petzal',
      					'Santa Bárbara',
      					'Tectitán',
      					'Colotenango',
      					'Jacaltenango',
      					'Nentón',
      					'San Juan Atitán',
      					'San Pedro Necta',
      					'San Sebastián Coatán',
      					'Santa Cruz Barillas',
      					'Todos Santos Cuchumatánes',
      					'Concepción Huista',
      					'La Democracia',
      					'San Antonio Huista',
      					'San Juan Ixcoy',
      					'San Pedro Soloma',
      					'San Sebastián',
      					'Santa Eulalia',
      					'Unión Cantinil');

      	$deptos['Izabal']=array( 'El Estor',
      					'Puerto Barrios',
      					'Livingston',
      					'Los Amates',
      					'Morales');
      	$deptos['Jalapa']=array(	'Jalapa',
      					'San Luis Jilotepeque',
      					'Mataquescuintla',
      					'San Manuel Chaparrón',
      					'Monjas',
      					'San Pedro Pinula',
      					'San Carlos Alzatate');
      	$deptos['Jutiapa']=array( 'Agua Blanca',
      					'Conguaco',
      					'Jerez',
      					'Quesada',
      					'Zapotitlán',
      					'Asunción Mita',
      					'El Adelanto',
      					'Jutiapa',
      					'San José Acatempa',
      					'Atescatempa',
      					'El Progreso',
      					'Moyuta',
      					'Santa Catarina Mita',
      					'Comapa',
      					'Jalpatagua',
      					'Pasaco',
      					'Yupiltepeque');
      	$deptos['Petén']=array('Dolores',
      					'Melchor de Mencos',
      					'San Francisco',
      					'Sayaxché',
      					'Flores',
      					'Poptún',
      					'San José',
      					'La Libertad',
      					'San Andrés',
      					'San Luis',
      					'Las Cruces',
      					'San Benito',
      					'Santa Ana');


      	$deptos['Quetzaltenango']=array( 'Almolonga',
      					'Coatepeque',
      					'Flores Costa Cuca',
      					'Olintepeque',
      					'San Carlos Sija',
      					'San Mateo',
      					'Cabricán',
      					'Colomba',
      					'Génova',
      					'Palestina de Los Altos',
      					'San Francisco La Unión',
      					'San Miguel Sigüilá',
      					'Cajolá',
      					'Concepción Chiquirichapa',
      					'Huitán',
      					'Quetzaltenango',
      					'San Juan Ostuncalco',
      					'Sibilia',
      					'Cantel',
      					'El Palmar',
      					'La Esperanza',
      					'Salcajá',
      					'San Martín Sacatepéquez',
      					'Zunil');

      	$deptos['Quiché']=array('Canillá',
                                      'Chichicastenango',
                                      'Joyabaj',
                                      'Sacapulas',
                                      'San Juan Cotzal',
                                      'Zacualpa',
                                      'Chajul',
                                      'Chinique',
                                      'Nebaj',
                                      'San Andrés Sajcabajá',
                                      'San Pedro Jocopilas',
                                      'Chicamán',
                                      'Cunén',
                                      'Pachalum',
                                      'San Antonio Ilotenango',
                                      'Santa Cruz del Quiché',
                                      'Chiché',
                                      'Ixcán',
                                      'Patzité',
                                      'San Bartolomé Jocotenango',
                                      'Uspantán');

      	$deptos['Retalhuleu']=array(	'Champerico',
      					'San Andrés Villa Seca',
      					'Santa Cruz Muluá',
      					'El Asintal',
      					'San Felipe',
      					'Nuevo San Carlos',
      					'San Martín Zapotitlán',
      					'Retalhuleu',
      					'San Sebastián');

      	$deptos['Sacatepéquez']=array( 'Alotenango',
      					'Magdalena Milpas Altas',
      					'San Lucas Sacatepéquez',
      					'Santa María de Jesús',
      					'La Antigua Guatemala',
      					'Pastores',
      					'San Miguel Dueñas',
      					'Santiago Sacatepéquez',
      					'Ciudad Vieja',
      					'San Antonio Aguas Calientes',
      					'Santa Catarina Barahona',
      					'Santo Domingo Xenacoj',
      					'Jocotenango',
      					'San Bartolomé Milpas Altas',
      					'Santa Lucía Milpas Altas',
      					'Sumpango');

      	$deptos['San Marcos']=array('Ayutla',
      					'El Quetzal',
      					'Ixchiguán',
      					'Ocós',
      					'San Cristóbal Cucho',
      					'San Miguel Ixtahuacán',
      					'Sibinal',
      					'Tejutla',
      					'Catarina',
      					'El Rodeo',
      					'La Reforma',
      					'Pajapita',
      					'San José Ojetenam',
      					'San Pablo',
      					'Sipacapa',
      					'Comitancillo',
      					'El Tumbador',
      					'Malacatán',
      					'Río Blanco',
      					'San Lorenzo',
      					'San Pedro Sacatepéquez',
      					'Tacaná',
      					'Concepción Tutuapa',
      					'Esquipulas Palo Gordo',
      					'Nuevo Progreso',
      					'San Antonio Sacatepéquez',
      					'San Marcos',
      					'San Rafael Pie de La Cuesta',
      					'Tajumulco');

      	$deptos['Santa Rosa']=array('Barberena',
      					'Guazacapán',
      					'San Juan Tecuaco',
      					'Santa Rosa de Lima',
      					'Casillas',
      					'Nueva Santa Rosa',
      					'San Rafaél Las Flores',
      					'Taxisco',
      					'Chiquimulilla',
      					'Oratorio',
      					'Santa Cruz Naranjo',
      					'Cuilapa',
      					'Pueblo Nuevo Viñas',
      					'Santa María Ixhuatán');

      	$deptos['Sololá']=array( 'Concepción',
      					'San Antonio Palopó',
      					'San Marcos La Laguna',
      					'Santa Catarina Palopó',
      					'Santa María Visitación',
      					'Nahualá',
      					'San José Chacayá',
      					'San Pablo La Laguna',
      					'Santa Clara La Laguna',
      					'Santiago Atitlán',
      					'Panajachel',
      					'San Juan La Laguna',
      					'San Pedro La Laguna',
      					'Santa Cruz La Laguna',
      					'Sololá',
      					'San Andrés Semetabaj',
      					'San Lucas Tolimán',
      					'Santa Catarina Ixtahuacan',
      					'Santa Lucía Utatlán');

      	$deptos['Suchitepéquez']=array('Chicacao',
      					'Pueblo Nuevo',
      					'San Bernardino',
      					'San Juan Bautista',
      					'Santa Bárbara',
      					'Cuyotenango',
      					'Río Bravo',
      					'San Francisco Zapotitlán',
      					'San Lorenzo',
      					'Santo Domingo',
      					'Mazatenango',
      					'Samayac',
      					'San Gabriel',
      					'San Miguel Panán',
      					'Santo Tomás La Unión',
      					'Patulul',
      					'San Antonio',
      					'San José El Ídolo',
      					'San Pablo Jocopilas',
      					'Zunilito');

      	$deptos['Totonicapán']=array('Momostenango',
      					'San Francisco El Alto',
      					'San Andrés Xecul',
      					'Santa Lucía La Reforma',
      					'San Bartolo',
      					'Santa María Chiquimula',
      					'San Cristóbal Totonicapán',
      					'Totonicapán');
      	$deptos['Zacapa']=array('Cabañas',
                                      'La Unión',
                                      'Usumatlán',
                                      'Estanzuela',
                                      'Río Hondo',
                                      'Zacapa',
                                      'Gualán',
                                      'San Diego',
                                      'Huité',
                                      'Teculután');
        return $deptos;
    }
    public function get_files($filesTo)
    {
      if(!empty($filesTo)){
           $lkd = asset('storage/'.$filesTo);
           return $lkd;
      }
    }
    public function uploadFilesLocal($file,$path,$owname=1)
    {
       $extension = $file->getClientOriginalExtension();
       $name= ($owname==1?$this->randem(35).'.'.$extension:$file->getClientOriginalName());
       $filePath = $path . $name;
       Storage::put('public/'.$filePath, file_get_contents($file));
       return $filePath;
    }

    public function generateRandomFileManagerId($reference){
        $id = $reference . '_' . $this->randomizedId(8);
        $rep = archivosModel::where('id_file_manager', $id)->get();

        if (count($rep) > 0){
            return $this->generatedRandomFileManagerId($reference);
        }

        return $id;
    }

    ////// es lo mismo que el anterior pero con un nombre mas general
    public function generateRandomManagerId($reference){
        $id = $reference . '_' . $this->randomizedId(8);
        $rep = archivosModel::where('id_file_manager', $id)->get();

        if (count($rep) > 0){
            return $this->generatedRandomFileManagerId($reference);
        }

        return $id;
    }

    public function randomizedId($size){
        $a = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
        $b = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        $c = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
        $all = array_merge($a, $b, $c);

        $rand = '';
        for ($i = 0; $i < $size; $i++){
            $pos = rand(0, count($all) - 1);
            $rand .= $all[$pos];
        }
        return $rand;
    }

    public function changeLog($params){
        $request = isset($params['request']) ? $params['request'] : null;
        $area = isset($params['area']) ? $params['area'] : null;
        $type = isset($params['type']) ? $params['type'] : null;
        $element_id = isset($params['element_id']) ? $params['element_id'] : null;

        $userModel = Auth::user();
        $user = $userModel->name.'-'.$userModel->usersys;

        $baseModel = new changeLogModel();
        $data = $request->only($baseModel->getFillable());
        $data['user'] = $user;
        $data['area'] = $area;
        $data['type'] = $type;
        $data['element_id'] = $element_id;

        $baseModel->fill($data)->save();
    }

    public function daysEsp(){
      return ["Sunday"=>"Domingo","Monday"=>"Lunes",
              "Tuesday"=>"Martes","Wednesday"=>"Miércoles",
              "Thursday"=>"Jueves","Friday"=>"Viernes","Saturday"=>"Sábado"];
          }

    public function hourSeconds($hour){
      $arr = explode(':', $hour);
      return ((INT)$arr[0] * 3600) + ((INT)$arr[1] * 60);
    }
    public function secondsHour($hour){
      return gmdate("H:i:s", $hour);
    }
    public function bettwenMin($hr1,$hr2){
      $time1 = $this->hourSeconds($hr1);
      $time2 = $this->hourSeconds($hr2);
      $difference = $time2 - $time1;
      if($difference<0){
        return false;
      }else{
        $difference = $difference/3600;
        $whole = floor($difference);      // 1
        $fraction = $difference - $whole;
        $minut = (INT)round($fraction*60);
        return ($whole<10?'0':'').$whole.':'.($minut<10?'0':'').$minut.':00';
      }
    }

    public static function list_alertas(){

        $alertas_data = alertasModel::get();
        $alertas = [];
        foreach($alertas_data as $temp){
            $alertas[] = $temp;
        }

        return $alertas;
    }

    public function restrict_access(){
        $data = $this->list_sidebar_front()["permissions"];
        $uri = \Route::current()->uri;
        if ( !isset($data[$uri]) || $data[$uri]["Leer"] ){
            return true;
        }
        return false;
    }
}
