<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\productsModel;
use App\adminModels\colorsModel;
use App\adminModels\sizesModel;
use App\adminModels\suscribersModel;
use App\adminModels\ordersModel;
use App\adminModels\ordersDetailsModel;
use App\adminModels\categoriesModel;
use Carbon\Carbon;
use App\adminModels\ordersMovesModel;
use App\adminModels\clientsModel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\adminModels\statusOrdersModel;

class productsController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth:api');
       $this->middleware('cors');
    }
    public function index()
    {
      $limitLect = productsModel::get();
      $products = [];

      ///////// categories
      $ctc = $sctc = [];
      $categs = categoriesModel::all();
      foreach($categs as $c){
          $ctc[$c['id']] = $c['category'];
          foreach($c->subcats()->get() as $sc){
              $sctc[$sc['id']] = $sc['subCategory'];
          }
      }

      foreach($limitLect AS $valu){
        $lim = json_decode($valu["images"],true);
        $el = (!empty($lim)?$lim:null);

        ///////tags
        $coloJ = json_decode($valu["colors"],true);
        $sizesJ = json_decode($valu["sizes"],true);
        $colors = [];
        if(!empty($coloJ)&&count($coloJ)>0){
          $colBel = colorsModel::whereIn("id",$coloJ)->get();
          foreach($colBel AS $valcol){
            $colors[$valcol["category"]]["category"]=$valcol["category"];
            $colors[$valcol["category"]]["list"][]=["id"=>$valcol["id"],"name"=>$valcol["namecolor"]];
          }
          $colors = array_values($colors);
        }
        $sizes = [];
        if(!empty($sizesJ)&&count($sizesJ)>0){
          $sizeBel = sizesModel::whereIn("id",$sizesJ)->get();
          foreach($sizeBel AS $valsize){
            $sizes[$valsize["category"]]["category"]=$valsize["category"];
            $sizes[$valsize["category"]]["list"][]=["id"=>$valsize["id"],"name"=>$valsize["namesize"]];
          }
          $sizes = array_values($sizes);
        }

        $products[]=[
            "id" => $valu["id"],
            "text"=>$valu["title"],
            "image"=>( !empty($el) ? asset('storage/'.$el[0]) : null),
            "price"=> $valu["price"],
            "colors"=> $colors,
            "sizes"=> $sizes,
            "category"=> isset($ctc[$valu["category"]]) ? $ctc[$valu["category"]] : '',
            "subcategory"=> isset($sctc[$valu["subcategory"]]) ? $sctc[$valu["subcategory"]] : '',
        ];
      }
      return response()->json([
          "status" => true,
          "data" =>$products
        ], 200);
    }
    public function show($id)
    {
      $produc = productsModel::find($id);
      if(!empty($produc)){
        $imag = [];
        $getim = json_decode($produc->images,true);
        if(!empty($getim)&&count($getim)>0){
          foreach($getim AS $fot){
            $imag[] = asset("storage/".$fot);
          }
        }
        ////////// tags
        $coloJ = json_decode($produc["colors"],true);
        $sizesJ = json_decode($produc["sizes"],true);
        $colors = [];
        if(!empty($coloJ)&&count($coloJ)>0){
          $colBel = colorsModel::whereIn("id",$coloJ)->get();
          foreach($colBel AS $valu){
            $colors[$valu["category"]]["category"]=$valu["category"];
            $colors[$valu["category"]]["list"][]=["id"=>$valu["id"],"name"=>$valu["namecolor"]];
          }
          $colors = array_values($colors);
        }
        $sizes = [];
        if(!empty($sizesJ)&&count($sizesJ)>0){
          $sizeBel = sizesModel::whereIn("id",$sizesJ)->get();
          foreach($sizeBel AS $valu){
            $sizes[$valu["category"]]["category"]=$valu["category"];
            $sizes[$valu["category"]]["list"][]=["id"=>$valu["id"],"name"=>$valu["namesize"]];
          }
          $sizes = array_values($sizes);
        }

        ///////// categories
        $ctc = $sctc = [];
        $categs = categoriesModel::all();
        foreach($categs as $c){
            $ctc[$c['id']] = $c['category'];
            foreach($c->subcats()->get() as $sc){
                $sctc[$sc['id']] = $sc['subCategory'];
            }
        }

        $products = ["id"=>$produc->id,
                     "code"=>$produc->code,
                     "name"=>$produc->title,
                     "description"=>$produc->description,
                     "images"=>$imag,
                     "price"=>$produc->price,
                     "tags1"=>$colors,
                     "tags2"=>$sizes,
                     "category"=> isset($ctc[$produc->category]) ? $ctc[$produc->category] : '',
                     "subCategory"=> isset($sctc[$produc->subcategory]) ? $sctc[$produc->subcategory] : ''
                   ];

        return response()->json([
            "status" => true,
            "data" =>$products
          ], 200);
        }else{
          return response()->json([
              "status" => false,
              "message" =>"Producto no encontrado",
            ], 400);
        }
    }
    public function store(Request $request)
    {
      $produc = productsModel::find($request->idProduct);
      if(!empty($produc))
      {
        $order = ordersModel::where("tokenProd",$request->token)->first();
        $prodet=0;
        $count = $produc->price*$request->quantity;
        $token = random_bytes(15);
        $token = bin2hex($token);
        if(!empty($order)){
          if(!empty($request->idclient))
            $order->userBuy = $request->idclient;
            $order->save();
          $prodet = ordersDetailsModel::where(["order_id"=>$order->id,
                                             "product_id"=>$request->idProduct])
                                     ->update(["amountSub"=>$count,
                                   "quantity"=>$request->quantity]);

          $token= $order->tokenProd;
          }else{
            $order = new ordersModel;
            if(!empty($request->idclient))
              $order->userBuy = $request->idclient;
            $order->statusOrder = 0;
            $order->tokenProd = $token;
            $order->save();
            $ordnm = new ordersMovesModel;
            $ordnm->idMove = 1;
            // $ordnm->idUser = 0;
            $ordnm->idOrders = $order->id;
            $ordnm->save();
          }
          if($prodet===0){
            $newProd = new ordersDetailsModel;
            $newProd->order_id = $order->id;
            $newProd->product_id = $request->idProduct;
            $newProd->amountSub = $count;
            $newProd->quantity = $request->quantity;
            $newProd->save();
          }
          return response()->json([
              "status" => true,
              "data" =>["items"=>$order->details()->count(),"tokenOrder"=>$token]
            ], 200);
        }
        else if(!empty($request->token)&&empty($request->idProduct)
        &&!empty($request->idclient)&&empty($request->quantity))
        {
            $order = ordersModel::where("tokenProd",$request->token)->first();
            $client = clientsModel::where("id",$request->idclient)->first();
            if(!empty($order)&&!empty($client)){
              $order->update(["userBuy"=>$request->idclient]);
            }
            // dd($order,$client);
          return response()->json([
              "status" => true,
              "data" =>["items"=>$order->details()->count(),
                        "tokenOrder"=>$request->token]
            ], 200);
        }


        else{
          return response()->json([
              "status" => false,
              "message" =>"Datos incorrectos",
            ], 400);
        }
    }
    public function get_order($token=null,$idOrder=null)
    {
      $ord = $this->getOrder($token);

      if($ord){
        return response()->json([
            "status" => true,
            "data" => [
                "items" => $ord['recib'],
                "discount" => $ord['discount'],
                "total" => $ord['suma'],
                "totalDis" => $ord['totalDis'],
            ]
        ], 200);
      }else{
        return response()->json([
            "status" => false,
            "message" =>"No hay productos en el carrito",
          ], 400);
      }
    }

    public function destroy($id){
      $limitLect = ordersDetailsModel::destroy($id);
      if($limitLect==1){
        return response()->json([
            "status" => true,
            "data" =>["mensaje"=>"Borrado correctamente"]
          ], 200);
      }else{
        return response()->json([
            "status" => true,
            "data" =>["mensaje"=>"No se ha encontrado el producto"]
          ], 200);
      }

    }
    public function upOrder(Request $request){
      $order = ordersModel::where("tokenProd",$request->token)->first();
      $prodet=0;




      if(!empty($order)){
          $order->statusOrder = 1;
          $order->save();
          $finded = ordersModel::find($order->id);
          // Mail::to('cesar@inblackmark.com')->send(new buyings($finded));
          $cltInfo = $finded->getBuyer()->first();
          $name = $cltInfo->nameF." ".$cltInfo->nameS." ".$cltInfo->lastnameF." ".$cltInfo->lastnameS;
          $message = $this->maling($order);
          $client = new \GuzzleHttp\Client();
          $url = "http://".request()->getHost()."/sendgrid/sendmail.php";
          $myBody['message'] = $message;
          $myBody['fromMail'] = $cltInfo->mail;
          $myBody['fromName'] = $name;
          $myBody['norder'] = $finded->id;
          $request = $client->post($url,  ['form_params'=>$myBody]);

        return response()->json([
            "status" => true,
            "data"=>["message" =>"Orden enviada"],
          ], 200);
      }else{
        return response()->json([
            "status" => false,
            "message" =>"Datos incorrectos",
          ], 400);
      }


    }
    public function maling($datas)
    {
        // $cltInfo = $this->data->getBuyer()->first();
        //
        //         $address = $cltInfo->mail;
        //         $subject = 'Compra de sitio web, orden #'.$this->data->id;
        //         $name = $cltInfo->nameF." ".$cltInfo->nameS." ".$cltInfo->lastnameF." ".$cltInfo->lastnameS;
        // dd($address,$subject,$name);
        // return view('admin.crmreports.orderDetails',
        //       ['menubar'=> $this->list_sidebar(),
        //         'finded'=>$finded,
        //       ]);
        return view('emails.mail',[ 'finded' => $datas ])->render();
    }
}
