<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\apiModels\loggsModel;
use Carbon\Carbon;
use App\adminModels\client_colab_assocModel;
use App\adminModels\colaboradores;
use App\adminModels\horaryModel;
use App\adminModels\petitionsModel;
class loggsController extends Controller
{
    public function __construct()
    {
       $this->middleware('cors');
    }
    public function index()
    {
      return response()->json([
          "status" => true,
          "data" =>[],
        ], 200);
    }
    public function store(Request $request)
    {
      $hourMin =$hourMinTmp = (isset($request->hour)?$request->hour:date("H:i:s"));
      // $hourMin = '08:00:00';
      // $hourMin =$hourMinTmp= '15:15:00';
      // $hourMin =$hourMinTmp= '16:45:00';
      $hour=$hourTmp=$hourTmp2 = $this->hourSeconds($hourMin);
      // date_default_timezone_set('America/Guatemala');
      // setlocale(LC_ALL, 'es_GT');
       $workIng=client_colab_assocModel::where("idColab",auth()->user()->id)->get();
       $clients = $shedules = [];
       $clientID = 0;
       $chekBefar = '00:00:00';
       $idLInks = $idUnique = $idHorar = null;
       $enterBefore = 'false';
       $days = $this->daysEsp();
       $days = $days[date('l')];
       $days = (isset($request->day)?$request->day:$days);
       $hasWork = 'false';
       $betw = $chekBefTxt = 'false';
       $logsMo = null;
       // dd($request->idFollow);
       if(isset($request->idFollow)){
         $logsMo = loggsModel::find($request->idFollow);
         // dd( $logsMo);
         $getShedules = horaryModel::find($logsMo->idDay);
         $hourbef = $this->bettwenMin($hourMin ,$getShedules->exit);
         // dd($hourbef);
         if(!empty($hourbef)){
           $hourbefSec = $this->hourSeconds($hourbef);
           $betw = ($hourbefSec<=3600&&$request->typo=='salida'||
                    empty($logsMo->hourExitBetw)&&$hourbefSec<=3600
                    &&$request->typo=='entrada'?'true':'false');
           // dd($betw);
         }else if(empty($hourbef)){
           $betw = 'true';
         }
         // dd($hourbef,$this->hourSeconds("01:00"),$hourbefSec);
         // dd($betw,$hourbef);
       }
       // dd( $betw);
       $companyId = null;
       if($request->typo=='entrada'&&empty($logsMo))
       {
         foreach($workIng AS $valus){
           $shed = $valus->getJobs()->first();
           if(!empty($shed)){
             // $days
             $horarios = $shed->getShedules()->where("day",$days)->get();
             // dd($horarios);
             foreach($horarios AS $vlone){
                 $chekBef = $this->bettwenMin($hourMinTmp,$vlone->entry);
                  if($chekBef!==false&&$this->hourSeconds($chekBef)>0){
                    $hourTmp =$this->hourSeconds($vlone->entry); //si entra antes de la hora establece la hora inicial
                    $hourMinTmp = $vlone->entry;
                    $chekBefar =  $chekBef;
                    $chekBefTxt = 'true';
                  }
                  if($hourTmp>=$this->hourSeconds($vlone->entry)&&$hourTmp<=$this->hourSeconds($vlone->exit)){
                    // dd($hourMinTmp,$vlone->entry);
                   $clientInfo = $valus->getClientInfo()->first();
                   $clientID =$clientInfo->idClient;
                   $shedules = ["entry"=>$vlone->entry,"output"=>$vlone->exit];
                   $clients= [$vlone->id,$valus->idClient,$valus->idBranch,$valus->idArea,$valus->idPuesto];
                   $idUnique = auth()->user()->id.$vlone->id.$valus->idClient.$valus->idBranch.$valus->idArea.$valus->idPuesto;
                   $idLInks = $valus->id;
                   $idBranch = $valus->idBranch;
                   $idArea = $valus->idArea;
                   $idHorar = $vlone->id;
                   $companyId = $clientInfo->idCompany;
                   // dd($companyId);
                   $hasWork = true;
                   // break;
                 }

             }

           }
         }

         // dd($shedules["entry"]);
         $ontime=$outime=0;
         $ontimeTxt=$outimeTxt='true';
           if($hasWork)
           {
             //si entra tarde
             $dateInterval = $this->bettwenMin($shedules["entry"],$hourMinTmp );
             // dd($dateInterval);
             $tosedc = $this->hourSeconds($dateInterval);
             if($tosedc>0){
               $outime=$dateInterval;
               $expl = explode(":",$dateInterval);
               $ontimeTxt = ($expl[1]>1?'true':'false');
             }
             // dd($shedules);
             // dd();
             $logMdl = new loggsModel;
             $logMdl->latitude = $request->latid;
             $logMdl->longitud = $request->longid;
             $logMdl->userId = auth()->user()->id;
             $logMdl->ontime = $ontimeTxt;
             $logMdl->idFollower = $idUnique;
             $logMdl->hourEnter = $hourMin;
             $logMdl->outTimeEnterTxt = $ontimeTxt;
             $logMdl->outTimeEnter = $outime;
             $logMdl->befTimeEnter = $hourMinTmp;
             $logMdl->befTimeEnterTxt = $chekBefTxt;
             $logMdl->dayWeek = $days;
             $logMdl->dateDay = date("Y-m-d");
             $logMdl->idBranch = $idBranch;
             $logMdl->idClient = $clientID;
             $logMdl->idCompany = $companyId;
             $logMdl->idArea = $idArea;
             $logMdl->idLink = $idLInks;
             $logMdl->idDay = $idHorar;
             $logMdl->save();
             $idEnter = $logMdl->id;
             $saveFollow = colaboradores::find(auth()->user()->id);
             $saveFollow->idWRegister = $logMdl->id;
             $saveFollow->save();
             return response()->json([
               "status" => true,
               "data"=>["idFollow"=>$idEnter],
               "message" => "Guardado"
             ], 200);
           }
           else
           {
             return response()->json([
               "status" => false,
               "message" => "No hay horarios de entrada"
             ], 404);
           }
       }
       else if($request->typo=='salida'&&!empty($logsMo)&&$betw!=='true')
       {
         // dd('Salida nueva');
         $logsMo->hourExitBetw = $hourMin;
         $logsMo->latitudeEnterBtw = $request->latid;
         $logsMo->longitudEnterBtw = $request->longid;

         // $logsMo->hourNight
         // $logsMo->extraHourNight
         // $logsMo->extraHourEspecial
         $logsMo->save();
         return response()->json([
           "status" => true,
           "data"=>["idFollow"=>$request->idFollow],
           "message" => "Guardado"
         ], 200);
       }
       else if($request->typo=='entrada'&&!empty($logsMo)&&$betw!=='true')
       {
         $logsMo->hourEnterBetw = $hourMin;
         $logsMo->latitudeExitBtw = $request->latid;
         $logsMo->longitudExitBtw = $request->longid;
         $logsMo->save();
         return response()->json([
           "status" => true,
           "data"=>["idFollow"=>$request->idFollow],
           "message" => "Guardado"
         ], 200);
       }
       else if($request->typo=='salida'&&!empty($logsMo))
       {
         // 356111
         // dd(auth()->user()->idWRegister);

         $exitAfter = false;
         // $logsMo = loggsModel::find($request->idFollow);
         if(!empty($logsMo)){
           $ontime=$outime=$howHours=$hourTm=$hourMinTmp=$exitTotal= 0;

               // if($tosedc>0){
                 $extraT = $outime = $exitHour = '00:00';
                 //tiempo trabajado desde que marco hasta el horario predefinido
                 $horaSal = $hourCalc = $hourMin;
                 $enterHour =$logsMo->hourEnter;
                 $exitHTime = $this->bettwenMin($getShedules->exit,$hourMin);
                 if($exitHTime!==false){
                   $exitHSec = $this->hourSeconds($exitHTime);
                   $exitHour = explode(":",$exitHTime);
                   $limit = $this->hourSeconds('00:30:00');
                   $hourCalc = $getShedules->exit;
                   $horaSal = $this->hourSeconds($getShedules->exit);
                   $limit = $horaSal + $limit;
                   $salida = $this->hourSeconds($hourMin);
                   if($salida>=$limit){
                     $exitAfter = true;
                     $howHours = floor($exitHSec / 1800);
                     // $payExtra = $howHours;
                   }
                   // $exitHour = $getShedules->exit;
                   // $estm = $this->secondsHour($limit);
                   // $exitHour = (INT)$exitHour[0];
                 }
                  $chekBef = $this->bettwenMin($logsMo->hourEnter,$getShedules->entry);
                  if($chekBef!==false&&$this->hourSeconds($chekBef)>0){
                    $enterHour =$getShedules->entry; //si entra antes de la hora establece la hora inicial
                  }
                  $horaTotalDay = $this->bettwenMin($enterHour,$hourCalc);
                 //horario configurado de trabajo del sistema
                 // $workHTime = $this->bettwenMin($getShedules->entry,$getShedules->output );
                 // $workHorSec = $this->hourSeconds($workHTime);
                 // $workHoraryT = explode(":",$workHTime);
                 // $workHorary = (INT)$workHoraryT[0];
                 // $workHoraryM = (INT)$workHoraryT[1];
                 // //horario marcado de trabajo
                 $workDone = $this->bettwenMin($logsMo->hourEnter,$hourMin );
                 $workDoneSec = $this->hourSeconds($workDone);

               $logsMo->latitudeExit = $request->latid;
               $logsMo->longitudExit = $request->longid;
               $logsMo->hourExit = $hourMin;
               $logsMo->hourDay = $horaTotalDay; //horas dia
               $logsMo->hourIdeal = $workDone; //horas reales(con extras)
               $logsMo->extraHourDay = $exitHTime; //hora extra
               $logsMo->extraHourDayMiddle = $howHours;
               // $logsMo->hourNight
               // $logsMo->extraHourNight
               // $logsMo->extraHourEspecial
               $logsMo->save();
               colaboradores::where('idWRegister',auth()->user()->idWRegister)->update(["idWRegister"=>null]);
               return response()->json([
                 "status" => true,
                 // "data"=>["idFollow"=>$idEnter],
                 "message" => "Guardado"
               ], 200);
             }else{
               return response()->json([
                 "status" => false,
                 "message" => "Aun no ha empezado un horario"
               ], 400);
             }
           }
           else
           {
             return response()->json([
               "status" => false,
               "message" => "Aun tiene un horario abierto"
             ], 400);
           }
    }
    // private function hourSeconds($hour){
    //   $arr = explode(':', $hour);
    //   return ((INT)$arr[0] * 3600) + ((INT)$arr[1] * 60);
    // }
    // private function secondsHour($hour){
    //   return gmdate("H:i:s", $hour);
    // }
    // private function bettwenMin($hr1,$hr2){
    //   $time1 = $this->hourSeconds($hr1);
    //   $time2 = $this->hourSeconds($hr2);
    //   $difference = $time2 - $time1;
    //   if($difference<0){
    //     return false;
    //   }else{
    //     $difference = $difference/3600;
    //     $whole = floor($difference);      // 1
    //     $fraction = $difference - $whole;
    //     $minut = (INT)round($fraction*60);
    //     return ($whole<10?'0':'').$whole.':'.($minut<10?'0':'').$minut.':00';
    //   }
    // }
    public function shedules(){
      $workIng=client_colab_assocModel::where("idColab",auth()->user()->id)->get();


      $clientsArr = [];
      $days = $this->daysEsp();
      $days = $days[date('l')];
      foreach($workIng AS $valus){
        $taskList =  $shedules = [];
        $shed =$valus->getJobs()->first();
        $clients = $valus->getClientInfo()->first()->getClient()->first();
        // dd($clients);
        if(!empty($shed)){
          $taskAssi = $shed->tasks()->get();
          foreach($taskAssi AS $vlist){
            $taskList[] = ["ids"=>$vlist["id"],
                           "name"=>$vlist["name"],
                           "descript"=>$vlist["description"]];
          }
          // dd($taskList);
          $horarios = $shed->getShedules()->where("day",$days)->get();
          // if()
          // dd($horarios);
          foreach($horarios AS $vlone){
            $theFollow = null;
            if(isset($valus->id)&&isset($vlone->id)&&isset($clients->id)){
              $theFollow = loggsModel::where("idLink",$valus->id)
              ->where("idDay",$vlone->id)
              ->where("idClient",$clients->id)
              ->where("userId",auth()->user()->id)
              ->first();

            }

              $shedules[] = ["idFollow"=>(!empty($theFollow)?  $theFollow->id:null),
                             "entry"=>$vlone->entry,
                             "output"=>$vlone->exit];
          }
        }
        $clientsArr[]=["idL"=> $valus->id,
                    "name"=> $clients->clientName,
                    "shedule"=>$shedules,
                    "tasks"=>$taskList];
      }
        // dd($clients);
         return response()->json(['data'=>$clientsArr]);
      // }
      // else
      // {
      //   return response()->json([
      //     "status" => false,
      //     "message" => "No hay horarios de entrada"
      //   ], 404);
      // }
    }
    public function storeWorkday(Request $request)
    {
       if(isset($request->idFollow)){
         $logsMo = loggsModel::find($request->idFollow);
         $priceTaskCol = $logsMo->getInfoCola()->first();
         // dd($priceTaskCol->hora_extra_diurna);
         $idPs = $logsMo->getClientAsoc()->first()->getJobs()->first();
         $getjsbs = json_decode($request->tastkDone,TRUE);
         $tDone = [];
         $sumTOt = 0;
         foreach($getjsbs AS $vat){
           $tDone[$vat["idT"]] = $vat["quantity"];
           $sumTOt+= $vat["quantity"];
         }
         $atsks = $idPs->tasks()->get();
         $limitTask = [];
         $totDone = $doneAdd = $doneLess = 0;
         $tmpDone = $totDoneBase = 0;
         if(count($atsks)){
           foreach($atsks AS $taskP){
             if(isset($tDone[$taskP->id])){
               if($tDone[$taskP->id]>$taskP->count){
                  $doneAdd+= ($tDone[$taskP->id]-$taskP->count);
               }else if($tDone[$taskP->id]<$taskP->count){
                 $doneLess+= ($taskP->count-$tDone[$taskP->id]);
               }
               $totDoneBase+= $taskP->count;
               // $limitTask[$taskP->id] = $taskP->count;
             }

           }
           $totDone = $totDoneBase-$doneLess;
         }
         // dd($atsks,$totDoneBase,$totDone,$doneAdd,$doneLess,($doneAdd*$priceTaskCol->hora_extra_diurna));
         $logsMo->tasks = $request->tastkDone;
         $logsMo->quantityTask = $totDone;
         // $logsMo->priceTask = $sumTOt;
         $logsMo->baseTasks = $totDoneBase;
         $logsMo->complTask = ($sumTOt>= $totDone?'true':'false' );
         $logsMo->aditTask = $doneAdd;
         $logsMo->lessTask = $doneLess;
         $logsMo->priceAdiTask = ($doneAdd*$priceTaskCol->hora_extra_diurna);
         $logsMo->save();

             return response()->json([
               "status" => true,
               "message" => "Guardado"
             ], 200);
       }
       else
       {
         return response()->json([
           "status" => false,
           "message" => "Faltan datos"
         ], 404);
       }

    }
    public function getPetitions(){
      $petTypes=petitionsModel::get();
      $petit=[];
      foreach($petTypes AS $valus){
          $petit[]= ["id"=>$valus->id,"name"=>$valus->name];
        }

         return response()->json(['data'=>$petit]);
    }
}
