<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\colaboradores;
use App\adminModels\cumplesModel;

class cumplesapiController extends Controller
{

    public function __construct() {
      
      $this->middleware('cors');

    }


    public function birthday(Request $request) {

      $usuario = colaboradores::find($request->id);
      // dd($usuario->fecha);

      // dd(date('m-d', strtotime($usuario->fecha)));

      if ($usuario!='') {


        if (date('m-d') == date('m-d', strtotime($usuario->fecha))) {
            // is today
          $cumple = cumplesModel::first();

          return response()->json([
                "data" => ["nombres"=>$usuario->nombres,
                           "apellidos"=>$usuario->apellidos,
                           "imagen"=>$cumple->imagen,
                           "mensaje"=>$cumple->mensaje],
                "status"=>true
              ], 200);

        } else {

          return response()->json([
                "message" => "No",
                "status"=>false
              ], 200);

        }


      } else {
        return response()->json([
              "message" => "Error",
              "status"=>false
            ], 404);
      }


    }

}
