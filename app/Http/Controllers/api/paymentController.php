<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\apiModels\paymentsModel;
use App\adminModels\ordersModel;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use nusoap_client;

class paymentController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth:api');
       $this->middleware('cors');
       $this->pdata = new paymentsModel;
    }


    public function sendpay(Request $request)
    {
        // Agregar:--------------------
        // $request->id; ---------id usuario
        // $request->tokenO; ------------ id de la carretilla

        // $order = ordersModel::where("tokenProd", $request->tokenO)->first();
        $order = $this->getOrder($request->tokenO);

        if ($order){

            $correlativo = $this->pdata->orderBy('created_at', 'desc')->first();
            $correlativo = $correlativo ? $correlativo->audit : -1;
            $correlativo++;
            if ($correlativo > 999999){
                $correlativo = 0;
            }

            $amount = (int)floor($order['totalDis'] * 100); //formato de decimales requerido

            $client = new nusoap_client('https://epaytestvisanet.com.gt/paymentcommerce.asmx?WSDL', true);

            $url = "https://epaytestvisanet.com.gt/paymentcommerce.asmx?WSDL";
            $param=array(
                'posEntryMode' => "012" //Método de entrada
                ,'pan' => $request->pan //"4000000000000416" //Número de Tarjeta
                ,'expdate' => $request->expdate //"2412" //Fecha de expiracion (YYMM)
                ,'amount' => $amount //"1500" //Monto de la transaccion sin delimitadores
                ,'track2Data' => ""
                ,'cvv2' => $request->cvv2 //"123" //Codigo de secguridad
                ,'paymentgwIP' => "190.149.69.135" //IP WebService Visanet
                ,'shopperIP' => $request->ip() //"190.149.168.54" //IP Cliente que realiza la compra
                ,'merchantServerIP' => "67.205.167.98" //IP Comercio integrado a VisaNet
                ,'merchantUser' => "76B925EF7BEC821780B4B21479CE6482EA415896CF43006050B1DAD101669921" //Usuario
                ,'merchantPasswd' => "DD1791DB5B28DDE6FBC2B9951DFED4D97B82EFD622B411F1FC16B88B052232C7" //Password
                ,'terminalId' => "77788881" //Terminal
                ,'merchant' => "00575123" //Afiliacion
                ,'messageType' => "0200" //Mensaje de Venta
                ,'auditNumber' => $correlativo //"990628" //Correlativo ciclico de transaccion de 000001 hasta 999999
                ,'additionalData' => "" //Datos adicionales cuotas o puntos
            );
            $params = array(array('AuthorizationRequest' => $param));

            ini_set("default_socket_timeout", 10); //Tiempo en segundos para disparar reversa automatica
            $client = new nusoap_client($url, 'wsdl');
            $client->connection_timeout = 10;

            try
            {
                $result = $client->call('AuthorizationRequest',$params);
            }
            catch(SoapFault $e)
            {
                $param=array(
                    'posEntryMode' => "012" //Método de entrada
                    ,'pan' => $request->pan //"4000000000000416" //Número de Tarjeta
                    ,'expdate' => $request->expdate //"2412" //Fecha de expiracion (YYMM)
                    ,'amount' => $request->amount //"1500" //Monto de la transaccion sin delimitadores
                    ,'track2Data' => ""
                    ,'cvv2' => $request->cvv2 //"123" //Codigo de secguridad
                    ,'paymentgwIP' => "190.149.69.135" //IP WebService Visanet
                    ,'shopperIP' => $request->ip() //"190.149.168.54" //IP Cliente que realiza la compra
                    ,'merchantServerIP' => "67.205.167.98" //IP Comercio integrado a VisaNet
                    ,'merchantUser' => "76B925EF7BEC821780B4B21479CE6482EA415896CF43006050B1DAD101669921" //Usuario
                    ,'merchantPasswd' => "DD1791DB5B28DDE6FBC2B9951DFED4D97B82EFD622B411F1FC16B88B052232C7" //Password
                    ,'terminalId' => "77788881" //Terminal
                    ,'merchant' => "00575123" //Afiliacion
                    ,'messageType' => "0400" //Mensaje de Reversion
                    ,'auditNumber' => "990628" //Numero de Auditoria enviado en la solicitud de venta a reversar
                    ,'additionalData' => "" //Datos adicionales cuotas o puntos
                );
                $params = array(array('AuthorizationRequest' => $param));
                $result = $client->__soapCall('AuthorizationRequest',$params);

                $result['response']['responseText'] = $this->parseResponseCode($result['response']['responseCode']);

                $this->pdata->no_tarjeta = substr($request->pan, -4);
                $this->pdata->amount = $request->amount;
                $this->pdata->shopper_ip = $request->ip();
                $this->pdata->user_id = $request->id;
                $this->pdata->tokenO =  $request->tokenO;
                $this->pdata->status = 'failed';

                $this->pdata->audit = $result['response']['auditNumber'];
                $this->pdata->reference = $result['response']['referenceNumber'];
                $this->pdata->authorization = $result['response']['authorizationNumber'];
                $this->pdata->response = $result['response']['responseCode'];
                $this->pdata->responseText = $result['response']['responseText'];
                $this->pdata->messageType = $result['response']['messageType'];
                $this->pdata->transactionType = 'Venta';
                $this->pdata->save();

                return response()->json([
                    "status" => false,
                    "info" => 'Timeout: la transacción no se realizó a tiempo, se hizo proceso de reversa',
                    "data" => $result
                  ], 200);
            }
            $result['response']['responseText'] = $this->parseResponseCode($result['response']['responseCode']);
            $result['orderData'] = $order;

            $this->pdata->no_tarjeta = substr($request->pan, -4);
            $this->pdata->amount = $request->amount;
            $this->pdata->shopper_ip = $request->ip();
            $this->pdata->status = 'success';
            $this->pdata->user_id = $request->id;
            $this->pdata->tokenO =  $request->tokenO;

            $this->pdata->audit = $result['response']['auditNumber'];
            $this->pdata->reference = $result['response']['referenceNumber'];
            $this->pdata->authorization = $result['response']['authorizationNumber'];
            $this->pdata->response = $result['response']['responseCode'];
            $this->pdata->responseText = $result['response']['responseText'];
            $this->pdata->messageType = $result['response']['messageType'];
            $this->pdata->transactionType = 'Venta';
            $this->pdata->save();

          return response()->json([
              "status" => true,
              "data" => $result
            ], 200);
        }else{
            return response()->json([
                "status" => false,
                "data" => 'No hay orden de donde obtener los productos'
            ],  400);
        }
    }

    public function reversepay(Request $request)
    {
        $result = $this->generalReversePay([
            'auditNumber' => 990628, ////////////////////////// Resolverlo de alguna manera
            'client_ip' => $request->ip(),
        ]);

        if ($result){
            return response()->json([
                    "status" => true,
                    "data" => $result
                ], 200);
        }else{
            return response()->json([
                    "status" => false,
                    "data" => 'No se pudo realizar el proceso de reversa',
                ], 400);
        }
    }
}
