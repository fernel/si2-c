<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\sliderModel;
use App\adminModels\subSliderModel;
use App\adminModels\pbecomeModel;
use App\adminModels\clientsModel;
use App\apiModels\productsBecomeModel;
use App\apiModels\suscribersModel;
use App\adminModels\frequentqModel;
use App\apiModels\contactTypeModel;
use App\adminModels\productsModel;
use App\adminModels\typesuscModel;
use App\adminModels\ordersModel;
use App\adminModels\contactModel;
use App\adminModels\addresToModel;
use Carbon\Carbon;



class homeController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth:api');
       $this->middleware('cors');
    }
    public function index()
    {


      $limitLect = sliderModel::orderBy('orders')->get();
      $slider = [];
      foreach($limitLect AS $valu){
        $el = json_decode($valu["imageurl"]);
        $slider[]=["text"=>$valu["title"],"image"=>asset('storage/'.$el[0])];
      }
      $limitLect = productsModel::get();
      $products = [];
      foreach($limitLect AS $valu){
        $lim = json_decode($valu["images"],true);
        $el = (!empty($lim)?$lim:null);
        $products[]=[
            "id"=>$valu["id"],
            "text"=>$valu["title"],
            "image"=>(!empty($el)?asset('storage/'.$el[0]):null),
            "price"=>$valu["price"]
        ];
      }
      $prodB = pbecomeModel::orderBy('orderB')->get();
      $listprods = [];
      foreach($prodB AS $valu){
        $listprods[]=["id"=>$valu["id"],"name"=>$valu["name"]];
      }
      $listof=productsBecomeModel::select("idProducts")
      ->orderBy("created_at")->get();
      $prods = $prodn = [];
      if(!empty($listof)){
        foreach($listof as $vals){
          $expus = json_decode($vals["idProducts"]);
          foreach($expus AS $nus){
            $prods[$nus] = (isset($prods[$nus])?$prods[$nus]+1:1);
            $prodn[$nus] =$nus;
          }
        }
      }
      arsort($prods);
      $produs=pbecomeModel::select("id","name")->whereIn("id",array_values($prodn))->get();
      $prosu = [];
      foreach($produs as $pval){
        $prosu[$pval->id] = $pval->name;
      }
      $rpos = [];
      foreach($prods AS $ky=>$val){
        $rpos[] = ["prod"=>$prosu[$ky],"votes"=>$val];
      }


      return response()->json([
          "status" => true,
          "data" =>["slider"=>$slider,
                    "mostvoted"=>$rpos,
                    "listBec"=>$listprods,
                    "prods"=>$products,
                    "wsp"=>0,

          ],
        ], 200);
    }
    public function store(Request $request)
    {
      $ipcl= request()->ip();
       $tar = productsBecomeModel::where("ipClient",$ipcl)->first();
       if(empty($tar)){
         $tar = new productsBecomeModel;
       }
        $tar->ipClient =$ipcl;
        $tar->idProducts = $request->list;
        $tar->idClient = (!empty($request->client)?$request->client:0);
        $tar->save();
        return response()->json([
          "status" => true,
          "message" => "Guardado"
        ], 200);

    }
    public function suscribers(Request $request)
    {
      $data= $request->validate(['email' => 'required|email']);

       $tar = suscribersModel::where("mail",$request->email)->first();
       if(empty($tar)){
         $tar = new suscribersModel;
         $tar->mail = $request->email;
         $tar->save();
         return response()->json([
           "status" => true,
           "message" => "Guardado"
         ], 200);
       }else{
         return response()->json([
           "status" => true,
           "message" => "Ya existe"
         ], 400);
       }

    }
    public function salesContact(Request $request)
    {
      $data= $request->validate(['email' => 'required|email']);

       // $tar = contactTypeModel::where("mail",$request->email)->first();
       // if(empty($tar)){
         $tar = new contactTypeModel;
         $tar->name = $request->name;
         $tar->mail = $request->email;
         $tar->phone = $request->phone;
         $tar->typeContact = $request->type;
         $tar->save();
         return response()->json([
           "status" => true,
           "message" => "Guardado"
         ], 200);
       // }else{
       //   return response()->json([
       //     "status" => true,
       //     "message" => "Ya existe"
       //   ], 400);
       // }


    }
    public function show(readingsModel $lectopoli):lectopolis
    {
      $ecul = ["ECU1"=>"LECTA","ECU2"=>"LECTB","ECU3"=>"LECTC",
              "ECU4"=>"LECTD","ECU5"=>"LECTE","ECU6"=>"LECTF",
              "ECU7"=>"LECTG","ECU8"=>"LECTH","ECU9"=>"LECTI",
              "ECU10"=>"LECTJ","ECU11"=>"LECTK","ECU12"=>"LECTL"];
      $prfix = ($lectopoli->prefijcnt=="ec01"?$ecul[strtoupper($lectopoli->packagebook)]:strtoupper($lectopoli->packagebook));
      $limitLect = lectopolis_readings_limitsByPrefix::select('limmin','limmax')
        ->where('prefixbook','=',$prfix)
        ->where('prefijcnt','=',$lectopoli->prefijcnt)->get()
        ->take(1);
      $lectopoli->min = $limitLect[0]->limmin;
      $lectopoli->max = $limitLect[0]->limmax;
      return new lectopolis($lectopoli);
    }
    public function login(Request $request)
    {
      $getUs = clientsModel::where(["mail"=>$request->User,
                                    "passwrd"=>$request->Passw])->first();
      $ordenN = null;
     if(!empty($getUs)){
       if(!empty($request->tokenb)){
         $orden = ordersModel::where("tokenProd",$request->tokenb)
         ->whereNull("userBuy")
         ->first();
         if(!empty($orden)){
            $orden->userBuy= $getUs->id;
            $orden->save();
          }
         $ordn = $getUs->getOrders()->first();
         // dd($ordn);
         $ordenN = (!empty($ordn)?$request->tokenb:null);
       }else{
         $orden = ordersModel::where("userBuy",$getUs->id)
         ->where("statusOrder",0)
         ->first();
         $ordenN = (!empty($orden)?$orden->tokenProd:null);
       }
       // dd($ordenN);
       // $token = random_bytes(55);
       // $token = bin2hex($token);
       // dd($getUs->getOrders()->first());
      $getUs=['id' =>$getUs->id,
              "name" => $getUs->nameF." ".$getUs->nameS.", ".
                        $getUs->lastnameF." ".$getUs->lastnameS,
              "birth" => date("Y-m-d",strtotime($getUs->birth)),
              'phone' =>$getUs->phone,
              'sustype' =>$getUs->typeS,
              'address' =>$getUs->addressF,
              'idnumber' =>$getUs->ipnsuscriber,
              'country' =>$getUs->country,
              'email' =>$getUs->mail,
              "image" => ($getUs->image!=null?asset("photos/"):null),
              "tokenO"=>$ordenN,
            ];
        return response()->json([
          "status" => true,
          "data"=>$getUs,
        ], 200);
      }
      else
      {
        return response()->json([
          "status" => false,
          "message"=>"Datos incorrectos",
          "data"=>null,
        ], 400);
      }
      // dd($getUs->get());

    }
    public function registerUser(Request $request)
    {
      $getUs = clientsModel::where(["mail"=>$request->email])->first();
     if(empty($getUs)){
     if(!empty($request->firstName)&&!empty($request->email)&&
      !empty($request->lastName)&&!empty($request->password)){
      $client = new clientsModel;
      $client->nameF = $request->firstName;
      $client->nameS = $request->secondName;
      $client->lastnameF = $request->lastName;
      $client->lastnameS = $request->surName;
      $client->birth = $request->datebirth;
      $client->phone = $request->phone;
      $client->ipnsuscriber = $request->idnumber;
      $client->country = $request->country;
      $client->typeS = $request->sustype;
      // $client->phone2 = $request->firstName;
      $client->mail = $request->email;
      $client->addressF = $request->adress1;
      $client->passwrd = $request->password;
      $client->save();
          return response()->json([
            "status" => true,
            "data"=>["idClient"=>$client->id],
          ], 200);
        }else
        {
          return response()->json([
            "status" => false,
            "message"=>"Faltan datos",
            "data"=>null,
          ], 400);
        }
      }
      else
      {
        return response()->json([
          "status" => false,
          "message"=>"El correo ya está registrado",
          "data"=>null,
        ], 400);
      }
      // dd($getUs->get());

    }
    public function get_frequent()
    {
      $freqn = frequentqModel::orderBy("orderprg")->get();
      $frqu = $frecord = [];
      $orcat = [];
      if(!empty($freqn)){
        foreach($freqn AS $vals){
          $cate = $vals->getNameCat()->first();
          $frqu[$vals["idCatego"]]["name"]=$cate->name;
          $frqu[$vals["idCatego"]]["prods"][]=[$vals["title"],$vals["descrip"]];
          $orcat[$vals["idCatego"]] = $cate->ordercat;
        }
        asort($orcat);
        foreach($orcat AS $ky=>$vals){
          $frecord[$ky] = $frqu[$ky];
        }
        return response()->json([
          "status" => true,
          "data"=>array_values($frecord),
        ], 200);
      }else
      {
        return response()->json([
          "status" => false,
          "message"=>"Faltan datos",
          "data"=>null,
        ], 400);
      }
    }
    public function getClientypes(){
      $freqn = typesuscModel::orderBy("typesus")->get();
      $frqu = [];
      if(!empty($freqn)){
        foreach($freqn AS $vals){
          $frqu[]=["id"=>$vals["id"],"name"=>$vals["typesus"]];
        }
        return response()->json([
          "status" => true,
          "data"=>$frqu,
        ], 200);
      }else
      {
        return response()->json([
          "status" => false,
          "message"=>"Faltan datos",
          "data"=>null,
        ], 400);
      }
    }
    public function contactpage(Request $request)
    {
      $data= $request->validate(['email' => 'required|email']);

       // $tar = contactTypeModel::where("mail",$request->email)->first();
       // if(empty($tar)){
         $tar = new contactModel;
         $tar->name = $request->name;
         $tar->subject = 'Contacto desde web';
         $tar->mail = $request->email;
         $tar->phone = $request->phone;
         $tar->message = $request->msg;
         $tar->addresTo = $request->dest;
         $tar->save();
         return response()->json([
           "status" => true,
           "message" => "Guardado"
         ], 200);
       // }else{
       //   return response()->json([
       //     "status" => true,
       //     "message" => "Ya existe"
       //   ], 400);
       // }


    }
    public function destiny(){
      $freqn = addresToModel::get();
      $frqu = $frecord = [];
      $orcat = [];

      if(!empty($freqn)){
        foreach($freqn AS $evl){
          $frqu[]=["id"=>$evl['id'],"name"=>$evl['name'],"jobtype"=>$evl['job']];
        }
        return response()->json([
          "status" => true,
          "data"=>array_values($frqu),
        ], 200);
      }
      else
      {
        return response()->json([
          "status" => false,
          "message"=>"Faltan datos",
          "data"=>null,
        ], 400);
      }
    }
}
