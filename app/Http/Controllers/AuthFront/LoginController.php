<?php

namespace App\Http\Controllers\AuthFront;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Auth;
use Hash;
class LoginController extends Controller
{
    public function __construct()
    {
      $this->middleware("guest:front",["except"=>['logout','userlogout']]);
    }

    public function showLoginView()
    {
      // dd(Auth::user());
      return view('front.Auth.login');
    }

    public function Login(Request $request)
    {
        $this->validate($request,[
          'usersys'=>'required',
          'password'=>'required',
        ]);
        $credentials=['usersys'=>$request->usersys,
                      'password'=>$request->password];
                      // dd($request->only('usersys','password'));
                      // dd(Auth::guard('admin'));
        $res = Auth::guard('front')->attempt($credentials);
        if($res){
          // dd(route('admin.home'));
            return redirect()
                ->intended(route('home'))
                ->with('status','Ingresaste correctamente');
        }

        return $this->loginFailed();
    }

    public function username()
    {
        return 'usersys';
    }

    public function loginUsername()
    {
        return 'usersys';
    }

    public function logout(Request $request)
    {
        Auth::guard('front')->logout();
        $request->session()->invalidate();

        $request->session()->regenerateToken();
        return redirect('/');
    }

    private function validator(Request $request)
    {
        //validation rules.
        $rules = [
            'usersys'    => 'required|string|exists:admins|min:5|max:191',
            'pass' => 'required|string|min:4|max:255',
        ];

        //custom validation error messages.
        $messages = [
            'email.exists' => 'These credentials do not match our records.',
        ];
        // dd($request->validate($rules,$messages));
        //validate the request.
        $request->validate($rules,$messages);
    }


    private function loginFailed()
    {
        return redirect()
                ->back()
                ->withInput()
                ->with('error','Login fallido, intenta de nuevo!');
    }

}
