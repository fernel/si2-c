<?php

namespace App\frontModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequerimientoMPModel extends Model
{
    use HasFactory;
    protected $guard = 'admin';
    protected $table = 'admin_requerimiento_mp';
    protected $fillable = [
                            'numero',
                            'fecha_requerimiento',
                            'fiscalia_emite',
                            'procedencia',
                            'destinatario',
                            'fecha_recepcion',
                            'hora_recepcion',
                            'plazo',
                        ];
}
