<?php

namespace App\frontModels;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\adminModels\roles;

class UserFront extends Authenticatable
{
    use Notifiable;
    protected $guard = 'front';
    protected $table = 'front_user';
    protected $fillable = [
                            'id',
                            'name',
                            'usersys',
                            'password',
                            'departamento_id',
                            'seccion_id',
                            'oficina_id',
                            'delegacion_id',
                            'status',
                            'permissions',
    ];
    protected $hidden = [
                            'password',
                            'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getAuthPassword()
    {
        return $this->password;
    }
    public function permissions(){
      return $this;
    }

    public static function get_name($id)
    {
        $user = UserFront::select('name')->where("id", $id)->first();

        if ($user!='') {
            return $user->name;
        } else {
            return '';
        }

    }


}
