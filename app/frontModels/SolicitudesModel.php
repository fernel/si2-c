<?php

namespace App\frontModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SolicitudesModel extends Model
{
    use HasFactory;
    protected $guard = 'admin';
    protected $table = 'admin_solicitudes';
    protected $fillable = [
                            'codigo',
                            'created_by_front_user_id',
                            'tipo_formulario',
                            'formulario_id',
                            'tipo_asignacion',
                            'asignado_id',
                            'estado',
                            // 'admin_user_id',
                        ];
}
