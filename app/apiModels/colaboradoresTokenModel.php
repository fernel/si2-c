<?php
namespace App\apiModels;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class colaboradoresTokenModel extends Authenticatable
{
  use HasApiTokens, HasFactory, Notifiable;
  protected $table='admin_colaboradores';
  protected $fillable = [
                          "nombres",
                          "apellidos",
                          "foto",
                          "fecha",
                          "email",
                          "dpi",
                          "nit",
                          "iggs",
                          "irtra",
                          "intecap",
                          "licencia",
                          "vence_licencia",
                          "fecha_salud",
                          "fecha_pulmones",
                          "genero",
                          "nivel_academico",
                          "nacionalidad",
                          "estado_civil",
                          "conyuge",
                          "hijos",
                          "profesion",
                          "tel1",
                          "tel2",
                          "direccion",
                          "departamento",
                          "municipio",
                          "contacto1",
                          "tel_con1",
                          "parentesco_con1",
                          "contacto2",
                          "tel_con2",
                          "parentesco_con2",
                          "tipo_sangre",
                          "contrato",
                          "puesto",
                          "sueldo",
                          "bono_extra",
                          "antiguedad",
                          "bono_ley",
                          "hora_extra_diurna",
                          "hora_extra_nocturna",
                          "hora_extra_especial",
                          "viaticos",
                          "movil",
                          "hora_normal",
                          "hora_normal_nocturna",
                          "tipo_pago",
                          "cuenta1",
                          "cuenta2",
                          "cuenta3",
                          "banco",
                          "pago_sueldo",
                          "otros_pagos",

                          "puntos1",
                          "puntos2",
                          "puntos3",
                          "puntos4",

                          "estatus",
                          "fecha_ingreso",
                          "observaciones",

                          "idUser",

                          "firma",

                          "id_file_manager",

                          "password"
                      ];
        protected $hidden = [
           'password',
           'remember_token',
       ];

}
