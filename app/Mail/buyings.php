<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class buyings extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $cltInfo = $this->data->getBuyer()->first();

                $address = $cltInfo->mail;
                $subject = 'Compra de sitio web, orden #'.$this->data->id;
                $name = $cltInfo->nameF." ".$cltInfo->nameS." ".$cltInfo->lastnameF." ".$cltInfo->lastnameS;
        // dd($address,$subject,$name);
        // return view('admin.crmreports.orderDetails',
        //       ['menubar'=> $this->list_sidebar(),
        //         'finded'=>$finded,
        //       ]);
        return $this->view('emails.mail')
                    ->from($address, $name)
                    // ->cc($address, $name)
                    // ->bcc($address, $name)
                    // ->replyTo($address, $name)
                    ->subject($subject)
                    ->with([ 'finded' => $this->data ]);
    }
}
