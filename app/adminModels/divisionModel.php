<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class divisionModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_division';
  protected $fillable = [
                          'nombre',
                          'descripcion',
                      ];

}
