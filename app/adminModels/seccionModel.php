<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class seccionModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_seccion';
  protected $fillable = [
                          'nombre',
                          'descripcion',
                          'departamento_id',
                      ];

}
