<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class petitionsModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_petitions_simplepetitions';
  protected $fillable = ['name'];
}
