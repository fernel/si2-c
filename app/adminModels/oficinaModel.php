<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class oficinaModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_oficina';
  protected $fillable = [
                          'nombre',
                          'descripcion',
                          'seccion_id',
                      ];

}
