<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class delegacionModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_delegacion';
  protected $fillable = [
                          'nombre',
                          'descripcion',
                          'seccion_id',
                      ];

}
