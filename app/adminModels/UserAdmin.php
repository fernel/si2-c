<?php

namespace App\adminModels;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserAdmin extends Authenticatable
{
    use Notifiable;
    protected $guard = 'admin';
    protected $table='admin_users';
    protected $fillable = [
        'id','name', 'usersys',
        'role','statusUs','country','superuser','roleUS','password'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getAuthPassword()
    {
        return $this->password;
    }
    public function role(){
      return $this->hasOne(roles::class,"id","roleUS");
    }
    public function get_platform(){
      return $this->belongsTo(countries::class, 'country', 'code');
    }

    public static function get_name($id)
    {
        $user = UserAdmin::select('name')->where("id", $id)->first();

        if ($user!='') {
            return $user->name;
        } else {
            return '';
        }
        
    }


}
