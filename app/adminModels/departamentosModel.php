<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class departamentosModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_departamentos';
  protected $fillable = [
                          'nombre',
                          'descripcion',
                          'division_id'
                      ];

}
