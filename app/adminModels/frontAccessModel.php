<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class frontAccessModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'front_access';
  protected $fillable = [
                          'name',
                          'route',
                          'icon',
                          'group'
                      ];

}
