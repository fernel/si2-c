<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
  protected $guard = 'admin';
  protected $table='admin_roles';
  protected $fillable = [ 'nameRole','country'];
  public function nameroles(){
    return $this->belongsToMany(roles_names::class, 'admin_access_roles',
                                'id_role', 'id_access')
                                ->orderBy('naccess', 'ASC');

  }

}
