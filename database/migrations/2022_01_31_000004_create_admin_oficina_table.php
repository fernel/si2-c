<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminOficinaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_oficina', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('nombre')->unique();
            $table->text('descripcion')->nullable();
            $table->unsignedBigInteger('seccion_id');

            $table->foreign('seccion_id')->references('id')->on('admin_seccion');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_oficina');
    }
}
