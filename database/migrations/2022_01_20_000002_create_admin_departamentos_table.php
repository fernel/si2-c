<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminDepartamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_departamentos', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('nombre')->unique();
            $table->text('descripcion')->nullable();
            $table->unsignedBigInteger('division_id');

            $table->foreign('division_id')->references('id')->on('admin_division');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_departamentos');
    }
}
