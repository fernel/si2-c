<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminRequerimientoMPTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_requerimiento_mp', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('numero')->nullable();
            $table->date('fecha_requerimiento')->nullable();
            $table->string('fiscalia_emite')->nullable();
            $table->string('procedencia')->nullable();
            $table->string('destinatario')->nullable();
            $table->date('fecha_recepcion')->nullable();
            $table->time('hora_recepcion')->nullable();
            $table->string('plazo')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_requerimiento_mp');
    }
}
