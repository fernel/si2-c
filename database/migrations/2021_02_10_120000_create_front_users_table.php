<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrontUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('front_user', function (Blueprint $table) {
          $table->id();
          $table->string('name',40);
          $table->string('usersys',25)->unique();
          $table->string('password',60);
          $table->unsignedBigInteger('departamento_id')->nullable();
          $table->unsignedBigInteger('seccion_id')->nullable();
          $table->unsignedBigInteger('oficina_id')->nullable();
          $table->unsignedBigInteger('delegacion_id')->nullable();
          $table->string('status', 1);
          $table->json('permissions', 1)->nullable();

          $table->timestamps();
          $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('front_user');
    }
}
