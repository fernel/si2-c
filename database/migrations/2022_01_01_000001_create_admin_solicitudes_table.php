<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_solicitudes', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('codigo')->unique();
            $table->unsignedBigInteger('created_by_front_user_id');
            $table->string('tipo_formulario');
            $table->unsignedBigInteger('formulario_id');
            $table->string('tipo_asignacion')->nullable();
            $table->unsignedBigInteger('asignado_id')->nullable();

            $table->string('estado')->default('pendiente');
            // $table->unsignedBigInteger('admin_user_id')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_solicitudes');
    }
}
